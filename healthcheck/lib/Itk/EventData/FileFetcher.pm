#!/usr/bin/perl
###----------------------------------------------------------------------------
#
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
#
# Author: epauste
# $Id: FileFetcher.pm 7649 2012-09-28 05:40:34Z eanzmark $

package Itk::EventData::FileFetcher;
use strict;
use Hash::Util qw(lock_keys);
use File::Path;
use Itk::FileTransfer;
use Itk::Utils::RemoteSession;
#use Net::Ping;
use File::stat;


use Time::Local qw(timegm_nocheck);
BEGIN {
	if (DEBUG6) {
		require Data::Dumper;
		import Data::Dumper;
	}
}

#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	# Result Codes
	OK => 			1,	# Operation succeeded
	OK_NONEWFILES =>	2,	# Operation succeeded, no new files were found
	SKIPPED =>		3,	# File was skipped
	PING_FAILURE =>		-10,	# Cannot even ping the node
	CONNECT_FAILURE => 	-11,	# Failure to connect to node
	LOGIN_FAILURE => 	-12,	# Failed to log in to node
	INVALID_STATE => 	-13,	# State is invalid for requested operation
	INVALID_PARAMETERS => 	-14,	# Bad parameters passed to a request
	FETCH_FAILURE => 	-15,	# Failure to fetch a file
	LOCAL_FILE_FAILURE =>	-16,	# Failure in storing / creating local file or directory
	INTERNAL_ERROR =>	-99,	# Internal error
	# File Fetching Methods
	COLLECT_FIRST_MATCH => 1,	# Collect the first file that matches (if modified since last run)
	COLLECT_NEWEST_MATCH => 2,	# Collect the file that has the newest timestamp (if modified since last run)
	COLLECT_ALL_MATCHING => 3,	# Collect all matching files (if modified since last run)
	COLLECT_PMDS => 4,		# Special method for collecting PMDs
	COLLECT_ALL_NEW => 5,		# Fetch all files not previously fetched
	# Others
	COMMAND_TIMEOUT => 40,		# Amount of time allowed for completion of cello commands
};


#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'collectionHistory', # Reference to a collectionHistory Object
	'ftp',			     # Currently active FTP handle
	'nodeInfo',		     # Node Info for currently processed node
	'startdate',		 # Timestamp (unix epoch) of start time filter
	'fetchedfiles',		 # Array of files fetched by this instance
	'localdir',          #local directory variable
	'nodelete'           #nodelete option
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;
*srv=\$main::srv;

our $current_yr = (gmtime(time))[5]+1900;

# Table mapping files to paths, fetch methods
our %fileTable = (
	alarmlog =>  {
		paths => [
			{ 
				dir => '/c/logfiles/alarm_event', 
				file => 'ALARM_LOG.xml', 
			},
			{
				dir => '/c/logfiles/cello',
				file => 'CELLO_ALARM_LOG.xml', 
			}
		],
		method => COLLECT_NEWEST_MATCH,
	},
	syslog => {
		paths => [
			{
				dir => '/c/logfiles/systemlog',
				file => qr/^\d+syslog$/,
			}
		],
		method => COLLECT_ALL_MATCHING,
	},
	avlog => {
		paths => [
			{ 
				dir => '/c/logfiles/availability', 
				file => 'CELLO_AVAILABILITY2_LOG.xml', 
			},
			{
				dir => '/c/logfiles/cello', 
				file => 'CELLO_AVAILABILITY2_LOG.xml', 
			},
			{
				dir => '/c/logfiles/cello', 
				file => 'CELLO_AVAILABILITY_LOG.xml', 
			}
		],
		method => COLLECT_ALL_MATCHING,
	},
	upgradelog => {
		paths => [
			{ 
				dir => '/c/systemfiles/cello/cma/su', 
				file => qr/Trace.log(_old)?/, 
			},
			{ 
				dir => '/c/systemfiles/cello/cma/su/trace', # P6 Nodes 
				file => qr/Trace.log(_old)?/, 
			},
		],
		method => COLLECT_ALL_MATCHING,
	},
	auditlog => {
		paths => [
			{ 
				dir => '/c/logfiles/audit_trail', 
                file =>'CORBA_AUDITTRAIL_LOG.xml', 
			},
		],
		method => COLLECT_ALL_MATCHING,
	},

	shellauditlog => {
		paths => [
			{ 
				dir => '/c/logfiles/audit_trail', 
                file =>'SHELL_AUDITTRAIL_LOG.xml', 
			},
		],
		method => COLLECT_ALL_MATCHING,
	},
		
	pmd => {
		paths => undef, # Gets the path from the dump list command
		method => COLLECT_PMDS
	},
	ecda => {
		paths => [
			{
				dir => '/c/logfiles/dspdumps',
				subdir => qr/\d+_\d+/,
				file => qr/ECDAdump\d+-\d+\.gz/,
			},
			{
				dir => '/d/logfiles/dspdumps',
				subdir => qr/\d+_\d+/,
				file => qr/ECDAdump\d+-\d+\.gz/,
			},
			{
				dir => '/p000200/logfiles/dspdumps',
				subdir => qr/\d+_\d+/,
				file => qr/ECDAdump\d+-\d+\.gz/,
			},
		],
		method => COLLECT_ALL_NEW,
	},
	gcpu => {
		paths => [
			{
				dir => '/c/logfiles/dspdumps',
				subdir => qr/\d+_\d+/,
				file => qr/GCPUdump\d+-\d+\.gz/,
			},
			{
				dir => '/d/logfiles/dspdumps',
				subdir => qr/\d+_\d+/,
				file => qr/GCPUdump\d+-\d+\.gz/,
			},
			{
				dir => '/p000200/logfiles/dspdumps',
				subdir => qr/\d+_\d+/,
				file => qr/GCPUdump\d+-\d+\.gz/,
			},
		],
		method => COLLECT_ALL_NEW,
	},
);
lock_keys(%fileTable);

our $storedir;
our $timeout;

# A pinger so we can check if nodes are alive. Note: initial testing used
# 'udp' protocol, but this was unreliable. 'icmp' requires root access. 
# 'external' invokes external ping progam which is reliable but expensive
#my $pinger = new Net::Ping('external');


#==============================================================================
# CLASS METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# init(%opts)
# Class Initialisation. This static method is provided to allow the class to be
# initialised before forking to service each node - this allows initial checks,
# creation of directories if needed, etc to be done once only instead of once
# per node.
# % timeout		Timeout value (default 60)
# % storedir		Root of log storage area
#------------------------------------------------------------------------------
sub init {
	$srv->logEnter(\@_) if ENTER;
	my (%opts) = @_;
	lock_keys(%opts, qw/timeout storedir/);
	
	$timeout = $opts{timeout} || 60;

	#-------------------------------------------------------------------------------------------
	# Set the logstore, that is where the fetched log files will be stored.
	# If the logstore location does not exist, then create that directory 
	#-------------------------------------------------------------------------------------------
	$storedir = $opts{storedir} 
		or die "init >> Invalid storedir setting, aborting as I do not know where to store logfiles"; 
	
	die "Directory '$storedir' does not exist or is not writable or readable" unless (-d $storedir && -r _ && -w _);
	
	return 1;
}

##-----------------------------------------------------------------------------
# getPingCommand()
# Depending on OS, choose a ping command
#------------------------------------------------------------------------------
sub getPingCommand {
	if ($srv->isLinux()) {
		return 'ping -c 1 -w 3';
	} elsif ($srv->isSolaris()) {
		return 'ping';
	}
	die "Unsupported operating system\n";
}

#==============================================================================
# PRIVATE METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# _warn($msg)
# Issue warning, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _warn {
	$srv->logWarn("($_[0]->{nodeInfo}{itk_nodename}) $_[1]");
}

##-----------------------------------------------------------------------------
# _error($msg)
# Issue Error, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _error {
	$srv->logError("($_[0]->{nodeInfo}{itk_nodename}) $_[1]");
}

##-----------------------------------------------------------------------------
# _fetchPmds($fetchData, $filetype)
# Fetch all PMDs that were generated later than the last time we ran
# - $fetchData Entry from fileTable for the file type to be fetched
# - $filetype	Name of a file type as given in the %fileTable
# RETURNS: $resultCode, $fetchedFiles
# - $resultCode	  Result Code - see constants for definitions
# - $fetchedFiles Ref to array, one entry for each file fetched. The entries are 
#                 hashes with the following entries:
#                  * localfile => the local path to the fetched file.
#		   * file => original filename on node
#                  * id => pmd id
#                  * board => board that generated the PMD
#------------------------------------------------------------------------------
sub _fetchPmds {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fetchData, $filetype)=@_;

	# get node name
	my $nodeName=$self->{nodeInfo}->{itk_nodename};

	# Get the envelope number of the last stored PMD on the node
	my $lastPmdNo = $self->{collectionHistory}->get('last_pmd_num');
	$srv->logDebug(4, "$nodeName : Max PMD number on last run: $lastPmdNo") if DEBUG4;
	
	my $tranferMethod = $self->{nodeInfo}{transferMethod};
	my $ip = $self->{nodeInfo}{ip_address};
	my $port = $self->{nodeInfo}{shellPort};
	my $timeout = $self->{nodeInfo}{connectionTimeout} || COMMAND_TIMEOUT;
	my $t = undef;
	
	my $numberFileFound = 0;
	my $numberFileFetched = 0;
	
	$srv->logDebug(4, "$nodeName: Transfer method is : $tranferMethod") if DEBUG4;
	
	##
	# if ftp is the transfer method chosen, we support it's not a secure network
	# therefore we instanciate a telnet connection
	if ($tranferMethod eq "ftp")  {
		$srv->logDebug(4,"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++") if DEBUG4;
		$srv->logDebug(4,"$nodeName : about to instantiate telnet connection with '$ip' and '$port' ") if DEBUG4;
		$srv->logDebug(4,"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++") if DEBUG4;
		
		eval {
			$t = new Itk::Utils::CelloTelnet (host=>$ip, port=>$port, timeout=>$timeout);
		};
		$self->_error("Failed telnet connection to '$ip'") if ($@ or not $t); 
    
	} elsif ($tranferMethod eq "sftp")  {
		$srv->logDebug(4,"-----------------------------------------------------------------------") if DEBUG4;
		$srv->logDebug(4,"$nodeName : about to instantiate ssh connection with '$ip' and '$port' ") if DEBUG4;
		$srv->logDebug(4,"-----------------------------------------------------------------------") if DEBUG4;
		
		eval {
			$t = new Itk::Utils::CelloSsh(host=>$ip, port=>$port, timeout=>$timeout);
		};
		$self->_error("Failed ssh connection to '$ip'") if ($@ or not $t); 
	    
	} else {
		$self->_error("$nodeName : TransferMethod is unknown");
	}
	return CONNECT_FAILURE unless $t;		
	
	my $loggedIn;
	eval {
		$loggedIn = $t->login($self->{nodeInfo}{username}, $self->{nodeInfo}{passwd});
	};
	if ($@ or not $loggedIn) {
		$self->_error("$nodeName : Failed telnet login to '$self->{nodeInfo}{ip_address}'");
		return LOGIN_FAILURE;
	}
		
	# The file /c/pmd/pmd.data contains the min and max envelope numbers
	# that have been generated in the node
	
	my $pmdinfo = $t->sendCommand("cat /c/pmd/pmd.data");
	my @pmdinfo = split(/\n/, $pmdinfo);
	
	my $currentMaxPmd = $pmdinfo[1];
	my $currentMinPmd = $pmdinfo[0];
	if ($currentMinPmd =~ /^Could not open file/) {  # In case someone (eg OSS) has deleted the pmd.data
		undef $currentMinPmd;
		undef $currentMaxPmd;
		$srv->logDebug(5, "$nodeName : pmd.data was not found.") if DEBUG5;
	}
	$srv->logDebug(5, "$nodeName : Last Pmd No Fetched was: $lastPmdNo; current min = $currentMinPmd; current max = $currentMaxPmd") if DEBUG5;

	if (defined $currentMinPmd && defined $lastPmdNo && $currentMaxPmd == $lastPmdNo) {
		$srv->logDebug(4, "$nodeName : No new PMDs since last run.") if DEBUG4;
		$t->close();
		return OK,undef;
	}
	
	if (! defined $currentMaxPmd || $lastPmdNo > $currentMaxPmd) {
		undef $lastPmdNo;
	}
	
	my $dumplist = $t->sendCommand("dump list -a");
	my @dumplist = split(/\n/, $dumplist);
	$t->close();
	
	my @PmdsToFetch;
	my $newMaxPmdNo;
	my %pmdInfo;
	
	foreach (@dumplist) {
		$srv->logDebug(6, "Dump List Line: $_\n") if DEBUG6;
		my ($pmdTime, $pmdFile, $pmdNo, $board, $pmdId);
		if (($pmdTime, $pmdFile, $pmdNo,$board, $pmdId) = (/(\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}.\d+)\s+[\w-]+\s+[\w-]+\s+(\/c\/pmd\/(\d+)\/(\d{4})\d{2}\/(0x[0-9a-f]+)\.pmd)/)) {
			
			$board="${board}00"; # Because board number given in pmd can include the SPM executionResourceNumber, which we discard
			
			$srv->logDebug(6, "$nodeName : Found PMD with t=$pmdTime, file=$pmdFile, pmdno=$pmdNo, board=$board, pmdid=$pmdId") if DEBUG6;

			if (defined $lastPmdNo && defined $currentMinPmd && $pmdNo >= $currentMinPmd && $pmdNo <= $lastPmdNo) {
				$srv->logDebug(5, "$nodeName : Pmd $pmdFile is too old. Lets chuck it in the bin.") if DEBUG5;
				next;
			}
			
			$newMaxPmdNo=$pmdNo unless defined $newMaxPmdNo && $newMaxPmdNo > $pmdNo;

			if (defined $self->{startdate}) {
				my ($yy,$mo,$dd,$hh,$mm,$ss) = ($pmdTime=~/(^\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
				my $ts;
				# Dump list sometimes has unreasonable dates & times - need to trap errors
				eval {
					$ts=timegm_nocheck($ss,$mm,$hh,$dd,$mo-1,$yy);
				};
				if ($@) {
					$srv->logDebug(4, "$nodeName : Invalid date in dump list: $pmdTime. Discarding this PMD.") if DEBUG5;
					next;
				}
				if ($self->{startdate} > $ts) {
					$srv->logDebug(5, "$nodeName : Pmd $pmdFile is older than startdate. Discarding.") if DEBUG5;
					next;
				}
			}
			$numberFileFound++;
			$srv->logDebug(6, "$nodeName : Pmd $pmdFile will be fetched.") if DEBUG4;
			push @PmdsToFetch, {
				file => $pmdFile,
				id => $pmdId,
				board => $board,
				pmdno => $pmdNo
			};
		} else {
			$srv->logDebug(4,"$nodeName : couldn't get pmdtime, pmdfile, pmdNo, board, pmdid with: $_ ");
		}
	}
	
    # If we did not pick up any PMDs from the last run, newMaxPmdNo will be undef   
    # In that case use the max achieved at last run   
    $newMaxPmdNo ||= $lastPmdNo;   
     
      
     if ($currentMaxPmd ne $newMaxPmdNo) {   
           # The maximum pmd number in the pmd.dat file on the node is not   
           # the same as the largest pmd number in the node. Probably some kind of error   
           # We just give a warning and use the latter   
           $self->_warn("$nodeName : Max Pmd number in pmd.dat ($currentMaxPmd) is not the same as the largest PMD Number ($newMaxPmdNo)");   
     }   
     
     my $localPath="$storedir/$nodeName/pmd";   
     eval {   
            # Make the directory if it does not exist   
            -d $localPath or mkpath $localPath;   
     };   
     if ($@) {   
          $self->_error("$nodeName : Could not create directory '$localPath': $@");   
          return LOCAL_FILE_FAILURE;   
     }   
     
     my @fetchedPmdFiles;   
     my $ftp = $self->{ftp};   
     foreach my $pmdInfo (@PmdsToFetch) {   
        my $pmdFile = $pmdInfo->{file};   
     
        # Note: PMD ID is not necessarily unique per board. On an SP board   
        # each SPM has it's own ID numbers.   
        my $localFile = "$localPath/$pmdInfo->{pmdno}_$pmdInfo->{board}_$pmdInfo->{id}.pmd";   
     
        if (defined $ftp->get($pmdFile, $localFile)) {   
            $pmdInfo->{localfile} = $localFile;   
            push @fetchedPmdFiles, $pmdInfo;   
            push @{$self->{fetchedfiles}}, $localFile;   
            $numberFileFetched++;
            $srv->logDebug(5, "$nodeName : Fetched: $pmdFile => $localFile") if DEBUG5;   
        } else {   
             $self->_error("$nodeName :Could not fetch file '$pmdFile'");   
            }   
     } ## for each  
     
     if ($numberFileFetched != $numberFileFound) {
     	 $self->_error("********************************************************");
     	 $self->_error("$nodeName : pmd file found ($numberFileFound) != nb file fetched: $numberFileFetched");
     	 $self->_error("********************************************************");
     }
     
     # Update persistant data   
     $self->{collectionHistory}->put('last_pmd_num', $newMaxPmdNo);   
     
     return OK, \@fetchedPmdFiles; 
	
}

##-----------------------------------------------------------------------------
# _fetchAllMatching($fetchData, $filetype)
# Fetch all files that match the criteria specified in the fetchData, as long as
# the file was modified more recently than the last time this was run
# - $fetchData Entry from fileTable for the file type to be fetched
# - $filetype	Name of a file type as given in the %fileTable
# RETURNS: $resultCode, $fetchedFiles
# - $resultCode	  Result Code - see constants for definitions
# - $fetchedFiles Ref to array, one entry for each file fetched. The entries contain 
#                 the local path to the fetched file. May be undef on failure.
#------------------------------------------------------------------------------
sub _fetchAllMatching {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fetchData, $filetype)=@_;
	my $ftp = $self->{ftp};
	
	# FIXME: This sub and the _fetchAllNew sub are almost identical
	# There is opportunity to combine them to make a single sub
	# that handles both cases (only the decision criteria is different)
	
	# Get the collection history for this file type
	# If undef was returned, ||{} will make an empty hash
	my $chist = $self->{collectionHistory}->get($filetype) || {};
		
	# List of fetched files
	my @fetchedFiles;
		
	foreach my $filespec (@{$fetchData->{paths}}) {
		my $dir = $filespec->{dir};

	    $srv->logDebug(5, "Get all files from : " .$dir ) if DEBUG5;

		my $filematch = $filespec->{file};
		my $matchtype = ref $filematch eq 'Regexp'; # Do this here so it does not need to be done each time through the loop
		
		my $files = $ftp->dirStat($dir);
		next unless defined $files;
		foreach my $filestat (@{$files}) {
			
			# Check if file matches the supplied criteria
			if ($matchtype) { # Regex match
				next unless $filestat->{name} =~ $filematch;
			} else { # Straight Comparison
				next unless $filestat->{name} eq $filematch;
			}
			
			$srv->logDebug(5, "mtime from $filestat->{name} is : $filestat->{mtime}") if DEBUG5;
			my $mtimeTs = $filestat->{mtime};
			if (! defined $mtimeTs) {
				$self->_error("Invalid file modification time for $filestat->{name} - skipping file");
				next;
			}
			
			$srv->logDebug(5, "Found file: '$filestat->{name}' mtime='$filestat->{mtime}' size='$filestat->{size}'") if DEBUG5;
			
			# Check if the file has been modified since last fetch.
			# Modification times are stored in the collection history
			# as filetype => <name> => mtime = <mtime>
			# If the file does not exist in the mtime hash then
			# we have
			my $fileName = "$dir/$filestat->{name}";
			my $fileHist;
			if (exists $chist->{$fileName}) {

				$fileHist = $chist->{$fileName}; # History of this particular file
				
				if ($fileHist->{mtime} eq $mtimeTs && $fileHist->{size} == $filestat->{size}) {
					$srv->logDebug(5, "File not modified since last run. Skipping") if DEBUG5;
					next;
				}

			} else {
				$fileHist = {};
			}
			
			if (defined $self->{startdate} && $self->{startdate}>$mtimeTs) {
				# File has not been modified after the starttime filter.
				# We don't need to fetch the file, but we must record it in collection history
				# so that it does not get considered in future
				$srv->logDebug(5, "Skipping file: $fileName because it is older than the startdate") if DEBUG5;
				$fileHist = {
					result=>SKIPPED,
					mtime => $mtimeTs,
					size  => $filestat->{size},
				};
				$chist->{$fileName}=$fileHist;
				next;
			}
				
			# File needs fetching!
			
			# When storing file locally, need to guard against
			# collisions, in case there are different versions of the file stored under
			# different directories (avlog, alarmlog are like this)
			# Take the full node path and convert to a file name
			# by making the / = _
			# eg. /c/logfiles/cello/ALARM_LOG.xml => _c_logfiles_cello_ALARM_LOG.xml 
			my $nodeName=$self->{nodeInfo}->{itk_nodename};
			my $destFileName = $fileName;
			$destFileName =~ tr/\//_/;
			my $localPath="$storedir/$nodeName/$filetype";
			my $localFile = "$localPath/$destFileName";
			
			eval {
				# Make the directory if it does not exist
				-d $localPath or mkpath $localPath;
			};
			if ($@) {
				$self->_error("Could not create directory '$localPath': $@");
				next;
			}
			
			unless (defined $ftp->get($fileName, $localFile)) {
				$self->_error("Could not fetch file '$fileName'");
				$fileHist->{result}=FETCH_FAILURE;
				next;
			}
			
			# Got the file. Record the fact and move on
			$chist->{$fileName}->{result}=OK;
			$chist->{$fileName}->{mtime}=$mtimeTs;
			$chist->{$fileName}->{size}=$filestat->{size};
			
			push @fetchedFiles, $localFile;
			$srv->logDebug(5, "Fetched File: $fileName => $localFile") if DEBUG5;
		}
	}
	
	# Store updated collection history for this node / event type
	$self->{collectionHistory}->put($filetype, $chist);

	push @{$self->{fetchedfiles}}, @fetchedFiles;
	return OK, \@fetchedFiles;
}

##-----------------------------------------------------------------------------
# _fetchAllNew($fetchData, $filetype)
# Fetch all files that match the criteria specified in the fetchData, as long as
# the file was not previously fetched
# - $fetchData Entry from fileTable for the file type to be fetched
# - $filetype	Name of a file type as given in the %fileTable
# RETURNS: $resultCode, $fetchedFiles
# - $resultCode	  Result Code - see constants for definitions
# - $fetchedFiles Ref to array, one entry for each file fetched. The entries contain 
#                 the local path to the fetched file. May be undef on failure.
#------------------------------------------------------------------------------
sub _fetchAllNew {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fetchData, $filetype)=@_;
	my $ftp = $self->{ftp};
	
	# Get the collection history for this file type
	# If undef was returned, ||{} will make an empty hash
	my $chist = $self->{collectionHistory}->get($filetype) || {};
	
	# List of fetched files
	my @fetchedFiles;
	my %foundFiles;
	
	# So that we can handle subdirectories, we perform the search within
	# an inner sub that we can call recursively
	# Note: To allow a recursive anonymous sub, we have to pass the sub
	# reference down as well. See: http://www.perlmonks.org/?node_id=614658
	my $fetchSub = sub {
		my ($recurse, $dir, $dirmatch, $filematch) = @_;
		$srv->logDebug(5, "Scanning Directory: $dir") if DEBUG5;
		my $matchtype = ref $filematch eq 'Regexp'; # Do this here so it does not need to be done each time through the loop
		
		my $files = $ftp->dirStat($dir);
		return unless defined $files;
		foreach my $filestat (@{$files}) {
			$srv->logDebug(6, "Directory Entry: '$filestat->{name}' (IsDir=$filestat->{isdir})") if DEBUG6;
			if ($filestat->{isdir}) {
				# Skip subdir if we have specified a directory matching regex and it does not match
				next if $dirmatch && $filestat->{name} !~ $dirmatch;
				next if $filestat->{name} =~ /^\.\.?$/;
				my $subdir = "$dir/$filestat->{name}";
				# Recursively call self to handle contents
				&$recurse($recurse, $subdir, $dirmatch, $filematch);
			} else {
				# Check if file matches the supplied criteria
				if ($matchtype) { # Regex match
					next unless $filestat->{name} =~ $filematch;
				} else { # Straight Comparison
					next unless $filestat->{name} eq $filematch;
				}
				
				my $mtimeTs = $filestat->{mtime};
				if (! defined $mtimeTs) {
					$self->_error("Invalid file modification time for $filestat->{name} - skipping file");
					next;
				}
				$srv->logDebug(5, "Found file: '$filestat->{name}' mtime='$filestat->{mtime}' size=$filestat->{size}") if DEBUG5;
				
				# Check if the file has been fetched previously
				my $fileName = "$dir/$filestat->{name}";
				$foundFiles{$fileName}=1;
				
				if (exists $chist->{$fileName}) {
					$srv->logDebug(5, "Skip file $fileName: It was fetched previously") if DEBUG5;
					next;
				}
				
				if (defined $self->{startdate} && $self->{startdate}>$mtimeTs) {
					# File has not been modified after the starttime filter.
					# We don't need to fetch the file, but we must record it in collection history
					# so that it does not get considered in future
					$srv->logDebug(5, "Skipping file: $fileName because it is older than the startdate") if DEBUG5;
					$chist->{$fileName}=SKIPPED;
					next;
				}
					
				# File needs fetching!
				
				# When storing file locally, need to guard against
				# collisions, in case there are different versions of the file stored under
				# different directories (avlog, alarmlog are like this)
				# Take the full node path and convert to a file name
				# by making the / = _
				# eg. /c/logfiles/cello/ALARM_LOG.xml => _c_logfiles_cello_ALARM_LOG.xml 
				my $nodeName=$self->{nodeInfo}->{itk_nodename};
				my $destFileName = $fileName;
				$destFileName =~ tr/\//_/;
				my $localPath="$storedir/$nodeName/$filetype";
				my $localFile = "$localPath/$destFileName";
				
				eval {
					# Make the directory if it does not exist
					-d $localPath or mkpath $localPath;
				};
				if ($@) {
					$self->_error("Could not create directory '$localPath': $@");
					next;
				}
				
				unless (defined $ftp->get($fileName, $localFile)) {
					$self->_error("Could not fetch file '$fileName'");
					$chist->{$fileName}=FETCH_FAILURE;
					next;
				}
				
				# Got the file. Record the fact and move on
				$chist->{$fileName}=OK;
				push @fetchedFiles, $localFile;
				$srv->logDebug(5, "Fetched File: $fileName => $localFile") if DEBUG5;
			}
		}
	};
	
	# An error in previous path definitions resulted in ecda/gcpu files being stored without the leading '/'.
	# This is corrected here if present for any entries in the collection history data.
	# NB this can be removed after eventdata restructure
	foreach (keys %$chist) {
		next unless m|^c/|;
		my $fixedPath = '/'.$_;
		$chist->{$fixedPath} = $chist->{$_};
		delete $chist->{$_};
	}

	foreach my $filespec (@{$fetchData->{paths}}) {
		&$fetchSub($fetchSub, $filespec->{dir}, $filespec->{dirmatch}, $filespec->{file});
	}
	
	# In order to make sure that the collection history does not just
	# keep growing, files are removed from it once they are no longer
	# in the node itself
	foreach (keys %{$chist}) {
		delete $chist->{$_} unless exists $foundFiles{$_};
	}
	
	# Store updated collection history for this node / event type
	$self->{collectionHistory}->put($filetype, $chist);
	push @{$self->{fetchedfiles}}, @fetchedFiles;
	return OK, \@fetchedFiles;
}

##-----------------------------------------------------------------------------
# _fetchNewestMatch ($fetchData, $filetype)
# Fetch all files that match the criteria specified in the fetchData, as long as
# the file was not previously fetched
# - $fetchData Entry from fileTable for the file type to be fetched
# - $filetype	Name of a file type as given in the %fileTable
# RETURNS: $resultCode, $fetchedFiles
# - $resultCode	  Result Code - see constants for definitions
# - $fetchedFiles Ref to array, one entry for each file fetched. The entries contain 
#                 the local path to the fetched file. May be undef on failure.
#------------------------------------------------------------------------------
sub _fetchNewestMatch {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fetchData, $filetype)=@_;
	my $ftp = $self->{ftp};
	
	# Get the collection history for this file type
	# If undef was returned, ||{} will make an empty hash
	my $chist = $self->{collectionHistory}->get($filetype) || {};
	
	if (!keys %$chist) {
		$chist = undef;
	}
		
	# List of fetched files
	my @fetchedFiles;

	# So that we can handle subdirectories, we perform the search within
	# an inner sub that we can call recursively
	# Note: To allow a recursive anonymous sub, we have to pass the sub
	# reference down as well. See: http://www.perlmonks.org/?node_id=614658
	my $fetchSub = sub {
		my ($recurse, $dir, $dirmatch, $filematch) = @_;
		
		$srv->logDebug(5, "Scanning Directory: $dir") if DEBUG5;
		my $matchtype = ref $filematch eq 'Regexp'; # Do this here so it does not need to be done each time through the loop
		
		my $files = $ftp->dirStat($dir);
		return unless defined $files;
				
		foreach my $filestat (@{$files}) {
			$srv->logDebug(6, "Directory Entry: '$filestat->{name}' (IsDir=$filestat->{isdir})") if DEBUG6;
			if ($filestat->{isdir}) {
				# Skip subdir if we have specified a directory matching regex and it does not match
				next if $dirmatch && $filestat->{name} !~ $dirmatch;
				my $subdir = "$dir/$filestat->{name}";
				# Recursively call self to handle contents
				&$recurse($recurse, $subdir, $dirmatch, $filematch);
				
			} else {
				# Check if file matches the supplied criteria
				if ($matchtype) { # Regex match
					next unless $filestat->{name} =~ $filematch;
				} else { # Straight Comparison
					next unless $filestat->{name} eq $filematch;
				}
				
				my $mtimeTs = $filestat->{mtime};
				if (! defined $mtimeTs) {
					$self->_error("Invalid file modification time for $filestat->{name} - skipping file");
					next;
				}
				
				$srv->logDebug(5, "Found file: '$filestat->{name}' mtime='$filestat->{mtime}' size=$filestat->{size}") if DEBUG5;
				
				# Check if the file has been fetched previously
				my $fileName = "$dir/$filestat->{name}";
				
				my $fileFound =  {'name'=>$fileName, 'mtime'=>$filestat->{mtime}, 'size'=>$filestat->{size}};
				
				
				if (defined $self->{startdate} && $self->{startdate}>$mtimeTs) {
					# File has not been modified after the starttime filter.
					# We don't need to fetch the file, but we must record it in collection history
					# so that it does not get considered in future
					$srv->logDebug(5, "Skipping file: $fileName because it is older than the startdate") if DEBUG5;
					next;
				}
								
				return 	$fileFound;	
			}
		
		}
		return undef;
	};
	
	my $newestFile = $chist;
	#
	# for each file to fecth, find the newest
	foreach my $filespec (@{$fetchData->{paths}}) {
		# try to find a file
		my $fileFound = &$fetchSub($fetchSub, $filespec->{dir}, $filespec->{dirmatch}, $filespec->{file});
		# if the file is found.		
		if (defined $fileFound )  {
			# if there no file called
			if (!defined ($newestFile) ) {
				# assign the file found to newest
				$newestFile = $fileFound;
			} else {
				# check the the file found is the newest.
				if ($fileFound->{time} > $newestFile->{time} ) {
					$newestFile = $fileFound;
				}
			}
		}
		$srv->logDebug(6, "newest file is :" . (defined $newestFile ? $newestFile : '(none)') ) if DEBUG6;
	}
	
	if (!defined ($newestFile)) {
		$srv->logWarn("No new files found for type '$filetype'");
		return undef;
	}
	
	# fetch the file
	my $nodeName=$self->{nodeInfo}->{itk_nodename};
	my $destFileName = $newestFile->{name};
	$destFileName =~ tr/\//_/;
	my $localPath="$storedir/$nodeName/$filetype";
	my $localFile = "$localPath/$destFileName";
				
	eval {
		# Make the directory if it does not exist
		-d $localPath or mkpath $localPath;
	};
	if ($@) {
		$self->_error("Could not create directory '$localPath': $@");
		return undef
	}
				
	unless (defined $ftp->get($newestFile->{name}, $localFile)) {
		$self->_error("Could not fetch file '$newestFile->{name}'");
		$chist->{$newestFile->{name}}{fetch}=FETCH_FAILURE;
		next;
	}
				
	# Got the file. Record the fact and move on
	$chist->{$newestFile->{name}}{fetch}=OK;
	push @fetchedFiles, $localFile;
	$srv->logDebug(5, "Fetched File: $newestFile->{name} => $localFile") if DEBUG5;

	$chist = $newestFile;
	
	# Store updated collection history for this node / event type
	$self->{collectionHistory}->put($filetype, $chist);
	push @{$self->{fetchedfiles}}, @fetchedFiles;
	return OK, \@fetchedFiles;
}







#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new($collectionHistory, $nodeInfo)
# Constructor
# - $collectionHistory	Collection history object
# - $nodeInfo	Ref to a hash containing information about the node to be
#		processed. The following entries are expected in the hash:
#		 * username     => node username
#		 * passwd       => node password
#		 * ip_address   => node ip address
#		 * itk_nodename => Node unique name or equivalent
#		 * transferMethod => ftp or sftp transfer method
# % startdate 	Starting date as a unix epoch timestamp; PMD files earlier than this will not be fetched
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self,$collectionHistory,$nodeInfo,%opts) = @_;
	lock_keys(%opts, qw/startdate localdir/);
	
	$self=fields::new($self) unless ref $self;

	# collectionHistory object is mandatory - needed to store node specifics
	$self->{collectionHistory} = $collectionHistory 
		or die "constructor >> No collectionHistory Object received, aborting as we dont know how to keep track of the log collection results\n";
	$self->{nodeInfo}=$nodeInfo
		or die "NodeInfo is a mandatory param for FileFetcher Constructor\n";
		
	$self->{startdate}=$opts{startdate};
	$self->{fetchedfiles}=[];
	
	return $self;
}

##-----------------------------------------------------------------------------
# login()
# Log in to the node specified by $nodeInfo
# RETURNS: $result
# - $result	Result code: See constants for definitions
#------------------------------------------------------------------------------
sub login {
	$srv->logEnter(\@_) if ENTER;
	my ($self) = @_;

	my $nodeInfo=$self->{nodeInfo};
	return INVALID_STATE if $self->{ftp};
	
	#-------------------------------------------------------------------------------------------
	# initiate file transfer session. Return immediately unless we get a valid object returned
	#-------------------------------------------------------------------------------------------
	my $transferClassName = ($nodeInfo->{transferMethod} eq 'sftp') ? "Itk::SFTP" : "Itk::FTP";
	$srv->logDebug(5, "Attempt to connect to node '$nodeInfo->{itk_nodename}' ($nodeInfo->{ip_address})") if DEBUG5;
	my $ftp = $transferClassName->new(
		host => $nodeInfo->{ip_address}, 
		port => $nodeInfo->{port} ? $nodeInfo->{port} : undef,
		timeout => $timeout 
	);
	
	return CONNECT_FAILURE unless defined $ftp;
	
	#-------------------------------------------------------------------------------------------
	# login to the remote node
	#-------------------------------------------------------------------------------------------
	$srv->logDebug(5, "Attempt to login to node '$nodeInfo->{itk_nodename}' username = '$nodeInfo->{username}', password='$nodeInfo->{passwd}'") if DEBUG5;
	my $loginResult = $ftp->login($nodeInfo->{username} ,$nodeInfo->{passwd});
	
	return LOGIN_FAILURE unless (defined $loginResult);
	
	$self->{ftp}=$ftp;
	return OK;
}
##-----------------------------------------------------------------------------
# logout()
# Log out and disconnect from the currently connected node
#------------------------------------------------------------------------------
sub logout {
	$srv->logEnter(\@_) if ENTER;
	my ($self)=@_;
	return undef unless defined $self->{ftp};
	$self->{ftp}->quit();
	$self->{ftp}=undef;
}

##-----------------------------------------------------------------------------
# fetch($filetype)
# Fetch file(s) specified by $filetype
# - $filetype	Name of a file type as given in the %fileTable
# RETURNS: $resultCode, $fetchedFiles
# - $resultCode	  Result Code - see constants for definitions
# - $fetchedFiles Ref to array, one entry for each file fetched. The entries contain 
#                 the local path to the fetched file. May be undef on failure.
#------------------------------------------------------------------------------
sub fetch {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $filetype) = @_;
	return INVALID_STATE unless $self->{ftp};
	return INVALID_PARAMETERS unless defined $filetype and exists $fileTable{$filetype};
	my $fetchData = $fileTable{$filetype};
	
	# Dispatch to correct sub to handle the fetching method
	my $fetchmethod = $fetchData->{method};
	if ($fetchmethod == COLLECT_ALL_MATCHING) {
		return $self->_fetchAllMatching($fetchData, $filetype);
	} elsif ($fetchmethod == COLLECT_PMDS) {
		return $self->_fetchPmds($fetchData, $filetype);
	} elsif ($fetchmethod == COLLECT_ALL_NEW) {
		return $self->_fetchAllNew($fetchData, $filetype);
	} elsif ($fetchmethod == COLLECT_NEWEST_MATCH) {
	    return $self->_fetchNewestMatch ($fetchData, $filetype);
	} else {
		$self->_error("Fetch Method $fetchmethod not implemented!");
		return INTERNAL_ERROR, undef; 
	}
}

##-----------------------------------------------------------------------------
# fetchLogFiles(%opts)
# FetchLogFiles
# % filetypes	Ref to array of file types which are to be fetched
# RETURNS: $rcode, $rtable
# - $resultCode	  Result Code - see constants for definitions
# - $rtable	Ref to HoH result tables. Outer hash key is the filetype. Inner
#		hash key is:
#		 * rcode - Result code (see constants)
#		 * time - Time of file fetching (epoch secs)
#		 * files - ref to array of filenames for each fetched file
#------------------------------------------------------------------------------
sub fetchLogFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self,%opts) = @_;
	lock_keys(%opts, qw/filetypes noping/);
	
	$srv->logDebug(4, "Requested file fetch for: ".join(',',@{$opts{filetypes}})) if DEBUG4;

	my $nodeInfo=$self->{nodeInfo};
	
	# check if noping option was set.
	if (!$opts{noping}) {	
		my $pingcmd = getPingCommand();

		if (system("$pingcmd $nodeInfo->{ip_address} >/dev/null 2>&1")) {
			#unless ($pinger->ping($opts{nodeInfo}{ip_address},2)) {
			$srv->logWarn("Could not ping $nodeInfo->{itk_nodename} (ip=$nodeInfo->{ip_address})");
			return PING_FAILURE;
		}
		$srv->logDebug(4, "ping option is ON while fetching files for $nodeInfo->{ip_address}") if DEBUG4;
	} else {
		$srv->logDebug(4, "ping option is OFF while fetching files for $nodeInfo->{ip_address}") if DEBUG4;
	}
	
	my $rcode;
	if ($rcode = $self->login() != OK) {
		$self->_error("Failed to log into node '$nodeInfo->{itk_nodename}' (rcode=$rcode)");
		return $rcode;
	}
	
	my (
		%rTable, # Table of results for each selected type
		$rcode,  # Result Code to return at end
	);
	
	foreach my $type (@{$opts{filetypes}}) {
		($rTable{$type}{rcode}, $rTable{$type}{files}) = $self->fetch($type);
		$rTable{$type}{time}=time;
		if (defined $rTable{$type}{files} && @{$rTable{$type}{files}}) {
			$rcode = OK ;
		}
	}
	
	$self->logout();
	
	$rcode = OK_NONEWFILES unless defined $rcode; # If this was not already set, that means we did not find any new files
	
	return OK, \%rTable;
}
##-----------------------------------------------------------------------------
# removeFiles()
# Delete all files that were fetched by this instance
#------------------------------------------------------------------------------
sub removeFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self) = @_;
	
	foreach (@{$self->{fetchedfiles}}) {
		next unless -e $_;
		$srv->logDebug(5, "Removing file: $_") if DEBUG5;
		unlink $_ or $srv->logError("Error deleting file '$_': $!");
	}
	my $nodeName=$self->{nodeInfo}->{itk_nodename};
	return unless -d "$storedir/$nodeName";
	foreach (<$storedir/$nodeName/*>,"$storedir/$nodeName") {
		$srv->logDebug(5, "Removing directory: $_") if DEBUG5;
		-d and (rmdir or $srv->logError("Could not remove directory '$_': $!"));
	}
}

##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	my $self=shift;
	# Do destruction!!
}

# Class destruction
#END {
#	$pinger->close();
#}


package Itk::EventData::LocalFileFetcher;

use base 'Itk::EventData::FileFetcher';
use strict;
use File::Copy;
use Hash::Util qw(lock_keys);
use File::Path;
use File::stat;
use Time::Local qw(timegm_nocheck);
BEGIN {
	if (DEBUG6) {
		require Data::Dumper;
		import Data::Dumper;
	}
}

#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	# Result Codes
	OK => 			1,	# Operation succeeded
	OK_NONEWFILES =>	2,	# Operation succeeded, no new files were found
	SKIPPED =>		3,	# File was skipped
	PING_FAILURE =>		-10,	# Cannot even ping the node
	CONNECT_FAILURE => 	-11,	# Failure to connect to node
	LOGIN_FAILURE => 	-12,	# Failed to log in to node
	INVALID_STATE => 	-13,	# State is invalid for requested operation
	INVALID_PARAMETERS => 	-14,	# Bad parameters passed to a request
	FETCH_FAILURE => 	-15,	# Failure to fetch a file
	LOCAL_FILE_FAILURE =>	-16,	# Failure in storing / creating local file or directory
	INTERNAL_ERROR =>	-99,	# Internal error
	# File Fetching Methods
	COLLECT_FIRST_MATCH => 1,	# Collect the first file that matches (if modified since last run)
	COLLECT_NEWEST_MATCH => 2,	# Collect the file that has the newest timestamp (if modified since last run)
	COLLECT_ALL_MATCHING => 3,	# Collect all matching files (if modified since last run)
	COLLECT_PMDS => 4,		# Special method for collecting PMDs
	COLLECT_ALL_NEW => 5,		# Fetch all files not previously fetched
	# Others
	COMMAND_TIMEOUT => 40,		# Amount of time allowed for completion of cello commands
};


##-----------------------------------------------------------------------------
# fetch($filetype)
# Fetch file(s) specified by $filetype
# - $filetype	Name of a file type as given in the %fileTable
# RETURNS: $resultCode, $fetchedFiles
# - $resultCode	  Result Code - see constants for definitions
# - $fetchedFiles Ref to array, one entry for each file fetched. The entries contain 
#                 the local path to the fetched file. May be undef on failure.
#------------------------------------------------------------------------------
sub fetch {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $filetype) = @_;
	return INVALID_PARAMETERS unless defined $filetype and exists $fileTable{$filetype};
	my $fetchData = $fileTable{$filetype};
	
	# Dispatch to correct sub to handle the fetching method
	my $fetchmethod = $fetchData->{method};
	
	$srv->logDebug(4, "Fetch method is  $fetchmethod ") if DEBUG4;
	
	if ($fetchmethod == COLLECT_ALL_MATCHING) {
		return $self->_localFetchAllMatching($fetchData, $filetype);
	} elsif ($fetchmethod == COLLECT_PMDS) {
		return $self->_localFetchPmds($fetchData, $filetype);
	} elsif ($fetchmethod == COLLECT_ALL_NEW) {
		return $self->_localFetchAllNew($fetchData, $filetype);
	} elsif ($fetchmethod == COLLECT_NEWEST_MATCH) {
		return $self->_localFetchAllNew($fetchData, $filetype);
	} else {
		$self->_error("Fetch Method $fetchmethod not implemented!");
		return INTERNAL_ERROR, undef; 
	}
}




##-----------------------------------------------------------------------------
# _localFetchAllNew($fetchData, $filetype)
# Fetch all files that match the criteria specified in the fetchData, as long as
# the file was not previously fetched
# - $fetchData Entry from fileTable for the file type to be fetched
# - $filetype	Name of a file type as given in the %fileTable
# RETURNS: $resultCode, $fetchedFiles
# - $resultCode	  Result Code - see constants for definitions
# - $fetchedFiles Ref to array, one entry for each file fetched. The entries contain 
#                 the local path to the fetched file. May be undef on failure.
#------------------------------------------------------------------------------
sub _localFetchAllNew {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fetchData, $filetype)=@_;


	# Get the collection history for this file type
	# If undef was returned, ||{} will make an empty hash
	my $chist = $self->{collectionHistory}->get($filetype) || {};

    my %foundFiles;
	$srv->logDebug(5, "_localFetchAllNew: fetchData: $fetchData") if DEBUG5;
	$srv->logDebug(5, "_localFetchAllNew: filetype: $filetype") if DEBUG5;
	
	# List of fetched files
	my @fetchedFiles;
	
		
	# So that we can handle subdirectories, we perform the search within
	# an inner sub that we can call recursively
	# Note: To allow a recursive anonymous sub, we have to pass the sub
	# reference down as well. See: http://www.perlmonks.org/?node_id=614658
	my $fetchSub = sub {
		my ($filematch) = @_;
		
		$srv->logDebug(5, "filematch is : $filematch") if DEBUG5;
				
		my $localdir = $self->{localdir};
		opendir(DIR, $localdir) || die "can't opendir $localdir: $!";
		my @files = readdir(DIR);
		closedir DIR;
		
		return unless defined @files;
		
		
		foreach my $filename (@files) {

		   if ($filename =~ /^\.+$/) {
		   	  next;
		   }	
			
		   my $f = "$localdir/$filename";
           my $sb = stat($f);
		
		    
		
			# ignore file not in the 
			if (!($filename =~ /$filematch/) ) {
				next;
			}

			$srv->logDebug(5, "Found file: '$filename' mtime='$sb->mtime' size=$sb->size") if DEBUG5;

			
			# Check if the file has been fetched previously
			$foundFiles{$filename}=1;
					
			if (exists $chist->{$filename}) {
				$srv->logDebug(5, "Skip file $filename: It was fetched previously") if DEBUG5;
				next;
			}
				
			if (defined $self->{startdate} && $self->{startdate}>$sb->mtime) {
				# File has not been modified after the starttime filter.
				# We don't need to fetch the file, but we must record it in collection history
				# so that it does not get considered in future
				$srv->logDebug(5, "Skipping file: $filename because it is older than the startdate") if DEBUG5;
					$chist->{$filename}=SKIPPED;
					next;
			}
		     						
			# File needs fetching!
				
			# When storing file locally, need to guard against
			# collisions, in case there are different versions of the file stored under
			# different directories (avlog, alarmlog are like this)
			# Take the full node path and convert to a file name
			# by making the / = _
			# eg. /c/logfiles/cello/ALARM_LOG.xml => _c_logfiles_cello_ALARM_LOG.xml 
			my $nodeName=$self->{nodeInfo}->{itk_nodename};
			my $destFileName = $filename;
			$destFileName =~ tr/\//_/;
				
			my $localPath="$storedir/$nodeName/$filetype";
			my $localFile = "$localPath/$destFileName";
				
			eval {
				# Make the directory if it does not exist
				-d $localPath or mkpath $localPath;
			};
			if ($@) {
				$self->_error("Could not create directory '$localPath': $@");
				next;
			}
			
			my $newFileName = "$localdir/$filename";
			$srv->logDebug(6, "About to Copy $newFileName to $localFile ") if DEBUG6;	
			my $retval = copy ($newFileName, $localFile);
			if ($retval != 1) {
				$self->_error("Could not copy file '$newFileName' because $!");
				$chist->{$filename}=FETCH_FAILURE;
				next;
			}
			
			# no delete
			if (!$self->{nodelete}) {
			   unlink $newFileName;
			}
				
			# Got the file. Record the fact and move on
			$chist->{$filename}=OK;
			push @fetchedFiles, $localFile;
			$srv->logDebug(5, "File copied: $filename => $localFile") if DEBUG5;
		}
	};
	
	foreach my $filespec (@{$fetchData->{paths}}) {

    	$srv->logDebug(5, "file: ". $filespec->{file}) if DEBUG5;
	
		&$fetchSub($filespec->{file});
	}
	
	# In order to make sure that the collection history does not just
	# keep growing, files are removed from it once they are no longer
	# in the node itself
	foreach (keys %{$chist}) {
		delete $chist->{$_} unless exists $foundFiles{$_};
	}
	
	# Store updated collection history for this node / event type
	$self->{collectionHistory}->put($filetype, $chist);
	
	push @{$self->{fetchedfiles}}, @fetchedFiles;
	return OK, \@fetchedFiles;
}

##-----------------------------------------------------------------------------
# _localFetchAllMatching($fetchData, $filetype)
# Fetch all files that match the criteria specified in the fetchData, as long as
# the file was modified more recently than the last time this was run
# - $fetchData Entry from fileTable for the file type to be fetched
# - $filetype	Name of a file type as given in the %fileTable
# RETURNS: $resultCode, $fetchedFiles
# - $resultCode	  Result Code - see constants for definitions
# - $fetchedFiles Ref to array, one entry for each file fetched. The entries contain 
#                 the local path to the fetched file. May be undef on failure.
#------------------------------------------------------------------------------
sub _localFetchAllMatching {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fetchData, $filetype)=@_;
	
	
	$srv->logDebug(5, "_localFetchAllMatching: fetchData: $fetchData") if DEBUG5;
	$srv->logDebug(5, "_localFetchAllMatching: filetype: $filetype") if DEBUG5;


	# Get the collection history for this file type
	# If undef was returned, ||{} will make an empty hash
	my $chist = $self->{collectionHistory}->get($filetype) || {};

	
	# List of fetched files
	my @fetchedFiles;

	foreach my $filespec (@{$fetchData->{paths}}) {
		
		my $localdir = $self->{localdir};
		my $filematch = $filespec->{file};
		my $matchtype = ref $filematch eq 'Regexp'; # Do this here so it does not need to be done each time through the loop
		
		opendir(DIR, $localdir) || die "can't opendir $localdir: $!";
		my @files = readdir(DIR);
		closedir DIR;

 	    $srv->logDebug(6, "_localFetchAllMatching: looking for $filematch") if DEBUG5;

		next unless defined @files;
		foreach my $filename (@files) {
			
		   if ($filename =~ /^\.+$/) {
		   	  next;
		   }	
									
		   my $f = "$localdir/$filename";
           my $sb = stat($f);
                     							
			# Check if file matches the supplied criteria
			if ($matchtype) { # Regex match
				next unless $filename =~ $filematch;
			} else { # Straight Comparison
				next unless $filename eq $filematch;
			}
			
			$srv->logDebug(5, "Found file: '$filename' mtime=$sb->mtime size=$sb->size") if DEBUG5;

			
			# Check if the file has been modified since last fetch.
			# Modification times are stored in the collection history
			# as filetype => <name> => mtime = <mtime>
			# If the file does not exist in the mtime hash then
			# we have
			my $fileHist;
			if (exists $chist->{$filename}) {
				$fileHist = $chist->{$filename}; # History of this particular file
				if ($fileHist->{mtime} eq $sb->mtime && $fileHist->{size} == $sb->size) {
					$srv->logDebug(5, "File not modified since last run. Skipping") if DEBUG5;
					next;
				}
			} else {
				$fileHist = {};
			}
			
			if (defined $self->{startdate} && $self->{startdate}>$sb->mtime) {
				# File has not been modified after the starttime filter.
				# We don't need to fetch the file, but we must record it in collection history
				# so that it does not get considered in future
				$srv->logDebug(5, "Skipping file: $filename because it is older than the startdate") if DEBUG5;
				$fileHist = {
					result=>SKIPPED,
					mtime => $sb->mtime,
					size  => $sb->size,
				};
				$chist->{$filename}=$fileHist;
				next;
			}
			
			# File needs fetching!
			
			
			# When storing file locally, need to guard against
			# collisions, in case there are different versions of the file stored under
			# different directories (avlog, alarmlog are like this)
			# Take the full node path and convert to a file name
			# by making the / = _
			# eg. /c/logfiles/cello/ALARM_LOG.xml => _c_logfiles_cello_ALARM_LOG.xml 
			my $nodeName=$self->{nodeInfo}->{itk_nodename};
			my $destFileName = $filename;
			$destFileName =~ tr/\//_/;
			
			my $localPath="$storedir/$nodeName/$filetype";
			my $localFile = "$localPath/$destFileName";
						
			eval {
				# Make the directory if it does not exist
				-d $localPath or mkpath $localPath;
			};
			if ($@) {
				$self->_error("Could not create directory '$localPath': $@");
				next;
			}

			my $fullname = "$localdir/$filename";
			
			$srv->logDebug(6, "_localFetchAllMatching: about to copy $fullname to $localFile") if DEBUG5;
			
			my $retval = copy ($fullname, $localFile);
			if ($retval != 1) {
				$self->_error("Could not copy file '$fullname' because $!");
				next;
			}
						
			# Got the file. Record the fact and move on
			$fileHist->{result}=OK;
			$fileHist->{mtime}=$sb->mtime;
			$fileHist->{size}=$sb->size;				
			push @fetchedFiles, $localFile;
			$srv->logDebug(5, "Fetched File: $fullname => $localFile") if DEBUG5;
			
			# delete the original file.
			if (!$self->{nodelete}) {
				$srv->logDebug(6, "_localFetchAllMatching: $fullname is deleted !") if DEBUG5;
				unlink $fullname;
			}
			
		}
	}
	
	# Store updated collection history for this node / event type
	#$self->{collectionHistory}->put($filetype, $chist);
	push @{$self->{fetchedfiles}}, @fetchedFiles;
	return OK, \@fetchedFiles;
}

##-----------------------------------------------------------------------------
# _localFetchPmds($fetchData, $filetype)
# Fetch all PMDs that were generated later than the last time we ran
# - $fetchData Entry from fileTable for the file type to be fetched
# - $filetype	Name of a file type as given in the %fileTable
# RETURNS: $resultCode, $fetchedFiles
# - $resultCode	  Result Code - see constants for definitions
# - $fetchedFiles Ref to array, one entry for each file fetched. The entries are 
#                 hashes with the following entries:
#                  * localfile => the local path to the fetched file.
#		   * file => original filename on node
#                  * id => pmd id
#                  * board => board that generated the PMD
#------------------------------------------------------------------------------
sub _localFetchPmds {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fetchData, $filetype)=@_;

	$srv->logDebug(5, "_localFetchPmds: fetchData: $fetchData") if DEBUG5;
	$srv->logDebug(5, "_localFetchPmds: filetype: $filetype") if DEBUG5;

	# get node name
	my $nodeName=$self->{nodeInfo}->{itk_nodename};


	# Get the envelope number of the last stored PMD on the node
	my $lastPmdNo = $self->{collectionHistory}->get('last_pmd_num');
	$srv->logDebug(4, "$nodeName : Max PMD number on last run: $lastPmdNo") if DEBUG4;

    # get local dir
	my $localdir = $self->{localdir};

	# The file pmd.data contains the min and max envelope numbers
	# that have been generated in the node

	open(DAT, "$localdir/pmd.data") || die("Could not open file $localdir/pmd.data because $!");
	my @pmdinfo=<DAT>;
	close(DAT); 
		
	my $currentMaxPmd = $pmdinfo[1];
	my $currentMinPmd = $pmdinfo[0];
	
	if ($currentMinPmd =~ /^Could not open file/) {  # In case someone (eg OSS) has deleted the pmd.data
		undef $currentMinPmd;
		undef $currentMaxPmd;
		$srv->logDebug(5, "$nodeName : pmd.data was not found.") if DEBUG5;
	}
		
	if (! defined $currentMaxPmd || $lastPmdNo > $currentMaxPmd) {
		undef $lastPmdNo;
	}
		
	$srv->logDebug(5, "$nodeName : Last Pmd No Fetched was: $lastPmdNo; current min = $currentMinPmd; current max = $currentMaxPmd") if DEBUG5;

	if (defined $currentMinPmd && defined $lastPmdNo && $currentMaxPmd == $lastPmdNo) {
		$srv->logDebug(4, "$nodeName : No new PMDs since last run.") if DEBUG4;
		return OK,undef;
	} 
		
	my $datafile="$localdir/pmd_dump_list.txt";
	$srv->logDebug(6, "About ot open the ") if DEBUG6;
	open(DAT, $datafile) || die("Could not open file $datafile because $!");
	my @dumplist=<DAT>;
	close(DAT); 
		
	my @PmdsToFetch;
	my $newMaxPmdNo;
	my %pmdInfo;
	my $pdmfilename;
	
	foreach (@dumplist) {
		my ($pmdTime, $pmdFile, $pmdNo, $board, $pmdId);
		if (($pmdTime, $pmdFile, $pmdNo,$board, $pmdId) = (/(\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}.\d+)\s+[\w-]+\s+[\w-]+\s+(\/c\/pmd\/(\d+)\/(\d{4})\d{2}\/(0x[0-9a-f]+)\.pmd)/)) {
			
			$board="${board}00"; # Because board number given in pmd can include the SPM executionResourceNumber, which we discard
			
			$srv->logDebug(6, "Found PMD with t=$pmdTime, file=$pmdFile, pmdno=$pmdNo, board=$board, pmdid=$pmdId") if DEBUG6;
			$pdmfilename = $pmdNo."_".$board."_".$pmdId.".pmd";

			if (defined $lastPmdNo && defined $currentMinPmd && $pmdNo >= $currentMinPmd && $pmdNo <= $lastPmdNo) {
				$srv->logDebug(5, "$nodeName : Pmd $pmdFile is too old. Lets chuck it in the bin.") if DEBUG5;
				next;
			}
			
			$newMaxPmdNo=$pmdNo unless defined $newMaxPmdNo && $newMaxPmdNo > $pmdNo;

			if (defined $self->{startdate}) {
				my ($yy,$mo,$dd,$hh,$mm,$ss) = ($pmdTime=~/(^\d{4})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/);
				my $ts;
				# Dump list sometimes has unreasonable dates & times - need to trap errors
				eval {
					$ts=timegm_nocheck($ss,$mm,$hh,$dd,$mo-1,$yy);
				};
				if ($@) {
					$srv->logDebug(4, "Invalid date in dump list: $pmdTime. Discarding this PMD.") if DEBUG5;
					next;
				}
				if ($self->{startdate} > $ts) {
					$srv->logDebug(5, "Pmd $pmdFile is older than startdate. Discarding.") if DEBUG5;
					next;
				}
			}
			
			$srv->logDebug(6, "Pmd $pmdFile will be fetched.") if DEBUG4;
			push @PmdsToFetch, {
				file => $pdmfilename,
				id => $pmdId,
				board => $board,
				pmdno => $pmdNo
			};
		} else {
			$srv->logDebug(4,"couldn't get pmdtime, pmdfile, pmdNo, board, pmdid with: $_ ");
		}
	}

    # If we did not pick up any PMDs from the last run, newMaxPmdNo will be undef   
    # In that case use the max achieved at last run
    $newMaxPmdNo ||= $lastPmdNo;   
    $srv->logDebug(4,"new Max Pmd number: $newMaxPmdNo");
      
     if ($currentMaxPmd ne $newMaxPmdNo) {   
           # The maximum pmd number in the pmd.dat file on the node is not   
           # the same as the largest pmd number in the node. Probably some kind of error   
           # We just give a warning and use the latter   
           $self->_warn("$nodeName : Max Pmd number in pmd.dat ($currentMaxPmd) is not the same as the largest PMD Number ($newMaxPmdNo)");   
     }   


	
	my $nodeName=$self->{nodeInfo}->{itk_nodename};
	
	my $localPath="$storedir/$nodeName/pmd";
	eval {
		# Make the directory if it does not exist
		-d $localPath or mkpath $localPath;
	};
	if ($@) {
		$self->_error("Could not create directory '$localPath': $@");
		return LOCAL_FILE_FAILURE;
	}

	my @fetchedPmdFiles;

	foreach my $pmdInfo (@PmdsToFetch) {
		my $pmdFile = "$localdir/$pmdInfo->{file}";
		
		
		# Note: PMD ID is not necessarily unique per board. On an SP board
		# each SPM has it's own ID numbers.
		my $localFile = "$localPath/$pmdInfo->{file}";

		$srv->logDebug(4,"About to copy $pmdFile to $localFile ");
		
		if (-e $pmdFile ) {
			my $retval = copy($pmdFile,$localFile);
			if ($retval != 1) {
				$self->_error("Could not copy $pmdFile to $localFile : $! ");
				next;
			}
			sleep (1);
		} else {
	        $self->_warn("file $pmdFile is missing ! ");
	        next;
		}
		
		##delete the original file
		if (!$self->{nodelete}) {
			unlink $pmdFile;
			$srv->logDebug(4," _localFetchPmds: $pmdFile is deleted ! ");
		}
		
		$pmdInfo->{localfile} = $localFile;
		push @fetchedPmdFiles, $pmdInfo;
		push @{$self->{fetchedfiles}}, $localFile;
		$srv->logDebug(5, "Fetched: $pmdFile => $localFile") if DEBUG5;
		
	}
	     
    # Update persistant data   
    $self->{collectionHistory}->put('last_pmd_num', $newMaxPmdNo);   
	
	return OK, \@fetchedPmdFiles;
}

##-----------------------------------------------------------------------------
# fetchLogFiles(%opts)
# FetchLogFiles
# % filetypes	Ref to array of file types which are to be fetched
# RETURNS: $rcode, $rtable
# - $resultCode	  Result Code - see constants for definitions
# - $rtable	Ref to HoH result tables. Outer hash key is the filetype. Inner
#		hash key is:
#		 * rcode - Result code (see constants)
#		 * time - Time of file fetching (epoch secs)
#		 * files - ref to array of filenames for each fetched file
#------------------------------------------------------------------------------
sub fetchLogFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self,%opts) = @_;
	lock_keys(%opts, qw/filetypes nodelete/);
	
	$srv->logDebug(4, "Requested file fetch for: ".join(',',@{$opts{filetypes}})) if DEBUG4;

	my $nodeInfo=$self->{nodeInfo};
	
	$self->{nodelete} = $opts{nodelete} if defined $opts{nodelete};
			
	my (
		%rTable, # Table of results for each selected type
		$rcode,  # Result Code to return at end
	);
	
	foreach my $type (@{$opts{filetypes}}) {
		
		$srv->logDebug(4, "Start Fetching for type $type ") if DEBUG4;
		
		($rTable{$type}{rcode}, $rTable{$type}{files}) = $self->fetch($type);
		$rTable{$type}{time}=time;
		if (defined $rTable{$type}{files} && @{$rTable{$type}{files}}) {
			$rcode = OK ;
		}
	}
			
	return OK, \%rTable;
}

##-----------------------------------------------------------------------------
# init(%opts)
# Class Initialisation. This static method is provided to allow the class to be
# initialised before forking to service each node - this allows initial checks,
# creation of directories if needed, etc to be done once only instead of once
# per node.
# % timeout		Timeout value (default 60)
# % storedir		Root of log storage area
#------------------------------------------------------------------------------
sub init {
	$srv->logEnter(\@_) if ENTER;
	my (%opts) = @_;
	lock_keys(%opts, qw/timeout storedir/);
	
	$timeout = $opts{timeout} || 60;

	#-------------------------------------------------------------------------------------------
	# Set the logstore, that is where the fetched log files will be stored.
	# If the logstore location does not exist, then create that directory 
	#-------------------------------------------------------------------------------------------
	$storedir = $opts{storedir} 
		or die "init >> Invalid storedir setting, aborting as I do not know where to store logfiles"; 
	
	die "Directory '$storedir' does not exist or is not writable or readable" unless (-d $storedir && -r _ && -w _);
	
	return 1;
}


##-----------------------------------------------------------------------------
# new($collectionHistory, $nodeInfo)
# Constructor
# - $collectionHistory	Collection history object
# - $nodeInfo	Ref to a hash containing information about the node to be
#		processed. The following entries are expected in the hash:
#		 * itk_nodename => Node unique name or equivalent
# % startdate 	Starting date as a unix epoch timestamp; PMD files earlier than this will not be fetched
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self,$collectionHistory,$nodeInfo,%opts) = @_;
    $self = __PACKAGE__->SUPER::new($collectionHistory,$nodeInfo,%opts);	
    $self->{localdir} = $opts{localdir}."/".$nodeInfo->{itk_nodename};
 	$srv->logDebug(4, "LocalFileFetcher is instantiated successfully with $opts{localdir} ") if DEBUG4;

    $self->{nodelete} = 0;
	return $self;
}

##-----------------------------------------------------------------------------
# removeFiles()
# Delete all files that were fetched by this instance
#------------------------------------------------------------------------------
sub removeFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self) = @_;
	
	foreach (@{$self->{fetchedfiles}}) {
		next unless -e $_;
		$srv->logDebug(5, "Removing file: $_") if DEBUG5;
		unlink $_ or $srv->logError("Error deleting file '$_': $!");
	}
	my $nodeName=$self->{nodeInfo}->{itk_nodename};
	return unless -d "$storedir/$nodeName";
	foreach (<$storedir/$nodeName/*>,"$storedir/$nodeName") {
		$srv->logDebug(5, "Removing directory: $_") if DEBUG5;
		-d and (rmdir or $srv->logError("Could not remove directory '$_': $!"));
	}
}


#------------------------------------------------------------------------------
1; # Modules always return 1!