#!/usr/bin/perl
###----------------------------------------------------------------------------
#
# Data Formatters.  These are used to transform as the file is being transferred
# within the NITS.
#
# (c) Ericsson AB 2008 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------

package Itk::PmData::DataFormatter; 
use strict;

#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
#	NAME => VALUE,		# Description
};
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
# Reference to global server object
use vars qw($srv);
*srv=\$main::srv;
our $numInstances=0;

my %knownFormatters = (
	#sasn 	=> 'Itk::PmData::DataFormatter::SASN',
	#sgsn    => 'Itk::PmData::DataFormatter::SGSN',
);

#==============================================================================
# CLASS METHODS
#==============================================================================

##S----------------------------------------------------------------------------
# getFormatter($format, %opts)
# Get a formatter for the given node type
# - $format		Formatter name ('sasn','sgsn')
# - %opts		Options which will be passed to the constructor of the formatter
# RETURNS: $formatter
# - $formatter		A formatter instance
#------------------------------------------------------------------------------
sub getFormatter {
	if (ENTER) {
		$srv->logEnter(\@_);
	}
	my ($format, @opts)=@_;
	if (exists $knownFormatters{$format}) {
		return $knownFormatters{$format}->new(@opts);
	} else {
		die "Unknown Format: '$format'\n";
	}
}
##S----------------------------------------------------------------------------
# registerFormatter($format, $class)
# Registers a new formatter class
# - $format		Formatter name
# - $class		Class name (or instance)
# RETURNS: $formatter
# - $formatter		A formatter instance
#------------------------------------------------------------------------------
sub registerFormatter {
	if (ENTER) {
		$srv->logEnter(\@_);
	}
	my ($format, $class)=@_;
	$knownFormatters{$format}=ref $class || $class;
}

#==============================================================================
# PRIVATE METHODS
#==============================================================================

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new(%opts)
# Constructor
#------------------------------------------------------------------------------
sub new {
	if (ENTER) {
		$srv->logEnter(\@_);
	}
	my ($self, %opts)=@_;
	$self=fields::new($self) unless ref $self;

	if (DEBUG9) {
		$numInstances++;
		my @caller = caller(1);
		my $caller="$caller[3] ($caller[1]:$caller[2])";
		$srv->logDebug(9,"NEW $self created by $caller : n=$numInstances");
	}
	return $self;
}

##-----------------------------------------------------------------------------
# postFileFetch($node, $file, $nodedetails)
# Immediately after a file is fetched, the transform converts the format
# suitable for ITK.  Returns 1 on success, else the original file will be redownloaded
# on the next run.
#
# $node 	node name
# $file 	full path to the file 
# $nodedetails 	hashref containing the details about the node if they are known:  
#  % fdn  - Full node FDN
#  % type - the node type
#  % username - the username used to collect the file
#  % passwd - the password used to collect the file
#  % ip_address - ip address/hostname where file collected
# RETURNS: $result
# - $result	$filename for success, undef for failure
#		The user hook will use the new filename
#------------------------------------------------------------------------------
sub postFileFetch {
	#my ($self, $node, $file, $nodedetails)=@_;
	#lock_keys(%opts, qw( ));
	die "Abstract Base Class method called\n"; 
}

##-----------------------------------------------------------------------------
# postNode($node, $dir, $nodedetails)
# Will be called after all files for the node have been fetched.
#
# $node 	node nameis 
# $dir		full directory path to files 
# $nodedetails 	hashref containing the details about the node if they are known:  
#  % fdn  - Full node FDN
#  % type - the node type
#  % username - the username used to collect the file
#  % passwd - the password used to collect the file
#  % ip_address - ip address/hostname where file collected
# RETURNS: $result
# - $result	1 for success, undef for failure
#------------------------------------------------------------------------------
sub postNode {
	#my ($self, $node, $dir, $nodedetails)=@_;
	#lock_keys(%opts, qw( ));
	die "Abstract Base Class method called\n"; 
}

##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	my ($self)=@_;
	if (DEBUG9) {
		$numInstances--;
		$srv->logDebug(9, "DESTROY $self : n=$numInstances");
	}
}

#------------------------------------------------------------------------------
1; # Modules always return 1!
