#!/usr/bin/perl
###----------------------------------------------------------------------------
# Abstract Module for Fetching different files with different protocol such
# as FTP, SFTP.
#
# � Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form without the written
# permission of the copyright owner. The contents are subject to revision without
# notice due to continued progress in methodology, design and manufacturing.
# Ericsson shall have no liability for any error or damage of any kind resulting
# from the use of these documents.
#
# Ericsson is the trademark or registered trademark of Telefonaktiebolaget LM
# Ericsson. All other trademarks mentioned herein are the property of their
# respective owners.
#
#-------------------------------------------------------------------------------
#
package Itk::FetchFiles;

use strict;
use Hash::Util qw(lock_keys);
use File::Temp qw(tempdir);
use File::Path;
use Time::Local;
use Data::Dumper;
use Itk::NitsPackServer;
use Itk::FileTransfer;
use Time::Local qw/ timegm /;
use Itk::CollectionHistory;
use Itk::PmData::DataFormatter;


use fields (
    'errorlog',
    'debug',
    'ipdatabase',
    'sitefile',
    'tmpdir',
    'spooldir',		# Base directory for spool files
    'storedir',
    'maxfiles',
    'collectionhistory',
    'defaultdir',
    'filematch',
    'filelistname',
    'protocol',
    'port',
    'dontfetch',
    'nodename',
    'ip',
    'password',
    'username',
    'nodedetails',
    'nodespool',	# The node specific spool directory
    'zip',
);

#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;  #reference to global server object
*srv=\$main::srv;
my %months = (jan=>0, feb=>1, mar=>2, apr=>3, may=>4, jun=>5, jul=>6, aug=>7, sep=>8, oct=>9, nov=>10, dec=>11);  #Month abbreviation strings (lower case for comparison)
my $currentYear;
my $defaultports = {sftp=>22, ftp=>21};

# Maximum number of files to allow in temp directory (per node).
# Once this limit is reached the oldest ones will be deleted
# Default is 7 days x 96 files
#
# Some nodes have multiple files per period, we will check for this
# in the OSS section and we may end up with a multiple of this amount.
my $maxfiles = 672;

##-----------------------------------------------------------------------------
# _warn($msg)
# Issue warning, prepending the current netword id
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _warn {
	$srv->logWarn("($_[0]->{nodename}) $_[1]");
}

##-----------------------------------------------------------------------------
# _error($msg)
# Issue Error, prepending the current netword id
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _error {
	$srv->logError("($_[0]->{nodename}) $_[1]");
}

##------------------------------------------------------------------------------
# new(%options)
# Constructs a new FetchFiles object (abstract class!)
# - %options
# % host  Remote hostname or IP address
# % port  Remote port
# % timeout  Login/command timeout to use initially
# RETURNS: FetchFiles object
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if DEBUG;
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->{ipdatabase} = $options{ipdatabase};
	$self->{sitefile} = $options{sitefile};	
	$self->{storedir} = $options{storedir};
	$self->{protocol} = $options{protocol};
	$self->{port} = (defined $options{port}) ? $options{port} : $defaultports->{$options{protocol}};
	$self->{spooldir} = $options{spooldir};

	$self->{defaultdir} = (defined $options{defaultdir}) ? $options{defaultdir} : undef;
	$self->{filematch} = (defined $options{filematch}) ? $options{filematch} : undef;
	$self->{maxfiles} = (defined $options{maxfiles}) ? $options{maxfiles} : $maxfiles;		
	$self->{dontfetch} = (defined $options{dontfetch}) ? $options{dontfetch} : 0;

	$self->{ip} = $options{ip};
	$self->{password} = $options{password};
	
	$self->{nodename} = $options{nodename};
	$self->{nodedetails} = $options{nodedetails};
	
	$srv->logDebug(4, "Get collection history ".$options{nodename}."\n") if DEBUG4;
	
	# Get collection history object for this node
	$self->{collectionhistory} = 
	new Itk::CollectionHistory(node => $self->{nodename},
	                           storedir => $self->{storedir},
		  		   dbfilename => $options{dbfilename});
	return $self;
}


##------------------------------------------------------------------------------
# _getListOfFile
# Gets the list of file present in a list of directory  
# 
# RETURNS: An array reference containing file names   
#------------------------------------------------------------------------------
sub _getListOfFile {
	$srv->logEnter(\@_) if DEBUG;
	my ($self, $session, $node) = @_;
	
	# Our algorithm for getting the active PM directory is:
	# 1) Get an 'ls' of the parent dir of all the candidates
	# 2) find the entry for the target dir (if not existing then skip)
	# 3) The current dir is the one with the most recent modification time
	# NB. Cello does not support the MDTM command, which would be much easier
	# drwxrwxrwx   0 0          0                512  Nov  4 23:48 pm
	my $latest=0;
	my $pmpath='';
	my $pmpaths = $self->{defaultdir};
	my $nodefiles = undef;
	
	foreach my $candidate (@$pmpaths) {
			
		$srv->logDebug(4, "$node Trying path: $candidate ") if DEBUG4;
		my ($parentdir, $subdir) = ($candidate =~ /^([\w\/]+)\/(\w+)\/?$/);
		
		my @subdirs;
		eval {
			if ($session->cwd($parentdir)) {
				$srv->logDebug(4, "_getListOfFile: Directory found: cwd $parentdir") if DEBUG4;
				@subdirs=$session->dirStat($parentdir);
			}
			else {
				$srv->logDebug(4, "_getListOfFile: Directory NOT found: cwd $parentdir") if DEBUG4;
			}
			
		};
		if ($@) {
			die unless $@ =~ /timeout/i;
		}

		next unless @subdirs; # If parent does not even exist

		# Get the list since subdirs is a list of list.
		my $a = $subdirs[0];
		my $d = undef;
		
		# find the sub directory
		foreach my $sub (@$a)  {
			if ($sub->{'name'} eq $subdir) {
				$d = $sub;
				last;
			}
		}
		
		if (!defined ($d)) {
			$srv->logDebug(4, "$subdir is not found in $parentdir") if DEBUG4;
			next;
		}

		# get the time from candidate
		my $mtime = $d->{mtime};
		$srv->logDebug(4, "$candidate is found with mtime=$mtime") if DEBUG4;
		next unless $mtime;
		
		#get the current UTC time
		my $now = time;
		
		# compare the mtime with the latest time and now.
		# mtime should not be newer than $now, if it's newer, then the candidate 
		# should not be regarded as the newest, due to the incompletion of time stamp.  
		$srv->logDebug(4, "compare current: $mtime with $latest and $now") if DEBUG4;
		if ($mtime>$latest && $mtime<$now) {
			$srv->logDebug(4, "$candidate becomes the latest with mtime=$mtime") if DEBUG4;
			$pmpath=$candidate;
			$latest=$mtime;
		}
		
	}  # for each
	
	# check if directories is found
	
	$srv->logDebug(4, "File will be fetched from $pmpath ") if DEBUG4;
	if ($pmpath) {

		eval {
			    $srv->logDebug(4, "$node Using path: $pmpath ") if DEBUG4;
				unless ($session->cwd($pmpath)) {
					$self->_error ("Failed to cd to $pmpath on $node");
					goto END_OF_LIST_OF_FILE;
				}
				# Get list of files in dir
				 $srv->logDebug(4, "about to get list of file in: $pmpath ") if DEBUG4;
				 $nodefiles=$session->ls($pmpath);
			};
		
		if ($@) {
			if ($@ =~ /timeout/i) { 
				$self->_error("$node:$@");
				goto END_OF_LIST_OF_FILE;
			}
		}
		
	} else {
		$self->_error ("$node: Could not find a candidate directory within: ".join(' ', @$pmpaths));
	}
 
    my $nb = scalar $nodefiles;
	$srv->logDebug(4, "number of file found : $nb ") if DEBUG4;
	
	END_OF_LIST_OF_FILE:
	return $nodefiles;	
	
}

##------------------------------------------------------------------------------
# addToZipFile (fileName,zipFileName)
# argument:
# spoolDir : spool directory
# tempDir : temporary direcoty
# dirToZip : directory to zip
# zipFileName : Zip file name
# deleteDir : delete directory 
# 
# RETURNS: true
#------------------------------------------------------------------------------
sub zipDirToFile {
	$srv->logEnter(\@_) if DEBUG;
	my ($self,$spoolDir, $tmpdir, $dirToZip, $zipFileName, $deletedir) = @_;
	
	$srv->logDebug(4, " spoolDir: $spoolDir tmpDir:$tmpdir dirToZip:$dirToZip zipfileName:$zipFileName ") if DEBUG4;
	
	# rename the zip file to 
	if (-e "$spoolDir/$zipFileName" ) {	
	    $srv->logDebug(4, "rename file $spoolDir/$zipFileName to $tmpdir/$zipFileName") if DEBUG4;
		rename ("$spoolDir/$zipFileName", "$tmpdir/$zipFileName");
	}
	
    $srv->logDebug(4, "zip $tmpdir/$zipFileName -j $spoolDir/$dirToZip/*") if DEBUG4;
	my $command = "zip $tmpdir/$zipFileName -j $spoolDir/$dirToZip/*";
	system ($command);
	
	if (-e "$tmpdir/$zipFileName" ) {	
         $srv->logDebug(4, "rename file $tmpdir/$zipFileName to $spoolDir/$zipFileName ") if DEBUG4;
		rename ("$tmpdir/$zipFileName", "$spoolDir/$zipFileName");
	}
	
	if ($deletedir && (-e "$spoolDir/$zipFileName")) {
        $srv->logDebug(4, "delete directory $spoolDir/$zipFileName ") if DEBUG4;
	    my $command = "rm -rf $spoolDir/$dirToZip";
	    system ($command);
		
	}
}




##------------------------------------------------------------------------------
# _getFileTransferSession
# Instantiate the appropriate FTP session based on the protocol name.
# RETURNS: FileTransfer object
#------------------------------------------------------------------------------
sub _getFileTransferSession  {	
	$srv->logEnter(\@_) if DEBUG;
	my ($self,$ip,$passwd,$node, $user) = @_;
			
	my %ftOptions = (
		host=>$ip,
		port=>$self->{port},
	);
	
	$user='itk' unless $user;
	
	my $session=undef;
	eval {
	
		# get the proper ftp instance based on the option 
		if ($self->{protocol} eq "ftp") {
			$session = Itk::FTP->new(%ftOptions);
		} elsif ($self->{protocol} eq "sftp") {
			$session = Itk::SFTP->new(%ftOptions);
		} else {
			$self->_error ("protocol ".$self->{protocol} ." is not supported !!");
			return undef;
		}
		
		# check if the instance 
		if (!defined ($session) ) {
			$self->_error ("Failed to connect to $node. Try again next time!");
			return undef;
		} 
	
		# do login
		unless ($session->login($user, $passwd)) {
			$self->_error ("Failed to log in to $node. Try again next time!");
			return undef;
		}
		
	};

	if ($@) {
		$self->_error($@);
		return undef;
	}

	return $session; 
	
}

##------------------------------------------------------------------------------
# Abstract class collect(%options)
# Constructs a new FetchFile object (abstract class!)
# RETURNS: 1 for success and 0 for failure
#------------------------------------------------------------------------------
sub collect {
	$srv->logEnter(\@_) if DEBUG;
	my ($self, %options) = @_;
	lock_keys(%options);
	
    $self->_error ("collect class is not implemented ");
    return 0;
}

##------------------------------------------------------------------------------
# runPostNodeHook
# RETURNS: 1 for success and 0 for failure
#------------------------------------------------------------------------------
sub runPostNodeHook {
	$srv->logEnter(\@_) if DEBUG;
	my ($self, %options) = @_;

	eval {
		# Only run if we have a node type
		if($self->{nodedetails}->{type}) {
			# If the formatter doesn't exist we will exit this code block.
			my $formatter = Itk::PmData::DataFormatter::getFormatter($self->{nodedetails}->{type});
			return 0 unless $formatter->postNode( $self->{nodename}, $self->{nodespool},$self->{nodedetails});
		}
	};
	
	if (exists $main::{postNode}) {
		return &{$main::{postNode}}(($self->{nodename}, $self->{nodespool},$self->{nodedetails}));
	}
	
	return 1;
}

##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	$srv->logEnter(\@_) if DEBUG;
	my $self=shift;
}
#==============================================================================
package Itk::FetchFilesPMXML;
use strict;
use Hash::Util qw(lock_keys);
use File::Path;
use Time::Local;
use Itk::FileTransfer;
use File::Temp qw(tempdir);
use Data::Dumper;
use base 'Itk::FetchFiles';

#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;  #reference to global server object
*srv=\$main::srv;

# Spool Directory
my $spooldir = "$ENV{HOME}/spool/pmxml";

# Default Paths to try on node for PM files
my @pmpaths=('/p001200/pm', '/p001300/pm', '/c/public_html/cello/XML_files', '/p001200/pm_data', '/p001300/pm_data', '/c/pm_data');

# Default file match
my $filematch='.xml.gz$';


##------------------------------------------------------------------------------
# new(%options)
# Constructs a new FetchFilePMXML object 
# - %options
# RETURNS: FetchFilePMXML object
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if DEBUG;
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	
	# call the ancestor
	$self = $self->SUPER::new(@_);
	
	# if the default directory isn't defined
	# take the default
	if (!defined ($self->{defaultdir})) {
		$self->{defaultdir} = \@pmpaths;
	}

	# if the spool directory is not defined 
	# take the default.
	if(!defined ($self->{spooldir})) {
		$self->{spooldir} = $spooldir;
	}
	
	# attribute for the filelistname
	$self->{filelistname} = "PMXML";
	
	# if the file to match is not defined
	# take the default.
	if(!defined ($self->{filematch})) {
		$self->{filematch} = $filematch;
	}
	
	$self->{nodespool} =  $spooldir."/".$self->{nodename};

    # zip option
    $self->{zip} = $options{zip};

	return $self;
}


##------------------------------------------------------------------------------
# collect(%options)
# collects pm xml file.
# - %options
# % ip  Remote hostname or IP address
# % password password to connect to the node
# % port  Remote port
# % timeout  Login/command timeout to use initially
# RETURNS: Boolean indicating success
#------------------------------------------------------------------------------
sub collect {
	$srv->logEnter(\@_) if DEBUG;
	my ($self) = @_;
	
	my $fileList=undef;
	
	my $ip = $self->{ip};
   	my $passwd = $self->{password};
	my $node = $self->{nodename};
	my $spooldir = $self->{spooldir};
    
	if (!defined ($ip) || !defined ($passwd) || !defined ($node)) {
		$self->_error ( " one parameter missing ( ip($ip) , password($passwd) , node($node) ");
		goto END_OF_COLLECT;
	}

	# get the transfer sesion 
	my $session = $self->_getFileTransferSession ($ip, $passwd, $node);

	# quit if transfer session isn't defined
	goto END_OF_COLLECT if (!defined ($session));
	
	my $return_value = 0;

    $srv->logDebug(4, "Trying node $node with ip/dns $ip and password $passwd ") if DEBUG4;
    
    # Make sure node's spool directory exists. NB. mkpath dies if it fails
	# and will also attempt to create all intermediate dirs
		
    $srv->logDebug(4, "spooldir is  $spooldir ") if DEBUG4;
    mkpath "$spooldir/$node" unless -d "$spooldir/$node";

	# create the temporary directory
	my $tmpdir=tempdir(DIR=>$spooldir, CLEANUP => 1);

	# Algorithm:
	# 1) Load previous list of fetched files
	# 2) get list of files on node, filtered according to parameters
	# 3) Remove any entries in list of fetched files that do not exist on the node
	# 4) For each file on node:
	# 	a) Fetch each file into temp dir
	# 	b) Move file into spool dir
	#    	c) Add file to list of files fetched
	#    	d) Save list of files fetched in this session
	
	# List of fetched files is stored in a hash for quick lookup
	# On disk we store it using Storable, also for efficiency
	
	#
	# get the filelist from the history
	$srv->logDebug(4, "About to get the file list($node) : >$self->{filelistname}<") if DEBUG4;
	
	$fileList = $self->{collectionhistory}->get($self->{filelistname}) || {};
	if (!defined ($fileList) ) {
		_warn ("Couldn't get the $self->{filelistname} from the history file ");
	}
			
	# get the list of gpeh header files.
	my $nodefiles = $self->_getListOfFile ($session, $node);
	
	goto END_OF_COLLECT unless $nodefiles;
	
	# Clear any files from the fetched file list that are no longer
	# on the node. Otherwise the list will grow forever
	if (defined $fileList) {
		foreach my $file (keys %{$fileList}) {
			$srv->logDebug(4, "remove file $file from the list of file to collect ") if DEBUG4;
			delete $fileList->{$file} unless grep($_ eq $file, @$nodefiles);
		}
	} else {
		$srv->logDebug(4, "The $self->{filelistname} isn't defined ") if DEBUG4;
	}
	
	# get the file to match
	my $filematch = $self->{filematch};
	$srv->logDebug(4, "filematch is  $filematch ") if DEBUG4;
	FILE:	foreach my $file (@$nodefiles) {

		next FILE if ($filematch && $file !~ $filematch);
		
		
		$srv->logDebug(4, "$node: Check if file $file is to be fetched: ") if DEBUG4;
		
		if ($self->{dontfetch}) {
			$fileList->{$file}=1;
			$self->{collectionhistory}->put($self->{filelistname}, $fileList);
			
			$srv->logDebug(4, "\033[33m NO \033[0m : it will be flagged as fetched ") if DEBUG4;
			next FILE;
		}
		
		# Have we already fetched this one?
		if (exists $fileList->{$file}) {
			$srv->logDebug(4, "\033[33m NO \033[0m ") if DEBUG4;
			next FILE;
		}
		
		
		$srv->logDebug(4, "\033[33m YES \033[0m ") if DEBUG4;

		
		$srv->logDebug(4, " $node: Try to fetch $file from $node:  ") if DEBUG4;
		
		# File is initially fetched into a temporary directory,
		# then moved into the actual destination. This is done so that
		# a file reciever will not get a partially completed file
		# if they happen to run at the same time as this is still
		# running
		my $ftpresult;
		eval {
			$ftpresult = ($session->get($file, "$tmpdir/$file") && 	
					rename("$tmpdir/$file", "$spooldir/$node/$file"));
		};
		
		if ($@) {
			die unless $@ =~ /timeout/i;
			$self->_error("$node:$@");
			last FILE;  #don't bother continuing with other files on the node
		}		

		# Check if file was retrieved. We test return from ftp operation
		unless ($ftpresult) {
			# In case file was partially recieved: delete remnants
			unlink "$tmpdir/$file" if (-e "$spooldir/$node/$file");
			
			$srv->logDebug(4, " \033[31mFAILED\033[0m ") if DEBUG4;
		} else {
			
			$srv->logDebug(4, " \033[32m OK \033[0m ") if DEBUG4;
			if (exists $main::{postFileFetch}) {
				next unless (&{$main::{postFileFetch}}($node, "$spooldir/$node/$file", $self->{nodedetails}));
			}
			# Flag the file as fetched by putting it in the hash
			$fileList->{$file}=1;
			$self->{collectionhistory}->put($self->{filelistname}, $fileList);
		}
	}
	# Make sure we have not exceeded the maximum number of files
	my @filelist=sort glob "$spooldir/$node/*.xml.gz";
	while (scalar @filelist > $self->{maxfiles} ) {
		
		# Since these are sorted, we are getting the oldest file
		# with shift
		my $f = shift @filelist;
		unlink $f;
		
	}

	# success 
	$return_value = 1;

    
	END_OF_COLLECT:

	# zip the output
    if ($self->{zip}) {
		$srv->logDebug(4, "zip the directory  $spooldir/$node to $spooldir/$node.zip ") if DEBUG4;
		$self->zipDirToFile ($spooldir,$tmpdir,"$node","$node.zip",1);
    }



	#store($filelist, $filelist_file);
	$session->quit () if defined ($session);
	
	my $nb = scalar @filelist;
	$srv->logDebug(4, " $node: $nb files waiting in the spool dir ") if DEBUG4;
	
	$self->{collectionhistory}->commit ();
	return $return_value;
	
}


##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	$srv->logEnter(\@_) if DEBUG;
	my $self=shift;
}
#==============================================================================
package Itk::FetchFilesGPEH;
use strict;
use Hash::Util qw(lock_keys);
use File::Path;
use Time::Local;
use Data::Dumper;
use File::Temp qw(tempdir);
use Data::Dumper;
use base 'Itk::FetchFiles';

#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;  #reference to global server object
*srv=\$main::srv;
#my %months = (jan=>0, feb=>1, mar=>2, apr=>3, may=>4, jun=>5, jul=>6, aug=>7, sep=>8, oct=>9, nov=>10, dec=>11);  #Month abbreviation strings (lower case for comparison)
#my $currentYear;
#my $defaultports = {sftp=>22, ftp=>21};

# Spool Directory
my $spoolgpehdir = "$ENV{HOME}/event/ready";

# Default Paths to try on node for PM files
my @gpehpaths=('/p001200/pm_data', '/p001300/pm_data', '/p001200/pm', '/p001300/pm/', '/c/pm_data','/c/public_html/cello/XML_files');

# Default file match
my $gpehfilematch='.lnk.gz$';

##------------------------------------------------------------------------------
# new(%options)
# Constructs a new FetchFileGPEH object 
# - %options
# RETURNS: FetchFileGPEH object
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if DEBUG;	
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	
	$self = $self->SUPER::new(@_);
	
	# if the default directory isn't defined
	# take the default
	if (!defined ($self->{defaultdir})) {
		$self->{defaultdir} = \@gpehpaths;
	}

	# if the spool directory is not defined 
	# take the default.
	if(!defined ($self->{spooldir})) {
		$self->{spooldir} = $spoolgpehdir;
	}
		
	# attribute for the filelistname
	$self->{filelistname} = "GPEH";
	
	# if the file to match is not defined
	# take the default.
	if(!defined ($self->{filematch})) {
		$self->{filematch} = $gpehfilematch;
	}
	
	$self->{nodespool} = $self->{spooldir};
	
	return $self;
}

##-------------------------------------------------------------------------------
#_getListOfHeaderFile 
# Returns the list of directories to look for data
#
##-------------------------------------------------------------------------------

sub _getListOfHeaderFile  {
	
	$srv->logEnter(\@_) if DEBUG;
	my ($self, $session, $node) = @_;
	
	# Our algorithm for getting the  directory is:
	# 1) Get an 'ls' of the parent dir of all the candidates
	# 2) find the entry for the target dir (if not existing then skip)
	# 3) The current dir is the one with the most recent modification time
	# NB. Cello does not support the MDTM command, which would be much easier
	# drwxrwxrwx   0 0          0                512  Nov  4 23:48 pm
	my $latest=0;
	my $pmpath='';
	my $pmpaths = $self->{defaultdir};
	my $nodefiles = undef;
	
	foreach my $candidate (@$pmpaths) {
		
		$srv->logDebug(4, "$node Trying path: $candidate ") if DEBUG4;
		my ($parentdir, $subdir) = ($candidate =~ /^([\w\/]+)\/(\w+)\/?$/);
				
		my @subdirs;
		eval {
			if ($session->cwd($parentdir)) {
				$srv->logDebug(4, "_getListOfHeaderFile: Directory found: cwd $parentdir") if DEBUG4;
				@subdirs=$session->dirStat($parentdir);
			}
			else {
				$srv->logDebug(4, "_getListOfHeaderFile: Directory NOT found:  cwd $parentdir") if DEBUG4;
			}
		};
		if ($@) {
			die unless $@ =~ /timeout/i;
		}

		next unless @subdirs; # If parent does not even exist

		# Get the list since subdirs is a list of list.
		my $a = $subdirs[0];
		my $d = undef;
		
		# find the sub directory
		foreach my $sub (@$a)  {
			if ($sub->{'name'} eq $subdir) {
				$d = $sub;
				last;
			}
		}
		
		if (!defined ($d)) {
			$srv->logDebug(4, "$subdir is not found in $parentdir ") if DEBUG4;
			next;
		}
		
		# get the time from candidate
		my $mtime = $d->{mtime};
		$srv->logDebug(4, "$candidate is found with mtime=$mtime") if DEBUG4;
		next unless $mtime;
		
		if ($mtime>$latest) {
			$pmpath=$candidate;
			$latest=$mtime;
		}
		
	}  # for each
	
	# check if directories is found
	if ($pmpath) {
		eval {
			    $srv->logDebug(4, "$node Using path: $pmpath ") if DEBUG4;
				unless ($session->cwd($pmpath)) {
					$self->_error ("Failed to cd to $pmpath on $node");
					goto END_OF_LIST_OF_FILE;
				}
				# Get list of files in dir
				 $srv->logDebug(4, "about to get list of file in: $pmpath ") if DEBUG4;
				 $nodefiles=$session->ls($pmpath);
			};
		
		if ($@) {
			if ($@ =~ /timeout/i) { 
				$self->_error("$node:$@");
				goto END_OF_LIST_OF_FILE;
			}
		}
		
	} else {
		$self->_error ("$node: Could not find a candidate directory for $pmpaths");
	}
 
    my $nb = scalar $nodefiles;
	$srv->logDebug(4, "number of file found : $nb ") if DEBUG4;
	
	END_OF_LIST_OF_FILE:
	return $nodefiles;	
	
	
}


##------------------------------------------------------------------------------
# collect(%options)
# Constructs a new FetchFileGPEH object (abstract class!)
# - %options
# % ip        Remote hostname or IP address
# % password  Password
# % node	  node name
# RETURNS:  FetchFileGPEH object
#------------------------------------------------------------------------------
sub collect {
	$srv->logEnter(\@_) if DEBUG;
	my ($self) = @_;
	
	# Algorithm:
	#
	# 0) Load previous list of fetched files
	# 1) Fetch all AYYYYMMDD.HHMM-HHMM_gpehfile.2.lnk.gz (header file) in tmp
	# 2) Check if the file has been already fetched
	# 3) For each header file on node:
	#   a) Get the list of GPEH file from header file  	
	#   b) Fetch each file into the tempdir
	#   c) zip all the files from dir into one file in the 
	
	my $fileList=undef;
	my $ip = $self->{ip};
	my $passwd = $self->{password};
	my $node = $self->{nodename};
	my $spooldir = $self->{spooldir};
	my $return_value = 0;
	my $nbFile = 0;
    
    #
    # check if all the mandatory arguments are there 
	if (!defined ($ip) || !defined ($passwd) || !defined ($node)) {
		$self->_error( " one parameter missing ( ip , password , node names) ");
		goto END_OF_COLLECT;
	}
		
	#
	# Load the list of the file fetched
	$fileList = $self->{collectionhistory}->get($self->{filelistname}) || {};	
	
	
	# get the transfer sesion 
	my $session = $self->_getFileTransferSession ($ip, $passwd, $node);
	
	# quit if transfer session isn't defined
	goto END_OF_COLLECT if (!defined ($session));
	
	# get the list of gpeh header files.
	my $headerFiles = $self->_getListOfHeaderFile ($session, $node);
	
	# remove files that are no longuer on the node
	if (defined $fileList) {
		foreach my $file (keys %{$fileList}) {
			$srv->logDebug(4, "remove file $file from the list of file collected ") if DEBUG4;
			delete $fileList->{$file} unless grep($_ eq $file, @$headerFiles);
		}
	} else {
		$srv->logDebug(4, "The $self->{filelistname} isn't defined ") if DEBUG4;
	}

    # get the filematch.
	my $filematch = $self->{filematch};
	$srv->logDebug(4, "filematch is  $filematch ") if DEBUG4;
		
	# process each file.
	FILE1: foreach my $file (@$headerFiles) {
		
		# make sure it gets a clean file name.
		if ($file =~ /^.*(A\d+\.\d+\-\d+.*)$/) {
			$file = $1;
		} else {
			# cannot get a clean file name .
			$self->_error("$node: could not determine zip file name with $file ");
			next FILE1;			
		}
		
		next FILE1 if ($filematch && $file !~ $filematch);

		# Have we already fetched this one?
		if (exists $fileList->{$file}) {
			$srv->logDebug(4, "File". $file) if DEBUG4;
			$srv->logDebug(4, "\033[33m NO \033[0m ") if DEBUG4;
			next FILE1;
		}


		# build the directory file name
		my $zipFileName = undef;
		if ( $file =~ /^(A\d+\.\d+\-\d+).*$/) {
			$zipFileName = "$spooldir/$node"."_".$1."_gpeh.zip";
		}
		
		if (!defined ($zipFileName) ) {
			$self->_error("$node: could not determine zip file name with $file ");
			next FILE1;
		}

        $srv->logDebug(4, "zip file name is $zipFileName") if DEBUG4;

	    #create the tempdirectory
    	my $tmpdir=tempdir(DIR=>$spooldir, CLEANUP => 1);
		$srv->logDebug(4, "$tmpdir is created ") if DEBUG4;
		

		$srv->logDebug(4, "About to fetch $file") if DEBUG4;

		my $ftpresult;
		eval {
			$ftpresult = $session->get($file, "$tmpdir/$file");
		};
		
		if ($@) {
			die unless $@ =~ /timeout/i;
			$self->_error("$node:$@");
			last FILE1;  #don't bother continuing with other files on the node
		}		


        #put the file in the hash
        $fileList->{$file}=1;

		## collectionHistory
		$self->{collectionhistory}->put($self->{filelistname}, $fileList);

		if ($self->{dontfetch}) {
			$srv->logDebug(4, "\033[33m NO \033[0m : it will be flagged as fetched ") if DEBUG4;
			next FILE1;
		}

		$srv->logDebug(4, "About to fetch GPEH : $self->{filelistname} ") if DEBUG4;
		
		# get the list of file to fetch
		my $gpehFileNames = $self->_getGPEHFileNameToFetch ($tmpdir,$file);

		FILE2: foreach my $gpehFile (@$gpehFileNames) {
			
				# get the module id
				my ($moduleId,$gphName) = undef;
				
				if ($gpehFile =~ /^\/(.*)\/(.*)$/) {
					$moduleId = $1;
					$gphName = $2;
				}
			
			
			    $srv->logDebug(4, "Module Id is $moduleId") if DEBUG4;
			    
				my $destinationName = "$tmpdir/$moduleId"."_".$gphName;
				
				$srv->logDebug(4, "About to get $file to $destinationName ") if DEBUG4;

				
				# get the file in the tmp dir;
				eval {
					$ftpresult = $session->get($gpehFile, $destinationName);
				};
			
				if ($@) {
					die unless $@ =~ /timeout/i;
					$self->_error("$node:$@");
					last FILE2;  #don't bother continuing with other files on the node
				}		

				# Check if file was retrieved. We test return from ftp operation
				unless ($ftpresult) {
					# In case file was partially recieved: delete remnants
					unlink "$destinationName" if (-e "$destinationName");
			
					$srv->logDebug(4, " \033[31mFAILED\033[0m ") if DEBUG4;
					next FILE2;
				};
			
				$srv->logDebug(4, " \033[32m OK \033[0m ") if DEBUG4;
			
		} # process 
		
		
		$srv->logDebug(4, "About to zip  $zipFileName ") if DEBUG4;
		
		# zip the file in the tmp directory
		my $result = $self->zipDirectory ($tmpdir, $zipFileName);

		$srv->logDebug(4, "About to delete $tmpdir ") if DEBUG4;		
		# delete the directory
		eval { rmtree($tmpdir) };
  		if ($@) {
   			 $self->_error( "Couldn't delete dir: $@");
  		}  
        $nbFile++;
	}  # for each header file 

        	
	$srv->logDebug(4, "$node: $nbFile files waiting in the spool dir ") if DEBUG4;
	
	# success 
	$return_value = 1;
	
	END_OF_COLLECT:	
	$self->{collectionhistory}->commit ();

	$session->quit () if defined ($session);

	return $return_value;
}

##------------------------------------------------------------------------------
# getGPEHFileNameToFetch:
# Gets the list of file to fetch from the node.
# File name convention is expected: AYYYMMDD.HHMM-HHMM_gpehfile:2.lnk.gz
# RETURNS: an array of files to fetch
#------------------------------------------------------------------------------
sub _getGPEHFileNameToFetch {
	$srv->logEnter(\@_) if DEBUG;
	my ($self, $dir, $file) = @_;

	my ($rec, $buffer);	
	$srv->logDebug(4, "About to get file names from $dir/$file") if DEBUG4;	
	
	my @arrayReturn = ();
	my $fileHandle;
	my $newName = undef;
	if ($file =~ /(A\d+\.\d+-\d+_gpehfile:2.lnk).gz/) {
		# FIX ME ... FIND A BETTER WAY
		system ("gzip", "-d", "$dir/$file");
		$newName = $1;
	}
	
	if (!defined ($newName)) {
		$self->_error(4, "The $file isn't recongnized ");
		# return an empty array.
		return \@arrayReturn;
	}
		
	if (!open( $fileHandle, "<$dir/$newName" )) {
		$self->_error( "Cant open $newName \n$!");
		return \@arrayReturn;
	}
	my $recNo = 0;
	while ( sysread( $fileHandle, $rec, 3 ) ) {
		my ( $l, $RecordType ) = unpack( "nC", $rec );
		sysread( $fileHandle, $buffer, ( $l - 3 ) );
		if ( $RecordType == 8 ) {
			my $buf = unpack( "A*", $buffer ), "\n";
			$srv->logDebug(4, "needs to get $buf") if DEBUG4;
			push @arrayReturn, $buf;
		}
	}
	close($fileHandle);

    # FIX ME ... FIND A BETTER WAY
	system ("gzip", "$dir/$newName");

	return \@arrayReturn;
}    

##------------------------------------------------------------------------------
# _zipDirectory
# Zip an entire directory.
#
# RETURNS: success 1 or failure 0
#------------------------------------------------------------------------------

sub zipDirectory {
	$srv->logEnter(\@_) if DEBUG;
	my ($self, $dir, $fileName) = @_;

    $srv->logDebug(4, "About to zip the directory $dir in $fileName ") if DEBUG4;

     my @command = ( "zip", "-j", "-r", "-q", $fileName, "$dir" );
     
     my $retVal = system (@command);
    
	if ($? == -1) {
		_error ("Error to zip file: $!\n ");
		return 0;
	}
	return 1;
	
}

##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	$srv->logEnter(\@_) if DEBUG;
	my $self=shift;
}

#==============================================================================
package Itk::FetchFilesOSS;
use strict;
use File::Temp qw(tempdir);
use File::Path;
use Itk::PmData::DataFormatter;
use base 'Itk::FetchFiles';

if(DEBUG5) {
	use Data::Dumper;	
}


# Available macros
# $nodename - node unique name
# $network  - first subnetwork in fdn
# $subnetwork - second subnetwork in fdn
# $mecontext - mecontext
# Information on the file location can be find in:
# SGw, Statistical Gateway, System Administrator Guide
# SYSTEM ADMINISTRATOR GUIDE         1543-APR 901 0150 Uen DG    
use constant NODEDETAILS => {
	# Core Nodes
	'mgw'	=>	{	path => ['/ossrc/data/sgw/sgwcg/xml/mgw/$nodename/'], 
				filematch => '.*$nodename.*xml', 	
				recursive => 0},	
	'ggsn'	=>	{	path=>['/ossrc/ericsson/ccpdm/pm_storage/$nodename/'],						
				filematch => '.*$nodename.*xml', 	
				recursive => 1},	
	'sgsn'	=>	{	path => ['/ossrc/data/sgw/sgwcg/xml/gsn/$nodename/','/var/opt/ericsson/sgw/outputfiles/xml/3gpp-32.435/gsn/$nodename/'],							
				filematch =>'.*$nodename.*',		
				recursive => 0,
				filesperperiod => 30},
	'hlr'	=>	{	path=>['/ossrc/data/sgw/sgwcg/asn1/apg_sts/$nodename/','/ossrc/data/sgw/sgwcg/asn1/oms/'],	
				filematch => '.*$nodename.*',
				recursive => 0,
				filesperperiod => 4},
				
	'mme'	=>	{path => ['/var/opt/ericsson/sgw/outputfiles/xml/mme'],	
				filematch => '.*$nodename.*xml', 	
				recursive => 0,
				filesperperiod => 4},
	'msc'	=>	{	path => ['/ossrc/data/sgw/sgwcg/asn1/apg_sts/$nodename/','/ossrc/data/sgw/sgwcg/asn1/oms/'],	
				filematch => '.*$nodename.*',
				recursive => 0,
				filesperperiod => 4},
	'sasn'=>	{	path => ['/var/opt/ericsson/ccpdm/pm_storage/SASN/'],
				filematch => '$nodename',
				recursive => 1,
				filesperperiod => 80},
	'sasnnode'=>	{	path => ['/opt/nsfw/var/log/'],
				filematch => '^oss.*stat',
				recursive => 0},
	# RAN Nodes
	'gsmiog'=>	{	path => ['/var/opt/ericsson/nms_eam_eac/data/fs/$nodename/'],
				filematch => '.*',
				recursive => 0},
	'rbs'=>		{	path => ['/var/opt/ericsson/nms_umts_pms_seg/segment1/XML/SubNetwork=$subnetwork/MeContext=$mecontext/'],
				filematch => '\.xml(\.gz)?',
				recursive => 0},
	'rnc'=>		{	path => ['/var/opt/ericsson/nms_umts_pms_seg/segment1/XML/SubNetwork=$subnetwork/MeContext=$mecontext/'],
				filematch => '\.xml(\.gz)?',
				recursive => 0},
	'rxi'=>		{	path => ['/var/opt/ericsson/nms_umts_pms_seg/segment1/XML/SubNetwork=$network/MeContext=$mecontext/'],
				filematch => '\.xml(\.gz)?',
				recursive => 0},				
	'enb'=>		{	path => ['/var/opt/ericsson/nms_umts_pms_seg/segment1/XML/SubNetwork=$subnetwork/MeContext=$mecontext/'],
				filematch => '\.xml(\.gz)?',
				recursive => 0},
	'siu'=>		{	path => ['/var/opt/ericsson/sgw/outputfiles/xml/stn/$nodename/'],
				filematch => '\.xml(\.gz)?',
				recursive => 0},
	# Other and Test
	# add new candidate directory for collect EBS files
	'ebsoss'=>	{	path => ['/var/opt/ericsson/eba_ebsw/data/xmloutput/SubNetwork=$subnetwork/'],
				filematch => '\.xml(\.gz)?|(ebs\.)',
				recursive => 0},
	'ebssgsnnode'=> {       path => ['/tmp/OMS_LOGs/ebs/ready'],
                                filematch => '\.xml(\.gz)?|(ebs\.)',
                                recursive => 0},
        'cv'    =>      {       path => ['/d/configuration/cv/'],
                                filematch => '.*',
                                recursive => 1},				
	'test'	=>	{	path => ['/etc/mail'],
				filematch => '.*',
				recursive => 1},
	'test2' =>	{	path => ['/var/www/manual','/var/www/icons','/var/www/icons/$nodename/$mecontext/$subnetwork/$network/'],
				filematch => 'html|gif\$',
				recursive => 0}				
};

use fields (
	'ossHost',
	'ossUser',
	'ossPass',
	'nodeFdn',
	'nodeType'
);

##------------------------------------------------------------------------------
# new(%options)
# Constructs a new FetchFilesOSS object 
# - %options
# RETURNS: FetchFilesOSS object
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my __PACKAGE__ $self = shift;
	my (%opts) = @_;
	$self = $self->SUPER::new(@_);

	# attribute for the filelistname
	$self->{filelistname} = "OSS";
	
	$self->{ossHost} =  $opts{osshost};
	$self->{ossUser} =  $opts{ossuser};
	$self->{ossPass} =  $opts{osspass};
	$self->{nodeFdn} =  $opts{nodedetails}->{fdn};
	$self->{nodeType} =  lc $opts{nodedetails}->{type};

	if(!defined ($self->{spooldir})) {
                $self->{spooldir} = $spooldir;
        }	

	$self->{nodespool} =  $self->{spooldir}."/".$self->{nodename};
	
	return $self;
}

##-----------------------------------------------------------------------------
# _deepCopy($struct, $keypairs)
# Perform a deep copy of a data structure, also replacing variables below with their values
# in any scalars
# - $struct	Ref to struct that is to be copied
# - $keypairs	Hash ref of the key=>value to be substituted
#------------------------------------------------------------------------------
sub _deepCopy {
	my ($struct, $keypairs) = @_;
	
	my $r=ref $struct;
	if ($r eq 'HASH') {
		my %result;
		foreach my $k (keys %{$struct}) {
			$result{$k}=_deepCopy($struct->{$k},$keypairs);
		}
		return \%result;
	} elsif (! $r) {
		$struct=~s/\$(\w+)/\$keypairs->{\1}/g;	
		my $result = eval qq( "$struct" );
		if ($@) {
			$srv->logError("error evaluating $struct:\n$@");
			return undef;
		}
		return $result;
	} elsif ($r eq 'CODE') {
		my $result = &{$struct}($keypairs);
		if ($@) {
			$srv->logError("error evaluating $struct:\n$@");
			return undef;
		}
		return $result;
	} elsif ($r eq 'ARRAY') {
		my @result;
		foreach my $v (@{$struct}) {
			push @result, _deepCopy($v, $keypairs);
		}
		return \@result;
	} elsif ($r eq 'Regexp') {
		# no need to copy
		return $struct;
	}
}


##-----------------------------------------------------------------------------
# _remoteFileList($ftp, $dir, $recurse)
# Perform a recursive file search on the given directory
# - $ftp	Reference to a connected $ftp object
# - $dir	Directory to search
# - $recurse	Whether or not to recurse through directories
#------------------------------------------------------------------------------
sub _remoteFileList {
	my ($ftp, $dir, $recurse) = @_;
	die "File Transfer Session not established" unless $ftp; 
	
	return [] unless($ftp->cwd($dir));
	my $filelist = $ftp->dirStat($dir);
	
	# Trim any trailing or duplicate folder markers
	$dir=~s/[\/]{2,}//g;
	$dir=~s/[\/]$//g;

	$srv->logDebug(5, "Scanning directory $dir") if DEBUG5;
	
	my $outputfilelist = [];
	foreach my $file (@$filelist) {
		next if $file->{name} =~ /^\.\.?$/;
		if($file->{isdir}) {
			push(@$outputfilelist,@{_remoteFileList($ftp,"$dir/$file->{name}",$recurse)}) if $recurse;	
		} else {
			push(@$outputfilelist,"$dir/$file->{name}");
		}
	}
	
	$srv->logDebug(5, "Files found in $dir:\n" . Dumper($outputfilelist)) if DEBUG5;
	return $outputfilelist;
}

sub collect {
	$srv->logEnter(\@_) if DEBUG;
	my ($self)=@_;
	
	my $ftp;
	my $node = $self->{nodename};
	my $spooldir = $self->{spooldir};
	
	eval {
		# Check we recognize the node type
		die "Unsupported node type $self->{nodeType} for $self->{nodename}.\n" unless exists NODEDETAILS->{$self->{nodeType}};

		# We default to OSS if we don't have an ip address, username and password provided
		my ($host,$user,$pass);
		if($self->{nodedetails}->{ip_address} && $self->{nodedetails}->{username} && $self->{nodedetails}->{passwd}) {
			($host,$user,$pass) = ($self->{nodedetails}->{ip_address},$self->{nodedetails}->{username},$self->{nodedetails}->{passwd});
			$srv->logDebug(4, "Connecting to host $host (u=$user,p=$pass) for node $node") if DEBUG4;
		} else {
			($host,$user,$pass) = ($self->{ossHost},  $self->{ossUser}, $self->{ossPass});
			$srv->logDebug(4, "Connecting to OSS host $host (u=$user,p=$pass) for node $node") if DEBUG4;
		}
				
		$ftp = $self->_getFileTransferSession($host, $pass, $node, $user);
		die "Failed to get File Transfer Session\n" unless $ftp;
		
		$self->{nodeFdn}=~/SubNetwork=([^,]+),(?:SubNetwork=([^,]+),)?MeContext=([^,]+)/ or $srv->logWarn("Invalid Node FDN: $self->{nodeFdn}.  We may be looking in the wrong place for the files.");  
		my $nodedetail = _deepCopy(NODEDETAILS->{$self->{nodeType}},{network=>$1,subnetwork=>$2,mecontext=>$3,nodename=>$node});
		
		$srv->logDebug(5, "Node collection details: " . Dumper($nodedetail)) if DEBUG5;
		
		# Path is an array of paths to check - we keep the base path, so we can
		# flatten the directories when we need to recurse
		my $nodefiles;
		foreach my $path (@{$nodedetail->{path}}) {
			$srv->logDebug(4, "Check files in path: $path") if DEBUG4;
			$nodefiles->{$path} = _remoteFileList($ftp,$path,$nodedetail->{recursive});
		} 
				
		$srv->logDebug(5, "Found Files: \n\t--> ".Dumper($nodefiles)."\n") if DEBUG5;
		
		my $fileList = $self->{collectionhistory}->get($self->{filelistname}) || {};
		
		# Clear any files from the fetched file list that are no longer
		# on the node. Otherwise the list will grow forever
		if (defined $fileList) {
			my $newfilelist;
			foreach my $dir (keys %$nodefiles) {
				foreach my $nfile (@{$nodefiles->{$dir}}) {
					$newfilelist->{$nfile} = $fileList->{$nfile};
				}
			}
			$fileList = $newfilelist;
		} else {
			$srv->logDebug(4, "The $self->{filelistname} isn't defined ") if DEBUG4;
		}
		
		mkpath $spooldir unless -d $spooldir;
		my $tmpdir=tempdir(DIR=>$spooldir, CLEANUP => 1);
		mkpath "$spooldir/$node" unless -d "$spooldir/$node";
		
		# Default to 1 file per period
		$nodedetail->{filesperperiod} = 1 unless $nodedetail->{filesperperiod};
		
		# Use the default filematch if one hasn't been provided
		my $filematch = $self->{filematch} || qr/$nodedetail->{filematch}/;
		$srv->logDebug(4, "filematch is $filematch ") if DEBUG4;
		foreach my $dir (keys %$nodefiles) { 
		FILE:	foreach my $file (sort @{$nodefiles->{$dir}}) {
				next if $file =~ /^\.\.?$/;
				next FILE if ($filematch && $file !~ $filematch);
				
				$srv->logDebug(4, "$node: Check if file $file is to be fetched: ") if DEBUG4;
				
				if ($self->{dontfetch}) {
					$fileList->{$file}=1;
					$srv->logDebug(4, "\033[33m NO \033[0m : it will be flagged as fetched ") if DEBUG4;
					next FILE;
				}
				
				# Have we already fetched this one?
				if ($fileList->{$file}) {
					$srv->logDebug(4, "\033[33m NO \033[0m ") if DEBUG4;
					next FILE;
				}
				
				$srv->logDebug(4, "\033[33m YES \033[0m ") if DEBUG4;
				
				$srv->logDebug(4, " $node: Try to fetch $file from $node:  ") if DEBUG4;
				
				# File is initially fetched into a temporary directory,
				# then moved into the actual destination. This is done so that
				# a file reciever will not get a partially completed file
				# if they happen to run at the same time as this is still
				# running
				# If we are recursing through subdirectories, we want to flatten
				# the directory structure to ease file handling. Each directory marker
				# will be replaced by an underscore.
				my ($rfile) = $file=~/$dir[\/]*(.+)$/ or die "Failed extracting relative path from $file\n";
				$rfile=~s/[\/]{2,}//g; 
				$rfile=~s/$[\/]//g; 
				$rfile=~s/[\/]/_/g;
	
				my $ftpresult;
				eval {
					$srv->logDebug(5, "Downloading $file to  $tmpdir/$rfile") if DEBUG5;
					$ftp->get($file, "$tmpdir/$rfile") or die "Could not fetch $file: ".$ftp->lastError()."\n";
					
					$srv->logDebug(5, "Moving file $tmpdir/$rfile to $spooldir/$node/$rfile") if DEBUG5;
					rename("$tmpdir/$rfile", "$spooldir/$node/$rfile") or die "Could not move file $rfile onto spooldir $spooldir/$node/$rfile: $!\n";
				};
				
				if ($@) {
					#die unless $@ =~ /timeout/i;
					$srv->logError("Unable to fetch file $file - $node:$@");
					unlink "$tmpdir/$rfile" if (-e "$tmpdir/$rfile");
					#last FILE;  #don't bother continuing with other files on the node
					next FILE;
				}		
				
				$srv->logDebug(4, "File $rfile fetched OK") if DEBUG4;
				my $outputfilename = "$spooldir/$node/$rfile";
				eval {
					# If the formatter doesn't exist we will exit this code block.
					my $formatter = Itk::PmData::DataFormatter::getFormatter($self->{nodeType});
					$outputfilename = $formatter->postFileFetch($node, $outputfilename,$self->{nodedetails});
					next unless $outputfilename;
				};
				if (exists $main::{postFileFetch}) {
					next unless (&{$main::{postFileFetch}}($node, $outputfilename, $self->{nodedetails}));
				}
				
				# Flag the file as fetched by putting it in the hash
				$fileList->{$file}=1;
			}
		}	
		# Make sure we have not exceeded the maximum number of files
		my @filelist=sort glob "$spooldir/$node/*";		
		$srv->logDebug(5, "Spool cleanup check using " . scalar @filelist . " / " . $self->{maxfiles} . " allowed files") if DEBUG5;
		while (scalar @filelist > $self->{maxfiles}) {
			# Since these are sorted, we are getting the oldest file
			# with shift
			my $f = shift @filelist;
			unlink $f;
			$srv->logDebug(5, "Removing file $f as it is over the maximum allowed files in spool directory") if DEBUG5;
		}
		
		my $nb = scalar @filelist;
		$srv->logDebug(4, " $node: $nb files waiting in the spool dir ") if DEBUG4;
		
		$self->{collectionhistory}->put($self->{filelistname}, $fileList);
		$self->{collectionhistory}->commit ();
		
	};
	
	my $result;
	if ($@) {
		$srv->logError("$node: $@");
		$result=0;
	} else {
		$result=1;
	}

	$ftp->quit () if defined ($ftp);
	
	return $result;
}

#==============================================================================

1; # Modules always return 1!

