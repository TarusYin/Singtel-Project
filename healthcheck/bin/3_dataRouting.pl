#!/usr/bin/perl
##
###
### Author: eqsvimp    Eric Yin Z
### $Id: itkreport 9360 2014-11-04 05:06:20Z eqsvimp $
##
### GSC CHINA Tool & Data Centre Service
### 

use FindBin qw($Bin);
use lib "../../../lib/";
use lib "$Bin/../lib/";
use warnings;
use Expect;

my $source = "/var/opt/ericsson/itk/stmsg/xmlfiles/";
$source = $source.current_timestr()."/";

my $dist = "/var/opt/ericsson/itk/stmsg/spool/data/";
my $dist_docker = "/data/xmlfiles";
my $nodetype = "Rnc";



my @xmlfiles = glob "$source*$nodetype*";
my $ip = "146.11.90.12";
foreach my $file (@xmlfiles)
{
   my ($path,$command,$filename) = ();	
   
   if($file =~ /.*\/xml_\d+_(.*)\.zip/)
   {
       $filename = $1;
   }
   $command = "unzip -n $file -d $dist$filename";
   print $command."\n";
   system($command);
   my @nodefiles = glob "$dist$filename/*";
   
   foreach my $nodefile(@nodefiles)
   {   	
   sync_files($ip,$nodefile,$dist_docker);
   }
   
}

print $dist;














sub sync_files
{

my ($ip,$source,$dist) = @_;
my $t1 = 100;
my $rsg_prompt ="\@";

#/var/opt/ericsson/itk/stmsg/spool/data/Rnc07/A20160419.1630-1645.1.xml.gz
#get path, filename , nodename
if($source =~ /(.*\/)(.*\.\d+-\d+.*\.xml\.gz)/)
{
  $path = $1;
  $filename = $2;
  if($path =~ /.*\/(.*)\//){ $nodename = $1;}
  
} 

#change filename to done.
$rename = "mv $source ".$path."done_".$nodename."_".$filename;
system($rename);
print $rename."\n";
$source = $path."done_".$nodename."_".$filename;

#transfer the data to remote server
my $command = "scp $source root\@$ip:/$dist";
print $command."\n\n";
my $ssh = Expect->spawn($command);
my $rsg_pwd="rootpw";
$ssh->expect($t1,

[ qr/Are you sure you want to continue connecting \(yes\/no\)\?/, sub {
                my $self=shift;
                $self->send("yes\n");
                exp_continue;
                } ],

        [ qr/word:|CODE:/, sub {
                my $self=shift;
                $self->send("$rsg_pwd\n");
                exp_continue;
                } ],


        -re, $rsg_prompt
) or return "end";


}




sub current_timestr
{
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime;
      $mon+=1; $year+=1900;
      if($mon <= 9) {$mon="0".$mon;}
      if($mday <= 9) {$mday="0".$mday;}
      if($hour <= 9) {$hour="0".$hour;}
      if($min <= 9) {$min="0".$min;}

$name = $year.$mon.$mday;
return $name;
#print $name;
}

