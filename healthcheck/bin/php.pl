#!/usr/bin/perl
use Itk::Logger;
use Itk::Server;
use Time::Local;
use Data::Dumper;
use JSON;
use JSON::XS;
use Time::gmtime;

#my $json_text = '{"bbb" : {"x" : 3},"a10" : {"b" : 1,"a" : 2},"a2" : {"z" : 4}}';
#my $decoded_json = decode_json($json_text);
#print Data::Dumper->Dumper($decoded_json);

#get_warranty(\@files,"ENB_952801_The_Alpha");
#print verify_warranty("201568202",365);

query();
#test1();


sub test1{

    $at=`/opt/ericsson/itk/bin/pmq`;
    @result = split '\n', $at;
    $at = join '<br>', @result;
require Itk::Ispdb::Admin;
import Itk::Ispdb::Admin;
require POSIX;
import POSIX qw(strftime);
our $srv = Itk::Server->new(admincheck=>0);
my ($dbStateTable, $dbHosts) = Itk::Ispdb::Admin::getDbStatus(nodns=>"stmsg");
my $json = new JSON;
my $json_text = $json->pretty->encode ($dbStateTable);

return $json_text;



}

sub query{

my $nwid = "stmsg";
my $db = "ispdb_".$nwid;
my $db_ip = "146.11.90.16";
my $user = "ispdbadmin";
my $password = "isp9119";
my $dbh = DBI->connect("DBI:Pg:dbname=$db;host=$db_ip;port=5432", $user, $password);

my $sql = "
        SELECT * FROM (
		SELECT  t01.fdn as \"itkAttrFdn\",
                t00.productdata as \"productData\",
                t03.uniquename as \"itk_nodename\",
                substring(t01.fdn from E'=([^,]+)\$') as \"itkAttrMoId\"
        FROM nd.auxpluginunit AS t00
                LEFT JOIN fdn AS t01 ON t01.id=t00.itkid AND t01.class='AuxPlugInUnit'
                LEFT JOIN fdn AS t02 ON t02.id=t00.itkid
                INNER JOIN nd.itk_uniquenodenames AS t03 ON t03.id=t02.parentnode
        WHERE (t00.itkdeltime IS NULL) AND (((t01.fdn)::text ~ E'ENB'))

        UNION ALL
        SELECT  t01.fdn as \"itkAttrFdn\",
                t00.productdata as \"productData\",
                t03.uniquename as \"itk_nodename\",
                substring(t01.fdn from E'=([^,]+)\$') as \"itkAttrMoId\"
        FROM nd.slot AS t00
                LEFT JOIN fdn AS t01 ON t01.id=t00.itkid AND t01.class='Slot'
                LEFT JOIN fdn AS t02 ON t02.id=t00.itkid
                INNER JOIN nd.itk_uniquenodenames AS t03 ON t03.id=t02.parentnode
        WHERE (t00.itkdeltime IS NULL) AND (((t01.fdn)::text ~ E'ENB'))	
		UNION ALL
        SELECT  t01.fdn as \"itkAttrFdn\",
                t00.productdata as \"productData\",
                t03.uniquename as \"itk_nodename\",
                substring(t01.fdn from E'=([^,]+)\$') as \"itkAttrMoId\"
        FROM nd.cabinet AS t00
                LEFT JOIN fdn AS t01 ON t01.id=t00.itkid AND t01.class='Cabinet'
                LEFT JOIN fdn AS t02 ON t02.id=t00.itkid
                INNER JOIN nd.itk_uniquenodenames AS t03 ON t03.id=t02.parentnode
        WHERE (t00.itkdeltime IS NULL) AND (((t01.fdn)::text ~ E'ENB'))	
		) AS t11
        ORDER BY substring(\"itkAttrFdn\" from E'=([^,]+)\$') ASC;

";
my $sth = $dbh->prepare("$sql");
$sth->execute();
my @res = ();
my $data = ();
my %info = readHO();

while ( my @row = $sth->fetchrow_array() )
{
      $data->{fdn} = $row[0];
      if($row[1] =~ /.*=>\"(.*)\".*=>\"(.*)\".*=>\"(.*)\".*=>\"(.*)\".*=>\"(.*)\"/)
      {
       ($data->{productName},$data->{productRevision},$data->{productNumber},$data->{productionDate},$data->{serialNumber})
       =($1,$2,$3,$4,$5);
      }
      $data->{nodeName} = $row[2];
      $data->{hodate} = $info{$data->{nodeName}} ;
      $data->{internalwarranty}= verify_warranty($data->{productionDate},365);
      $data->{extendwarranty}= verify_warranty($info{$data->{nodeName}},2*365);
      $data->{productNumber} =~ s/\s//g if($data->{productNumber} =~ /.*\s*.*/); #remove space
      unless($data->{productName} eq ''and $data->{productRevision} eq '' and $data->{productNumber} eq '' and $data->{productionDate} eq '' and $data->{serialNumber} eq '' or $data->{serialNumber} =~/^\d$/ )     
      {
        push @res, $data;
      }
      $data = ();
}

my $json = new JSON;
my $json_text = $json->pretty->encode(\@res);
return $json_text;

}


sub verify_warranty
{
   my ($date,$period) = @_;
   if($date =~ /^(\d\d\d\d)(\d\d)(\d\d)$/)
  {
    
    if($1 > 1900 && $2 > 0 && $3 > 0)
    {
        ($year,$mon,$mday) = ($1-1900,$2-1,$3);        
    }
    
  }
  else
  {
#      print "\n".$date."\n";
      return "INVALID DATE";
  }

  ($sec, $min, $hours) = (0,0,0);
  $mon =~ s/.*(\d)$/$1/g  if ($mon > 12);
  $mday =~ s/.*(\d)$/$1/g if ($mday > 31);
  my $pagetime = timegm($sec, $min, $hours, $mday, $mon, $year);
  $days = (time-$pagetime)/(3600*24);
  if($days>= $period)
  {
    return "NO";
  }
  return "YES";
  
}










sub get_warranty
{
   my ($node,%info) = @_;

  #verify warranty
  if($info{$node} =~ /(\d\d\d\d)(\d\d)(\d\d)/)
  {
    ($year,$mon,$mday) = ($1-1900,$2-1,$3);
  }
  else
  {
    return "UNKNOWN";
  }

  ($sec, $min, $hours) = (0,0,0);
  my $pagetime = timegm($sec, $min, $hours, $mday, $mon, $year);
  $days = (time-$pagetime)/(3600*24);
  if($days>= 2*365)
  {
    return "NO";
  }
  return "YES";
}




sub readHO
{

my %info;  
$csvPATH = '/var/www/html/singtel/HWtracking/warranty.csv';
open(FH, $csvPATH);
my @files = <FH>;
close FH;
  #establish warranty info from csv file
   foreach my $item (@files)
   {
     chomp($item);
     ($nodename, $warranty) = split "," , $item;
     $info{$nodename} = $warranty;
  }
  return %info;
}


1;

