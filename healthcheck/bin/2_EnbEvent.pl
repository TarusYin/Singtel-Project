#!/usr/bin/perl
##
###
### Author: eqsvimp    Eric Yin Z
### $Id: itkreport 9360 2014-11-04 05:06:20Z eqsvimp $
##
### GSC CHINA Tool & Data Centre Service
### 
#
use FindBin qw($Bin);
use lib "../../../lib/";
use lib "$Bin/../lib/";
use Itk::Utils::LoginSession qw(eca_login node_login);
use List::Util qw/max min sum maxstr minstr shuffle/;
use Time::Local;
use Parallel::ForkManager;
use Itk::NitsPackServer;
use DBI;

use warnings;

#global variable
our $srv;
our %info;
my $dir = "/home/eqsvimp/healthcheck/report/spool/";
$maxProc = 5;
# multiple nodes
our @keynodelist = ("ENB_736491;10.245.99.69");
#our @keynodelist = ("ENB_730041","ENB_730181","ENB_730071");

#@remove = ("ENB_730181","ENB_730241","ENB_730071");
#@result = list(\@keynodelist,\@remove);

	


$configFile ||= "$Bin/../etc/itk.conf";
$srv=new Itk::NitsPackServer(config => $configFile);


use constant TIMEOUT => {
        'PMXML'         =>      {MAX_PROC_TIME  => 600,  TIMEOUT_POLL_INTERVAL=>3},
        'GPEH'          =>      {MAX_PROC_TIME  => 1200,TIMEOUT_POLL_INTERVAL=>60},
        'OSS'           =>      {MAX_PROC_TIME  => 2400,TIMEOUT_POLL_INTERVAL=>60},
};


my %ecainfo = (  'user'      => "eca",  
                 'password'  => "ecastm",  
                 'port'      => 30023,
                 'cliPrompt' => "\@",
    		         'host'      => "localhost",
  	       			 'timeout'   => 1000
);

my %ossinfo = (  'user'      => "",  
                 'password'  => "",  
                 'cliPrompt' => "\@ocsuas3>",
                 'timeout'   => 10000,                 
		             'host'      => ""
  
);

#@keynodelist = getNodelist();

sub getNodelist
{
    my %node;
    my $session = new Itk::Utils::LoginSession();
	  $session->setField(%ecainfo);
	  $session->ecaLogin();
	#######get OSS password
	  $session->{cliPrompt}= "\@eca";
	  $read = $session->commandCommit('ecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_lte');
#eca ~ \$ \e[0mecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_w \cMran | grep password\cM\cJ\cI       
# password => 'CNYfeb\@16',\cM\cJ\e[1;31meca\e[33m
    %ossinfo = getOSSinfo($read);
    $ossinfo{cliPrompt} = "\@.*>";
  	$session->setField(%ossinfo);
    $session->nodeLogin();
	  $read = $session->commandCommit('/opt/ericsson/ddc/util/bin/listme | grep "SubNetwork=ENB"');
    @res = captureFromTo($read,"HEAD%^&","TAIL%^&");
    #SubNetwork=ONRM_ROOT_MO_R,SubNetwork=ENB_PIONEER,MeContext=ENB_959313_SDV_Logistics@10.245.72.113@vF.1.107@4@OFF@2@3@ERBS_NODE_MODEL@1465366766527@1465189200831
    map{ $_ =~ s/.*MeContext=(ENB\_\d+)\_.*/$1/g } grep {/MeContext/} @res; 
    pop @res; shift @res; #remove the first and the last element


    $session->{cliPrompt}= "\@eca";
	  $session->commandCommit('exit');
	  $session->{cliPrompt}= "\@";
	  $session->commandCommit('exit');
	  $session->close();
	  return @res;

}
=pod
my ($nodesStarted,$nodesComplete,%childStartTimes)=();
($nodesStarted,$nodesComplete) = (0,0);
my $totalNodes = scalar @keynodelist;
my $pm = new Parallel::ForkManager($maxProc);
$pm->run_on_start(
        sub {
                my ($pid,$nodeId)=@_;
                $nodesStarted+=1;
                $childStartTimes{$pid}=time;
                my $nodesRemaining = $totalNodes-$nodesComplete;
                $srv->logInfo("Processing started for '$nodeId', Process ID '$pid' : total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining) ");

        }
);

$pm->run_on_finish(
        sub {
                my ($pid, $exit_code, $nodeId, $exitSig) = @_;
                $nodesStarted-=1;
                $nodesComplete+=1;
                my $nodesRemaining = $totalNodes-$nodesComplete;
                if ($exitSig) {
                        $srv->logError("Child process terminated on signal $exitSig");
                }
                my $runTime = time() - $childStartTimes{$pid};
                $srv->logInfo("Processing ended for '$nodeId', Process ID '$pid' : total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining) : runtime $runTime ");
                delete $childStartTimes{$pid};
        }
);

# To guard against hanging processes, we keep track of how long each child has
# been running and kill any that take longer than MAX_PROC_TIME
$SIG{ALRM}=sub {
        my $t=time;
        if (DEBUG3) {
                my @lines = map sprintf("\t$_ => %d s",$t - $childStartTimes{$_}), keys %childStartTimes;
                $srv->logInfo("Timeout Check: current process run times:\n".join("\n",@lines));
        }
        foreach my $pid (keys %childStartTimes) {
                if ($t - $childStartTimes{$pid} > TIMEOUT->{PMXML}->{MAX_PROC_TIME}) {
                        $srv->logInfo("Process pid=$pid exceeded MAX_PROC_TIME - killing it");
                        kill 15, $pid;
                }
        }
        alarm TIMEOUT->{PMXML}->{TIMEOUT_POLL_INTERVAL};
};
alarm TIMEOUT->{PMXML}->{TIMEOUT_POLL_INTERVAL};

=cut

########Mutiple processes start here

foreach my $item(@keynodelist)
{
    ($nodename,$nodeip) = split ";" , $item; 
    task_start($nodename,$nodeip,$dir,%ossinfo);
}

sub task_start
{
	my ($nodename,$nodeip,$dir,%ossinfo) = @_;
	my $session = new Itk::Utils::LoginSession();
	$session->setField(%ecainfo);
	$session->ecaLogin();

#######get OSS password
	$session->{cliPrompt}= "\@eca";
	$read = $session->commandCommit("ecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_wran");
#eca ~ \$ \e[0mecaadmin config -get server.hostTable -nwid stmsg | grep -A 10 oss_w \cMran | grep password\cM\cJ\cI       
# password => 'CNYfeb\@16',\cM\cJ\e[1;31meca\e[33m
  %oss = getOSSinfo($read);
  ($ossinfo{user},$ossinfo{password},$ossinfo{host}) = ($oss{user},$oss{password},$oss{host});
	$session->setField(%ossinfo);
	$session->nodeLogin();


#######login the node
	$session->{cliPrompt}= '.*\d\..*\S>';
  $session->commandCommit("amos $nodeip");
  $session->{cliPrompt}= "$nodename.*>";
  $session->commandCommit("lt all");  
  
  $command = "lga -m 1d";
	$session->{cliPrompt}= "Password";
	$session->commandCommit($command);
  $session->{cliPrompt}= "$nodename.*>";
  $read = $session->commandCommit("rbs");
	

##########command 
	$description = "Alarm & Event History";
	@res = captureFromTo($read,"HEAD","TAIL");
	my($critical,$major,$minor,$warnings) = (scalar(grep {/.*\s+C\s+.*/} @res),scalar(grep {/.*\s+M\s+.*/} @res),scalar(grep {/.*\s+m\s+.*/} @res),scalar(grep {/.*\s+w\s+.*/} @res));
	#write $info{$nodename}{comments} and $info{$nodename}{status}
	if($critical > 0){$info{$nodename}{$description}{status} = "Critical";$info{$nodename}{$description}{comments} = "Critical:- $critical;Major:- $major;Minor:- $minor;Warning:- $warnings";}
	elsif($major > 0){$info{$nodename}{$description}{status} = "Major";$info{$nodename}{$description}{comments} = "Critical:- $critical;Major:- $major;Minor:- $minor;Warning:- $warnings";}
	elsif($minor > 0){$info{$nodename}{$description}{status} = "Minor";$info{$nodename}{$description}{comments} = "Critical:- $critical;Major:- $major;Minor:- $minor;Warning:- $warnings";}
	elsif($warnings > 0){$info{$nodename}{$description}{status} = "Warning";$info{$nodename}{$description}{comments} = "Critical:- $critical;Major:- $major;Minor:- $minor;Warning:- $warnings";}
  #push @tmp , grep {/.*\s+M\s+.*w/} @res;
	
##########command 
  $command = "lgx";
  $read = $session->commandCommit($command);
  $description = "Active Alarms";
  @res = captureFromTo($read,"Timestamp (UTC)","TAIL");
  ($critical,$major,$minor,$warnings) = (scalar(grep {/.*\s+C\s+.*/} @res),scalar(grep {/.*\s+M\s+.*/} @res),scalar(grep {/.*\s+m\s+.*/} @res),scalar(grep {/.*\s+w\s+.*/} @res));
  
  if($critical > 0){$info{$nodename}{$description}{status} = "Critical";$info{$nodename}{$description}{comments} = "Critical:- $critical;Major:- $major;Minor:- $minor;Warning:- $warnings";}
  elsif($major > 0){$info{$nodename}{$description}{status} = "Major";$info{$nodename}{$description}{comments} = "Critical:- $critical;Major:- $major;Minor:- $minor;Warning:- $warnings";}
  elsif($minor > 0){$info{$nodename}{$description}{status} = "Minor";$info{$nodename}{$description}{comments} = "Critical:- $critical;Major:- $major;Minor:- $minor;Warning:- $warnings";}
  elsif($warnings > 0){$info{$nodename}{$description}{status} = "Warning";$info{$nodename}{$description}{comments} = "Critical:- $critical;Major:- $major;Minor:- $minor;Warning:- $warnings";}
  
  
##########command 
#====================================================================================================================
#SMN APN  BOARD    FAULT  OPER   MAINT   PRODUCTNUMBER  REV    SERIAL     DATE     TEMP COREMGR
#====================================================================================================================
#  0   1  DUL2001  OFF    ON     OFF     KDU137533/4    R1C    C824928275 20110725 54C  Active*
#--------------------------------------------------------------------------------------------------------------------

  $command = "cabh";
  $read = $session->commandCommit($command);
  $description = "Board Status";
  @res = captureFromTo($read,"HEAD","TAIL");
  for (0..$#res) 
  {
   if($res[$_] =~ /BOARD/) {$count = $_; last;}
  }
  @record = split ' ' , $res[$count+2];
  my ($BOARD,$FAULT,$OPER,$MAINT,$STAT) = ($record[2],$record[3],$record[4],$record[5],$record[6]);
  if($FAULT eq 'ON' or $OPER eq 'OFF'){$info{$nodename}{$description}{status} = "Critical";$info{$nodename}{$description}{comments} = "Board info: FAULT is $FAULT and OPERATION is $OPER";}
  elsif($MAINT eq 'ON'){$info{$nodename}{$description}{status} = "Major"; $info{$nodename}{$description}{comments} = "Board is working and is in maintenance";}
  if($STAT eq 'ON'){$info{$nodename}{$description}{status} = "Warning";}

  
##########command 
#==================================================================================================
#cellId froId froType         admState         opState      availabilityStatus
#==================================================================================================
#7      0     117506048 (FDD) 1 (UNLOCKED)     1 (ENABLED)  0 (NO_STATUS)
#8      1     117506048 (FDD) 1 (UNLOCKED)     1 (ENABLED)  0 (NO_STATUS)
#9      2     117506048 (FDD) 1 (UNLOCKED)     1 (ENABLED)  0 (NO_STATUS)
#
#==================================================================================================

  $command = "cell list";
  $read = $session->commandCommit($command);
  $description = "Cell status/availability state";
  @res = map {print $_."\n"; if($_ =~ /.*\(TDD|FDD\).*\((UNLOCKED|LOCKED)\).*\((ENABLED|DISABLED)\).*\((NO_STATUS|DEGRADED)\)/) { $_ = $1.";".$2.";".$3;}} grep {/.*\(TDD|FDD\).*/} captureFromTo($read,"cellId","TAIL");
  
  my ($num_admState,$num_opState,$num_availabilityStatus) = (0,0,0);
    
  foreach my $item (@res)
  {
    my ($admState,$opState,$availabilityStatus) = split ';' , $item;
    if($admState ne "UNLOCKED"){$num_admState++;}
    if($opState ne "ENABLED"){$num_opState++;}
    if($availabilityStatus ne "NO_STATUS"){$num_availabilityStatus++;}
    if($num_opState > 0 )
    {
      $info{$nodename}{$description}{status} = "Critical";
      $info{$nodename}{$description}{comments} = "$num_opState cells are in disabled state";
    }
    elsif($num_availabilityStatus > 0)
    {
      $info{$nodename}{$description}{status} = "Major";
      $info{$nodename}{$description}{comments} = "$num_availabilityStatus cells having degraded status";
    }
    elsif($num_admState > 0)
    {
      $info{$nodename}{$description}{status} = "Warning";
      $info{$nodename}{$description}{comments} = "$num_admState cell are in Locked state";
    }  
 
  }


##########command 
#/d           976M   905M  93%    70M   7% FRW usbdd 0 <4-1999999>
#/zip           1K     1K 100%     0K   0% -RW none
#/c          5859M  1080M  18%  4778M  82% FRW usbdd 0 <2000000-13999999>
  $command = "vols";
  $read = $session->commandCommit($command);
  $description = "Disk volume space";
  @res = captureFromTo($read,"volume","TAIL");
  @res = map {
    if($_ =~ /.*\s+(\d+)\%.*\s+(\d+)\%.*/)
    {
      $_= max($1,$2);
    }
  } 
  grep {/^\/c|^\/d/} @res;
  my $disk_space = max @res;
  if($disk_space > 50 and $disk_space <= 70) {$info{$nodename}{$description}{status} = "Warning";$info{$nodename}{$description}{comments} = "Disk space is highly used($disk_space%)";}
  if($disk_space > 70 and $disk_space <= 80) {$info{$nodename}{$description}{status} = "Minor";$info{$nodename}{$description}{comments} = "Disk space is highly used($disk_space%)";}
  if($disk_space > 80 and $disk_space <= 90) {$info{$nodename}{$description}{status} = "Major";$info{$nodename}{$description}{comments} = "Disk space is highly used($disk_space%)";}
  if($disk_space > 90) {$info{$nodename}{$description}{status} = "Critical";$info{$nodename}{$description}{comments} = "Disk space is highly used($disk_space%) and will be full";}


##########command 
#nodeSystemClock                      3 (HOLD_OVER_MODE)  

  $command = "get TransportNetwork=1,Synchronization=1";
  $read = $session->commandCommit($command);
  $description = "Network Synchronization";
  @res = captureFromTo($read,"nodeSystemClock","TAIL");
  my $Synchronization = "";
  if($res[0] =~/nodeSystemClock.*\((.*)\)/){$Synchronization = $1;}
  if($Synchronization ne "LOCKED_MODE")
  {
    if($Synchronization eq "FREE_RUNNING_MODE") {$info{$nodename}{$description}{status} = "Critical";$info{$nodename}{$description}{comments} = "Synchronization is in $Synchronization mode";}
    if($Synchronization eq "LOSS_OF_TRACKING_MODE" or $Synchronization eq "HOLD_OVER_MODE") {$info{$nodename}{$description}{status} = "Major";$info{$nodename}{$description}{comments} = "Synchronization is in $Synchronization mode";}    
  }




#Timestamp (UTC)     RestartType/Reason         Configuration Version                    SwRelease  CPP Downtime    Appl. Downtime  JVM Downtime
#=====================================================================================================================================
#2016-04-15 02:47:25 Spontaneous                CVBEFOREINTEGRATION                      ERBS_G     658s (10m58s)   660s (11m0s)    720s (12m0s)
#2016-06-28 06:33:42 PartialOutage              11% Cell 6 (Manual)                      ERBS_G     3s                             
#2016-06-28 06:34:52 PartialOutage              14% Cell 8 7 (Manual)                    ERBS_G     13s                            
#2016-06-28 06:35:09 PartialOutage              11% Cell 9 (Manual)                      ERBS_G     6s                             

#Node uptime since last restart: 1829149 seconds (21 days, 4 hours, 5 minutes, 49 seconds)
  $command = "lgd";
  $session->commandCommit("lt all");
  $read = $session->commandCommit($command);
  $description = "Node Restart and System Downtime";
  @res = captureFromTo($read,"Timestamp (UTC)","Node uptime");
  my($sec, $min, $hours, $mday, $mon, $year, $restart,$timestamp);
  foreach my $item (reverse @res)
  {
    if($item =~ /(\d+)-(\d+)-(\d+)\s(\d+):(\d+):(\d+)\s(\w+).*/)
    { 
      ($sec, $min, $hours, $mday, $mon, $year, $restart)=($6,$5,$4,$3,$2,$1,$7);
      $timestamp = "$year-$mon-$mday $hours:$min:$sec";
      last;
    }
  }
            $sec+=0; $min+=0; $hours+=0; $mday+=0; $mon-=1; $year-=1900;
            my $pagetime = timegm($sec, $min, $hours, $mday, $mon, $year);
            if(time - $pagetime <= 60*60*24*7)
            {
              $info{$nodename}{$description}{status} = "Major";$info{$nodename}{$description}{comments} = "Last Node restart happened on $timestamp";
            }




#===================================================================================
#Proxy  Adm State     Op. State     MO
#===================================================================================
# 2023  1 (UNLOCKED)  1 (ENABLED)   ENodeBFunction=1,TermPointToMme=3
# 2024  1 (UNLOCKED)  1 (ENABLED)   ENodeBFunction=1,TermPointToMme=4
# 2025  1 (UNLOCKED)  1 (ENABLED)   ENodeBFunction=1,TermPointToMme=5
# 2026  1 (UNLOCKED)  1 (ENABLED)   ENodeBFunction=1,TermPointToMme=6
#===================================================================================
#Total: 4 MOs


  $command = "st mme";
  $read = $session->commandCommit($command);
  $description = "S1 link status (TermpointToMme)";
  @res = captureFromTo($read,"Proxy","Total:");
  @res = grep {/.*\(\w+\).*\(\w+\).*/} @res;
  my ($num_Proxy,$num_Unlocked,$num_Enabled,$num_UnlockedDisabled) = (scalar @res,0,0,0);
  foreach my $item (@res)
  {
    if($item =~ /.*\((\w+)\).*\((\w+)\).*/)
    {
      if($1 eq "UNLOCKED"){$num_Unlocked++;}
      if($2 eq "ENABLED"){$num_Enabled++;}
      if($1 eq "UNLOCKED" and $2 eq "DISABLED"){$num_UnlockedDisabled++;}
    }
  }
  $num_locked = $num_Proxy-$num_Unlocked;
  if($num_Unlocked == $num_Proxy and $num_Enabled == 0){$info{$nodename}{$description}{status} = "Critical";$info{$nodename}{$description}{comments} = "$num_Proxy plugin units are disabled";}
  elsif($num_UnlockedDisabled >=1 and $num_UnlockedDisabled < $num_Proxy){$info{$nodename}{$description}{status} = "Major";$info{$nodename}{$description}{comments} = "$num_UnlockedDisabled units are disabled";}
  elsif($num_Unlocked < $num_Proxy){$info{$nodename}{$description}{status} = "Warning";$info{$nodename}{$description}{comments} = "$num_locked units are locked";}


#Process                                   Status     AddedTraceConditions
#omfLicenseHandlerOutputPowerC             saved      all
  $command = "fte status";
  $read = $session->commandCommit($command);
  $description = "Status of enabled traces";
  @res = captureFromTo($read,"Process","TAIL");
  @res = map {if($_ =~/(\w+)\s+.*/) { $_ = $1;}} grep {/.*\w+.*\s+.*/} grep{!/Process\s+Status\s+.*/i}@res;
  $fte_status = join "-" , @res;
  if(@res) {$info{$nodename}{$description}{status} = "Warning";$info{$nodename}{$description}{comments} = "$fte_status is found and it may lead to increase in processor load"}

    
#===================================================================================
#Proxy  Adm State     Op. State     MO
#===================================================================================
#  476  1 (UNLOCKED)  1 (ENABLED)   Equipment=1,Subrack=1,Slot=1,PlugInUnit=1
#  661  1 (UNLOCKED)  1 (ENABLED)   Equipment=1,RbsSubrack=1,RbsSlot=1,AuxPlugInUnit=RU-1-1
#  672  1 (UNLOCKED)  1 (ENABLED)   Equipment=1,RbsSubrack=1,RbsSlot=2,AuxPlugInUnit=RU-1-2
#  683  1 (UNLOCKED)  1 (ENABLED)   Equipment=1,RbsSubrack=1,RbsSlot=3,AuxPlugInUnit=RU-1-3
#  694  1 (UNLOCKED)  0 (DISABLED)  Equipment=1,RbsSubrack=1,RbsSlot=4,AuxPlugInUnit=RU-1-4
 
  $command = "st plugin";
  $read = $session->commandCommit("cvls");
  @res = captureFromTo($read,"Total:","TAIL");
  if($res[0] =~ /Total:.*(\d+).*CV.*(\d+).*UP.*/)
  {
    ($num_CV,$num_UP) = ($1,$2);
  }
  $read = $session->commandCommit($command);
  $description = "Status of Plug-in-unit/devices";
  @res = captureFromTo($read,"Proxy","TAIL");
  @res = grep {/.*\(\w+\).*/} @res;
  my ($num_critical,$num_warning) = (0,0);
  foreach my $item (@res)
  {
    if($item =~ /.*\((\w+)\).*\((\w+)\).*/)
    {
       if($1 eq "UNLOCKED" and $2 eq "DISABLED"){$num_critical++;}
       if($1 eq "LOCKED"){$num_warning++;}
    }

  }
  $tmp_comment = "";
  if($num_CV > 40 and $num_CV <= 45)
  {
    $info{$nodename}{$description}{status} = "Warning";
    $tmp_comment = "Total number of CV $num_CV Upgrade packages are $num_UP";
    $info{$nodename}{$description}{comments} = "Total number of CV $num_CV Upgrade packages are $num_UP";
  }
  if($num_warning > 0)
  {
    $info{$nodename}{$description}{status} = "Warning";
    $info{$nodename}{$description}{comments} = "$num_warning pluginunit are locked ".$tmp_comment;
  }
  
  if($num_CV > 45){$info{$nodename}{$description}{status} = "Major";$info{$nodename}{$description}{comments} = "Total number of CV $num_CV Upgrade packages are $num_UP";$tmp_comment= "Total number of CV $num_CV Upgrade packages are $num_UP";}
  if($num_critical > 0){$info{$description}{status} = "Critical";$info{$nodename}{$description}{comments} = "$num_critical plugin units are disabled ".$tmp_comment;}
  
    
#====================================================================================================================
#SMN APN PORT     BOARD    AuxPIU         TX1(W/dBm)  TX2(W/dBm)  VSWR1 (RL1) VSWR2 (RL2)  Cells (cellId,PCI)
#====================================================================================================================
#  0   1 BXP_0    RUS      1/1/RU-1-1     8.6 (39.3)         N/A  1.16 (22.6) N/A          7304631 (1,408)
#  0   1 BXP_0_1  RUS      1/2/RU-1-2     4.4 (36.4)         N/A  1.17 (22.3) N/A          7304631 (1,408)
#  0   1 BXP_1    RUS      1/3/RU-1-3     2.2 (33.3)         N/A  1.13 (24.2) N/A          7304632 (2,7)
#  0   1 BXP_2    RUS      1/5/RU-1-5     5.5 (37.4)         N/A  1.25 (19.0) N/A          7304633 (3,424)
#  0   1 BXP_2_1  RUS      1/6/RU-1-6     6.1 (37.8)         N/A  1.16 (22.6) N/A          7304633 (3,424)
#  0   1 BXP_3    RUS01B3  1/7/RU-1-7     6.8 (38.3)         N/A  1.13 (24.1) N/A          7304634 (4,182)
#  0   1 BXP_3_1  RUS01B3  1/8/RU-1-8     9.5 (39.8)         N/A  1.29 (18.0) N/A          7304634 (4,182)
#  0   1 BXP_4    RUS01B3  1/9/RU-1-9    38.3 (45.8)         N/A  1.25 (19.2) N/A          7304635 (5,24)
#  0   1 BXP_4_1  RUS01B3  1/10/RU-1-10  33.4 (45.2)         N/A  1.27 (18.4) N/A          7304635 (5,24)
#  0   1 BXP_5    RUS01B3  1/11/RU-1-11   8.3 (39.2)         N/A  1.22 (20.0) N/A          7304636 (6,102)
#  0   1 BXP_5_1  RUS01B3  1/12/RU-1-12   7.6 (38.8)         N/A  1.11 (25.8) N/A          7304636 (6,102)
#--------------------------------------------------------------------------------------------------------------------
    $command = "Cabx";
    $read = $session->commandCommit($command);
    $description = "VSWR check/TX transmitt Power";
    @res = captureFromTo($read,"SMN APN","TAIL:");
    @res = grep {/.*\d+\s+\d+\s+\w+.*\(\d.*\).*/} @res;
    my ($VS_critical,$VS_major,$VS_warning)=(0,0,0);
    foreach my $item (@res)
    {
      @tmp = split /\s+/ , $item;
      if($item =~/.*\(\d+\.\d\).*(\d\.\d+).*\(\d+.\d\).*/)
      {
        print $1;

      }
      if($tmp[9] > 1.3 and $tmp[9] < 1.4){$VS_warning++;}
      if($tmp[9] >= 1.4 and $tmp[9] < 1.5){$VS_major++;}
      if($tmp[9] >= 1.5 ){$VS_critical++;}      
    }
    if($VS_warning >0){$info{$nodename}{$description}{status} = "Warning";$info{$nodename}{$description}{comments} = "VSWR values for one or more($VS_warning) RU between 1.3 to 1.4";} 
    if($VS_major >0){$info{$nodename}{$description}{status} = "Major";$info{$nodename}{$description}{comments} = "VSWR values for one or more ($VS_major)RU between 1.4 to 1.5";} 
    if($VS_critical >0){$info{$nodename}{$description}{status} = "Critical";$info{$nodename}{$description}{comments} = "VSWR values for one or more($VS_critical) RU greater than 1.5";} 
  

#Total: 37 MOs
    $command = "st enb 1.*0";
    $read = $session->commandCommit($command);
    $description = "X2 link status";
    @res = grep {/Total:/} captureFromTo($read,"Total:","TAIL:");
    if($res[0] =~ /Total:\s+(\d+).*MO.*/)
    {
      $info{$nodename}{$description}{status} = "Minor";$info{$nodename}{$description}{comments} = "$1 X 2 Links in disabled state";
    }

  writeCSV($dir,$nodename,%info);

####
###exit session####
	close FH;
	$session->{cliPrompt} = $ossinfo{cliPrompt};
	$session->commandCommit("exit");
	$session->{cliPrompt} = $ecainfo{cliPrompt};
	$session->{timeout}= 1;
	$session->commandCommit("exit");
	$session->{cliPrompt} = "localhost";
	$session->commandCommit("exit");
  $session->close();
	$srv->logInfo("$nodename => $command => comments :".$info{$nodename}{$description}{comments});
	$srv->logInfo("$nodename collection finished "); 


}

sub writeCSV
{
	my ($dir,$nodename,%info) = @_;
	$srv->logInfo("write file >> $dir$nodename"); 
	open(FH, ">>$dir$nodename");
	foreach $key (sort keys %info)
  {
    $list = $info{$key};
    foreach $description (sort keys %$list)
    {
      print FH "$nodename,$description,$info{$nodename}{$description}{status},$info{$nodename}{$description}{comments}\n";
  
    }
    
  }

  
}



sub getOSSinfo
{
    my $read = shift;
    my ($username,$password,$hostaddress,%ossinfo);
    if($read =~ /.*username.*\>\s'(.*)'/) { $username = $1;}  #'CNYfeb@16',
	if($read =~ /.*password.*\>\s'(.*)'/) { $password = $1;}
	if($read =~ /.*hostaddress.*\>\s'(.*)'/) { $hostaddress = $1;}
	($ossinfo{user},$ossinfo{password},$ossinfo{host}) = ($username,$password,$hostaddress);
    return %ossinfo;
}





sub captureFromTo
{
    my ($read,$from,$to) = @_;
    my @result;
    my $count = 0;
    my @rows =  split '\cM\cJ', $read;
    my ($start,$end) = (0,$#rows);
    foreach my $row (@rows)
   {
  if($row =~ /$from/)
        {
          $start = $count;
          $from = "&$^&";
          next;
        }
        if($row =~ /$to/)
        {
          $end = $count;
          last;
        }
        $count++;
   }    
    for ($start..$end){ push @result, $rows[$_];} 
    return @result;
}


sub list
{
my($x,$y)=@_;   
print "list a： @$x \n";
print "list b: @$y \n";
my @a=@$x;
my @b=@$y;
my %a = map{$_ => 1} @a;
my %b = map{$_ => 1} @b;
my @c = map{$_ => 1} @a; 

my @inter = grep {$a{$_}} @b; 
print "inner：@inter \n";

my %merge = map {$_ => 1} @a,@b;
my @merge = keys (%merge);
print "merge：@merge \n";

my @ca = grep {!$a{$_}} @merge;  
my @cb = grep {!$b{$_}} @merge;  
print "\@a complementary set:@ca \n";
print "\@b complementary set:@cb \n";
return @cb;
}
