#!/usr/bin/perl
###----------------------------------------------------------------------------
#
# Utility to muck about with eventcollect history database
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
#
# Author: eharmic
# $Id: edb 4443 2011-02-08 04:09:16Z eanzmark $

use strict;
use FindBin qw($Bin);
use lib "$Bin/../lib";
use Itk::NitsPackServer;
use Itk::CollectionHistory;
use Data::Dumper;
use Getopt::Long;

#------------------------------------------------------------------------------
sub usage {
	print <<_EOF_;
edb - View/Edit eventcollector history log

Dump key(s):
  edb -f dbfilename -n <node> [-l <logstore>] -d [-k <regex>] [-c <regex>]

Set key:
  edb -f dbfilename -n <node> [-l <logstore>] -k <keyname> -c <classname> -s <value>

Remove Key:
  edb -f dbfilename -n <node> [-l <logstore>] -k <keyname> -c <classname> -r

_EOF_
	exit;
}

#------------------------------------------------------------------------------
# MAIN
#------------------------------------------------------------------------------

my (
	$help,
	$logStore,
	$dump,
	$node,
	$keyname,
	$classname,
	$set,
	$remove,
	$dbfilename
);

GetOptions(
        'h|help' 		=> \$help,
	'f=s'			=> \$dbfilename,
	'l|logstore=s'		=> \$logStore,
	'n|node=s'		=> \$node,
	'd'			=> \$dump,
	'k=s'			=> \$keyname,
	'c=s'			=> \$classname,
	's=s'			=> \$set,
	'r'			=> \$remove,
) or die;

usage if $help;

my $configFile = "$Bin/../etc/itk.conf";
our $srv;
$srv=new Itk::NitsPackServer(config => $configFile);

$logStore ||= $srv->getConfig("event.logstore") || "$ENV{HOME}/logstore/event";
#usage() if $help;
#die "No ipdatabase specified and could not find one\n" unless $ipdatabase;
die "Node required" unless $node;
$dbfilename = 'event.db' unless $dbfilename;
die "CollectionHistory DB not found\n" unless -e "$logStore/$dbfilename";

my $ch = new Itk::CollectionHistory(
	node => $node,
	storedir => $logStore,
	dbfilename => $dbfilename
);

$Data::Dumper::Terse=1;

if ($dump) {
	my $classRe;
	if ($classname) {
		$classRe=qr/$classname/i;
	}
	my $keyRe;
	if ($keyname) {
		$keyRe=qr/$keyname/i;
	}
	foreach my $class ($ch->getAllClasses()) {
		next if defined $classname && $class!~$classRe;
		print "Class: $class\n";
		foreach my $key ($ch->getAllKeys($class)) {
			next if defined $keyname && $key!~$keyRe;
			print "Key: $key\n";
			print "Value: ",Dumper($ch->get($key, $class)),"\n";
		}
	}
} elsif ($set) {
	die "Key mandatory when setting values\n" unless $keyname;
	die "Classname mandatory when setting values\n" unless $classname;
	my $value=eval $set;
	if ($@) {
		die "Error evaluating set expression: $@";
	}
	$ch->put($keyname,$value,$classname);
	$ch->commit;
} elsif ($remove) {
	die "Classname mandatory when removing values\n" unless $classname;
	if ($keyname) {
		$ch->rm($keyname, $classname);
	} else {
		$ch->rmclass($classname);
	}
	$ch->commit;
} else {
	die "What do you want me to do? -d (dump), -s (set), -r (remove)\n";
}

