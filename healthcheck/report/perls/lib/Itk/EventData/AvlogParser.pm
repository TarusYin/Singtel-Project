#!/usr/bin/perl
###----------------------------------------------------------------------------
# Parser/Analyser for Cello Availability Log files
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
#
# Author: EHARMIC
# $Id: AvlogParser.pm 7650 2012-09-28 05:56:32Z eanzmark $

package Itk::EventData::AvlogParser;
use strict;
use Itk::EventData::EventRec;
use Time::Local qw(timegm_nocheck);
use Hash::Util qw(lock_keys);

BEGIN {
	if (DEBUG6) {
		require Data::Dumper;
		import Data::Dumper;
	}
}

#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	# Result Codes
	OK => 1,			# Operation succeeded
	PARSE_FAILED => -1,		# Parsing failed
	OK_MISSEDDATA => 2,		# Operation succeeded but there was some missing data
	# Parser state constants
	PIUIN => 	0x01,		# Event was a PIU In
	PIUOUT => 	0x02,		# Event was a PIU Out
	PIUMASK =>	0x03,		# Mask for detecting PIU related event
	NODEIN =>	0x04,		# Event was node IN
	NODEOUT =>	0x08,		# event was node OUT
	NODEMASK =>	0x0C,		# Mask for detecting NODE related event
	OTHER =>	0x10,		# Event was OtherEvent	
	PGMMASK =>  0x23,       # Mask for detecting
	PGMOUT =>   0x22,       # event was program out
	PGMIN =>    0x21,       # event was program in 
	
	# Definition of nodeRestartState bitmap
	NR_STARTED => 		0b00000001,     # A restart has started
	NR_RESTARTED => 	0b00000010,	# Subsequent restart has started
	NR_NODERESTARTED => 	0b00000100,	# NodeRestarted message seen
	NR_APPOPER =>		0b00001000,	# Application Operational seen
	NR_CELLOOPER =>		0b00010000,	# Cello Operational Seen
	NR_COMPLETE =>		0b00011100,	# Mask for all needed indications for a restart to be complete
	# Sanity Checks
	T_MAXNODERESTART => 7200,	# Max amount of time a node restart can take before we consider it faulty
};
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'collHist',		# Ref to collection history object
	'nodename',
	'nodetype',
	'startdate',		# Timestamp (unix epoch) of start time filter
	'downtimes',		# Index of downtimes. Stored here so that they can later be used for uprgade analysis
	'stateData',		# Persistent State Data transferred from one run to the next
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;
*srv=\$main::srv;
#==============================================================================
# CLASS METHODS
#==============================================================================

#==============================================================================
# PRIVATE METHODS
#==============================================================================
##-----------------------------------------------------------------------------
# _warn($msg)
# Issue warning, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _warn {
	$srv->logWarn("($_[0]->{nodename}) $_[1]",(caller(0)));
}

##-----------------------------------------------------------------------------
# _error($msg)
# Issue Error, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _error {
	$srv->logError("($_[0]->{nodename}) $_[1]", (caller(0)));
}

##-----------------------------------------------------------------------------
# _bsearch($t)
# Search through the downtimes, to find the entry at or immediately before $t
# - $t 	Time to search for
# RETURNS: $i
# - $i	Index of entry that is at or immediately before the given time, or -1
#	if the search time is before the first entry in the table.
#------------------------------------------------------------------------------
sub _bsearch {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $t)=@_;
	my $d = $self->{downtimes};
	return -1 if ($t < $d->[0][0]);
	my $maxindex = $#{$d};
	my $minindex = 0;
	return $maxindex if ($t > $d->[$maxindex][0]);
	my $i = int($maxindex / 2);
	for (;;) {
		if ($d->[$i][0] <= $t) {
			return $i if $d->[$i+1][0] > $t;
			$minindex=$i;
			$i = $i + int(($maxindex - $i)/2);
		} else {
			return $i-1 if $d->[$i-1][0] <= $t;
			$maxindex=$i;
			$i = $i - int(($i-$minindex)/2);
		}
	}
}

##-----------------------------------------------------------------------------
# _fmtTime($ts)
# Format a timestamp as an ISO 8601 string
# - $ts		Timestamp in epoch seconds
# - $f		Format. 0=yyyy-mm-dd hh:mm; 1=yyyy-mm-dd
# RETURNS: $time
# - $time 	Time as a string
#------------------------------------------------------------------------------
sub _fmtTime {
	my ($ts, $f)=@_;
	my ($s,$m,$h,$dd,$mm,$yy)=gmtime($ts);
	if ($f==1) {
		return sprintf("%04d-%02d-%02d", $yy+1900,$mm+1,$dd);
	} else {
		return sprintf("%04d-%02d-%02d %02d:%02d:%02d", $yy+1900,$mm+1,$dd,$h,$m,$s);
	}
}

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new($collHist, %opts)
# Constructor
# - $collHist	Ref to instance of collection history
# % nodename	Name of node
# % nodetype	Type of node
# % startdate 	Starting date as a unix epoch timestamp; PMD files earlier than this will not be fetched
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $collHist, %opts)=@_;
	$self=fields::new($self) unless ref $self;
	$self->{collHist}=$collHist;
	$self->{nodename}=$opts{nodename};
	$self->{nodetype}=$opts{nodetype};
	$self->{startdate}=$opts{startdate};
	$self->{downtimes}=[];
	return $self;
}

##-----------------------------------------------------------------------------
# parse($file, $callback, $pmdParser, $syslogParser, $lastrecord, %opts)
# Parses the indicated avlog file
# - $file	Filename of syslog file to parse
# - $callback	Call back sub to recieve results
# - $pmdParser	PMD Parser Instance, to obtain PMD Info for auto restarts
# - $sysLogParser	Syslog Parser instance, to obtain syslog related info
# - $lastrecord	Record ID of the last processed record. If this is not undef,
#		then we skip forward through the file until we find a matching
#		record; otherwise we just process all records in the file
# % dumpfh	Debugging option: if provided, should be a filehandle into which
#		all records found will be dumped.
# RETURNS: $resultcode, $lastrecord, $missingRecs
# - $resultCode	  Result Code - see constants for definitions
# - $lastrecord   Record ID of the last processed record
# - $missingRecs  Flag: if true, we could not find lastrecord so some records have been missed.
# - $startTime    Starting time for the time window covered by this parse (epoch secs)
# - $endTime      Ending time for the time window covered by this parse (epoch secs)
#------------------------------------------------------------------------------
sub parse {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $file, $callback, $pmdParser, $syslogParser, $lastrecord, %opts)=@_;
	lock_keys(%opts, qw( dumpfh ));
	
	my $parseErr = sub {
		$self->_error("Parse Error: $file:$. $_[0]");
	};
	
	my (
		@avevents,	# Array into which found avlog entries are placed for later analysis
		$maxlogrec,	# Ending log record number processed
		$minlogrec,	# Starting log record number found
		$startTime,	# Start time for this parse (either time of last processed, or first record found if none)
		$firstTime,	# Timestamp of earliest record in the file
		$endTime	# Time of last record found in the file
	);
	
	my $fh;
	open $fh, $file or do {
		$self->_error("Failed to open '$file': $!");
		return PARSE_FAILED;
	};
	
	my $maxlogrec;
	
LOGRECORD:
	while (<$fh>) {
		if (/^\s*<LogRecord number = "(\d+)">/) {
			my $logRec=$1;
			next LOGRECORD if (defined $lastrecord && $logRec < $lastrecord);
			$maxlogrec = $logRec unless defined $maxlogrec && $logRec < $maxlogrec;
			$minlogrec = $logRec unless defined $minlogrec && $logRec > $minlogrec;
			my ($ts, $eventType, $reason, $cause, $infoText, $rank, $piutype, $board, $cv, $up, $addInfo,$swprodno, $swprodrev, $hwprodno, $hwprodrev );
			while (<$fh>) {
				
				if (! defined $ts && /<TimeStamp>/) {
					$_=<$fh>;
					my ($yy,$mm,$dd)=(/^\s*<year>\s*(\d+)\s*<\/year>\s*<month>\s*(\d+)\s*<\/month>\s*<day>\s*(\d+)\s*<\/day>/) or do {
						&$parseErr("Expected year,month & day");
						next LOGRECORD;
					};
					$_=<$fh>;
					my ($h,$m,$s)=(/^\s*<hour>\s*(\d+)\s*<\/hour>\s*<minute>\s*(\d+)\s*<\/minute>\s*<second>\s*(\d+)\s*<\/second>/) or do {
						&$parseErr("Expected hour/ minute & second");
						next LOGRECORD;
					};
					$_=<$fh>;
					/^\s*<\/TimeStamp>/ or do {
						&$parseErr("Expected: </TimeStamp>");
						next LOGRECORD;
					};
					
					eval {
						$ts=timegm_nocheck($s,$m,$h,$dd,$mm-1,$yy);
					};
					if ($@) {
						&$parseErr("Invalid Timestamp: $yy/$mm/$dd $h:$m:$s");
						next LOGRECORD;
					};
					
					if ($ts > 79833599) { # Skip the next part if time is in 1970
						if (defined $lastrecord) {
							# Starting time for this parse is actually the time of the last record
							# we parsed the last time (if any)
							$startTime = $ts if $logRec == $lastrecord;
						}
						$firstTime=$ts unless defined $firstTime and $firstTime < $ts;
						$endTime=$ts unless defined $endTime and $endTime > $ts;
					}
				}
				
			    if (! defined $eventType && /^\s*<(Program|PlugInUnit|Node)Event\/>\s*<(OutOf|In)Service\/>/) {
					if ($1 eq 'PlugInUnit') {
						$eventType=($2 eq 'In') ? PIUIN : PIUOUT;
					} elsif ($1 eq 'Node') {
						$eventType=($2 eq 'In') ? NODEIN : NODEOUT;
					} else {
						$eventType=($2 eq 'In') ? PGMIN : PGMOUT;
					}
					next;
				}
				
				if (! defined $eventType && /<OtherEvent\/>/) {
					$eventType=OTHER;
					next;
				}
				
				if ($eventType &  (PIUMASK | NODEMASK | PGMMASK)) {
					if (/^\s*<EventReason>\s*(\w+)\s*<\/EventReason>/) {
						$reason=$1;
						next;
					}
					
					if (/^\s*<AdditionalInfo>/) {
						while (<$fh>) {
							if (/^\s*<InfoText>\s*(.+)\s*<\/InfoText>/) {
								$infoText=$1;
								next;
							}
							
							if (/^\s*<Rank(\w+)\/>/) {
								$rank=$1;
								next;
							}
							
							if (/^\s*<Cause>\s*(\S+)\s*<\/Cause>/) {
								$cause=$1;
								next;
							}
							
							
							if (/^\s*<App[lL]og>\s*(<RestartCompleted\/>\s*)?(?:<NodeInfo>\s*(\S.+\S)\s*<\/NodeInfo>\s*)?<\/AppLog>/) {
								$cause="RestartCompleted" if ($1);
								$addInfo=$2;
								next;
							}
							if (/^\s*<CppCore>/) {
								# If this is an 'operational' event coming from Cello then
								# we change the reason to 'CelloOperational' so we can distinguish
								# from other operational indications
								$reason='CelloOperational' if $reason eq 'Operational';
								next;
							}
							# Accumulate other text lines within the additionalInfo tags
							# NB. This is very fragile, change of whitespace placement will break this!
							unless (/^\s*</) {
								my ($text) = (/^\s*(\S.+)\s*$/);
								$addInfo.=$text if $text;
								next;
							}
							last if /<\/AdditionalInfo>/;
						}
					}
				}

				if ($eventType & PIUMASK|PGMMASK) {
					if (/^\s*<PiuType>\s*(\w+)\s*<\/PiuType>/) {
						$piutype=$1;
						next;
					}
					if (/^\s*<PiuAddress>/) {
						my ($smn, $apn);
						$_=<$fh>;
						($smn)=(/^\s*<SwitchModuleNumber>\s*(\d+)\s*<\/SwitchModuleNumber>/) or do {
							&$parseErr("Expected SwitchModuleNumber");
							next LOGRECORD;
						};
						$_=<$fh>;
						($apn)=(/^\s*<SwitchPortNumber>\s*(\d+)\s*<\/SwitchPortNumber>/) or do {
							&$parseErr("Expected SwitchPortNumber");
							next LOGRECORD;
						};
						$board=sprintf("%02d%02d00",$smn,$apn);
						$_=<$fh>; # discard </PiuAddress>
						next;
					}
					
					if (/^\s*<SwPid>\s*/) {
                        while (<$fh>) {
                        	if (/^\s*<ProdNo>\s*([\w%]+)\s*<\/ProdNo>/) {
                        		$swprodno = $1;
                        		next;
                        	}
                        	if (/^\s*<ProdRev>\s*(\w+)\s*<\/ProdRev>/) {
                        		$swprodrev = $1;
                        		next;
                        	}
                            last if /<\/SwPid>/;
                        }
                    }
                    
                    if (/^\s*<HwPid>\s*/) {
                        while (<$fh>) {
                        	if (/^\s*<ProdNo>\s*([\w%\/]+)\s*<\/ProdNo>/) {
                        		$hwprodno = $1;
                        		next;
                        	}
                        	if (/^\s*<ProdRev>\s*(\w+)\s*<\/ProdRev>/) {
                        		$hwprodrev = $1;
                        		next;
                        	}
                            last if /<\/HwPid>/;
                        }
                    }
					
				}
				
				if ($eventType == OTHER && /^\s*<AvailabilityInfo>/) {
					while (<$fh>) {
						if (/^\s*<NodeIdReason>\s*(\w+)\s*<\/NodeIdReason>/) {
							$reason=$1;
							next;
						}
						if (/^\s*<ConfigurationVersion>\s*([^\s<]+)\s*<\/ConfigurationVersion>/) {
							$cv=$1;
							next;
						}
						if (/^\s*<UpgradePackage>\s*([^\s<]+)\s*<\/UpgradePackage>/) {
							$up=$1;
							undef $up unless $up =~ /^CXP/i; # Discard spurios output: sometimes you get 'no' or 'Not'
							next;
						}
						# Accumulate other text lines within the additionalInfo tags
						# NB. This is very fragile, change of whitespace placement will break this!
						unless (/^\s*</) {
							my ($text) = (/^\s*(\S.+)\s*$/);
							$addInfo.=$text if $text;
							next;
						}
						last if /^\s*<\/AvailabilityInfo>/;
					}
					next;
				}
				
				# Try to find end of record, even if malformed
				last if /^\s*<\/Log/;
				
			}
			
			# Unfortunately, there is no guarantee that records stored in the file will be in
			# chronological sequence, since the file wraps around. Therefore, we place
			# all the entries into an array; once the whole file is read we sort them
			# and then process them in chron order
			push @avevents, [ $logRec,$ts,$eventType,$reason,$cause,$infoText,$rank,$piutype,$board,$cv,$up, $addInfo, $swprodno, $swprodrev, $hwprodno, $hwprodrev ];
			#print "Found Record:  $logRec : $ts , type=$eventType, reas=$reason, cause=$cause, info=$infoText, rank=$rank, piu=$piutype, smn=$smn, apn=$apn, cv=$cv, up=$up\n";
		}
	}
	
	close $fh;
	
	@avevents = sort { $a->[0] <=> $b->[0] } @avevents;

	if ($opts{dumpfh}) {
		my $fh = $opts{dumpfh};
		foreach my $ev (@avevents) {
			print $fh _fmtTime($ev->[1]),">> ",join(', ',@{$ev}),"\n";
		}
	}
	
	my ($analyseResult, $outrecs) = $self->_analyzeEvents(\@avevents, $callback, $pmdParser, $syslogParser, %opts) if $callback;
	$lastrecord = $minlogrec unless defined $lastrecord;
	my $missingrecs = $minlogrec - $lastrecord;
	
	# If we did not manage to find the lastprocessed record then start time is the earliest record we did find
	$startTime = $firstTime unless defined $startTime;
	
	$srv->logDebug(4, "Result of parsing $file: lastrecno = $lastrecord; minrec = $minlogrec; maxrec = $maxlogrec missingrecs = $missingrecs; output records=$outrecs") if DEBUG4;

	return OK, $maxlogrec, $missingrecs, $startTime, $endTime;
}

##-----------------------------------------------------------------------------
# _analyzeEvents($events, $callback, $pmdParser, %opts)
# Analyses the given events, returning restart events via the callback
# - \@events	AoA containing event data in internal format
# - $callback	Call back sub to recieve results
# - $pmdParser	PMD Parser Instance, to obtain PMD Info for auto restarts
# - $sysLogParser	Syslog Parser instance, to obtain syslog related info
# RETURNS: $rcode, $outrecs
# - $rcode	Result Code
# - $outrecs	Number of records that were output
#------------------------------------------------------------------------------
sub _analyzeEvents {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $avevents, $callback, $pmdParser, $sysLogParser)=@_;
	
	my $nodename=$self->{nodename};
	my $nodetype=$self->{nodetype};
	my $startDate=$self->{startdate};
	my $outrecs;				# Number of records output
	
	#	my $dumprecs;
	#	open $dumprecs, ">avrecs.txt";
	
	# State variables for analysis
	my (
		$recordIndex,		# Index of currently processed record
		
		$nodeRestartState,	# Bitmap describing the state of an ongoing restart. See constants for bit meanings 
		$nodeRestartRank,	# Rank of the restart
		$nodeRestartTime,	# Start time for node restart
		$nodeRestartDown,	# Time at which node reported down
		$nodeRestartCause,	# Cause for ongoing node restart
		$nodeRestartReason,	# Reason for manual restarts
		$nodeRestartInfotext,	# Free text input for manual restarts
		$nodeRestartCv,		# CV loaded at time of restart
		$nodeRestartUp,		# SW Loaded at time of restart
		$nodeRestartCdt,	# Cello Downtime: time from restart start to cpp node operational indication
		$nodeRestartAdt,	# Application Downtime
		
		%piuRestart,		# Hash containing restart state for each piu slot
		%pgmRestart,        # Hash containing restart state for each piu slot
		$tsOff,			# Offset for timings when clock is 1970
		);
	
	# The following state is preserved between runs of this sub via the
	# collection history module
	my $currentCv = $self->{stateData}{currentCv}; # Currently Loaded CV
	my $currentUp = $self->{stateData}{currentUp}; # Currently Loaded upgrade package
	
	# Declared here so sub below has access
	my ($logRec,$ts,$eventType,$reason,$cause,$infoText,$rank,$piutype,$board,$cv,$up, $addInfo, $swprodno, $swprodrev, $hwprodno, $hwprodrev );
	
	# Inner Sub, for sending node restart events.
	# We put this here because it is used in a couple of places in the event
	# processing loop. Declaring it as a closure allows us to reference all the
	# state variables above without having to pass them in.
	
	my $sendNodeRestartEvent = sub {
		
		my $restartType;
		if ($nodeRestartCause eq 'ExtRestartRequest') {
			# Manual restart
			$restartType="manual(node-$nodeRestartRank)";
		} elsif ($nodeRestartCause eq 'poweroff') {
			$restartType="poweroff(node-$nodeRestartRank)";
		} elsif ($nodeRestartCause eq 'UpgradeNormal') {
			$restartType="upgrade(node)";
		} else {
			#print "--> nodeRestartCause = $nodeRestartCause\n";
			$restartType="$nodeRestartCause(node-$nodeRestartRank)";
		}
		
		my $rec = new Itk::EventData::EventRec::Restart(
			time => $nodeRestartTime,
			nodename => $nodename,
			nodetype => $nodetype,
			restarttype => $restartType,
			scope => 'Node',
			appdt => $nodeRestartAdt,
			cellodt => $nodeRestartCdt,
			cv => $nodeRestartCv,
			sw => $nodeRestartUp,
			newcv => $currentCv,
			newsw => $currentUp,
			reason => $nodeRestartReason,
			infotext => $nodeRestartInfotext
			
			);
		
		# Store the outage in downtimes array. This array is used by the
		# calcDownTime method so that we can count the amount of down time
		# an upgrade event has caused (and also the number of restarts). The same array can also be used to 
		# determine the sw level at any point in time, because the new sw level is stored in it
		push @{$self->{downtimes}}, [ $nodeRestartTime, $nodeRestartCdt, $nodeRestartAdt, $currentUp, $currentCv ];
		
		$outrecs++;
		
		$srv->logDebug(5, "OutputRecord: ".$rec->sprint) if DEBUG5;
		&$callback($rec);
		
		# Reset state variables
		undef $nodeRestartState;
		undef $nodeRestartRank;
		undef $nodeRestartTime;
		#undef $nodeRestartDown;
		undef $nodeRestartCause;
		undef $nodeRestartCv;
		undef $nodeRestartUp;
		undef $nodeRestartCdt;
		undef $nodeRestartAdt;
		undef $nodeRestartReason;
		undef $nodeRestartInfotext;
		
	};
	
	$recordIndex=0;
	
	# The following vars are used for processing of queued records from the system log parser
	my $currentSysLogRec = (defined $sysLogParser) ? $sysLogParser->getNextRecord() : undef;
	my $currentSysLogRecTime;
	if ($currentSysLogRec) {
		$currentSysLogRecTime=$currentSysLogRec->get('time');
	}
	
	RECORD:
	foreach my $ev (@{$avevents}) {
		($logRec,$ts,$eventType,$reason,$cause,$infoText,$rank,$piutype,$board,$cv,$up, $addInfo, $swprodno, $swprodrev, $hwprodno, $hwprodrev ) = @{$ev};
		
		$recordIndex++;
		
		if (DEBUG6) {
			$srv->logDebug(6, _fmtTime($ts)." s=$nodeRestartState >> ".join(', ',@{$ev})); # //// DEBUG ////
		}
		
		# Adjust for potentially faulty timestamps. if the timestamp is in year 1970 then probably the
		# system clock has been reset to power off. The algorithm for handling this is as follows:
		# 1. The first time the transition is seen, scan forward until the clock is reset to correct time
		# 2. Calculate the difference between the last 1970 timestamp and the first correct timestamp
		# 3. Apply that difference to all of the 1970 timestamps
		
		if ($ts < 79833599) { # = anytime in 1970
			if (defined $tsOff) {
				$ts+=$tsOff;
			} else {
				my $i=$recordIndex;
				while (++$i<@{$avevents}) {
					my $ts1 = $avevents->[$i][1];
					if ($ts1>79833599) {
						# Found transition back to correct time
						$tsOff = $ts1 - $avevents->[$i-1][1];
						$ts += $tsOff;
						last;
					}
				}
			}
		} else {
			undef $tsOff; # If date is OK then remove any offset
		}
		
		# Skip this record if before the start date filter
		if (defined $startDate and $startDate > $ts) {
			$srv->logDebug(6, "Discard record id=$logRec ts=$ts because it is earlier than startdate") if DEBUG6;
			next RECORD;
		}
		
		# Check if we can process the next record in the syslog queue.
		# these records just need to have their sw and up filled out, then to be output
		# There might be multiple syslog records that match
		while ($currentSysLogRec && $ts>$currentSysLogRecTime) {
			# We have just gone past the time for the current syslog record
			# Therefore the current cv & up must be the ones for this record
			$currentSysLogRec->set(
				cv => $currentCv,
				sw => $currentUp
				);
			&$callback($currentSysLogRec);
			$currentSysLogRec = $sysLogParser->getNextRecord();
			if ($currentSysLogRec) {
				$currentSysLogRecTime=$currentSysLogRec->get('time');
			} else {
				undef $currentSysLogRecTime;
			}
		}
		
		if ($eventType == NODEOUT) {
			if ($reason eq 'ShutdownCommand') {
				if ($nodeRestartState) {
					# Uh oh. a new node restart and we have not even finished the first one
					# Emit a record for the first restart, but with no downtime. The record for the
					# last restart (the one that actually succeeds) will have all the downtime
					&$sendNodeRestartEvent();
					$nodeRestartState |= NR_RESTARTED; # Record the fact that we are in another (internal) restart
					#$self->_error("Node Shutdown command received while restart ongoing");
					#next RECORD;
				} else {
					$nodeRestartState=NR_STARTED;
				}
				$nodeRestartRank=$rank;
				if ($cause =~ /Manual(\w+)/) {
					$nodeRestartCause='manual';
					$nodeRestartReason=$1;
					$nodeRestartInfotext=$infoText;
				} else {
					$nodeRestartCause=$cause;
				}
				$nodeRestartTime=$ts;
				$nodeRestartCv=$currentCv;
				$nodeRestartUp=$currentUp;
				next RECORD;
			}
			
			if ($reason eq 'UnOperational') {
				unless ($nodeRestartState) {
					# Must have been a power-off restart
					$nodeRestartState=NR_STARTED;
					$nodeRestartRank="cold";
					$nodeRestartCause="poweroff";
					$nodeRestartCv=$currentCv;
					$nodeRestartUp=$currentUp;
					# NB. In this case we do not really know when the node was powered off, can only record the
					# time taken to start up
					$nodeRestartDown=$ts;
					$nodeRestartTime=$ts;
					next RECORD;
				}
				
				# Only record the node down time for the first restart
				# in cases where one restart occurs inside another
				$nodeRestartDown=$ts unless defined $nodeRestartDown;
				$nodeRestartTime=$ts;
				next RECORD;
			}
		}
		
		if ($eventType == PIUOUT) {
			
			# Each board can have a separate restart state
			# This is stored in hash %piuRestart
			# $p is a pointer to the record for the board involved in this event
			
			my $p=$piuRestart{$board};
			$p=$piuRestart{$board}={} unless defined $p;
			
			if ($reason eq 'ShutdownCommand') {
				if ($p->{state}) {
					$self->_warn("(rec $logRec) New PIU Restart ordered with cause $cause when restart is ongoing on $board");
					# This might be an escalation from a software error to a full restart of some kind
					# In this scenario we keep the cause from this second shutdown
					# Otherwise the restart may be missed, because we will never get the PMD (and therefore
					# will never get an auto restart event)
					$p->{cause}=$cause;
				}
				$p->{state}=10;
				if ($cause =~ /Manual(\w+)/) {
					$p->{type}='manual(PIU)';
					$p->{reason}=$1;
					$p->{infotext}=$infoText;
				} elsif ($cause =~ /upgrade/i) {
					$p->{type}='upgrade(PIU)';
				} elsif ($cause eq 'ExtOperatorRequest') {
					$p->{type}='manual(PIU)';
				} elsif ($cause eq 'PiuNoContact' or $cause eq 'PiuFault' or $cause eq 'PiuFtcSwitch' or $cause eq 'ExtRestartRequest' or $cause eq 'RestartedByPri') {
					$p->{type}='auto(PIU)';
					$p->{reason}=$cause;
					if ($cause eq 'RestartedByPri') {
						$p->{infotext}=$infoText;
					}
				} else {
					$p->{type}='unknown(PIU)';
					$p->{reason}=$cause;
				}
				$p->{rank}=$rank;
				$p->{scope}=$piutype;
				$p->{hwprodno}=$hwprodno if $hwprodno;
				$p->{hwprodrev}=$hwprodrev if $hwprodrev;
				# Time should be measured from UnOperational, however since we do not
				# always get this we set the time here as well. It will be overwritten if
				# there is a CelloOperational
				$p->{time}=$ts;
				next RECORD;
			}
			
			if ($reason eq 'UnOperational') {
				# Can sometimes get multiple UnOperational records coming through
				# We assume the first one we see is the one we need to record
				if ($p->{state}==20) {
					next RECORD;
				}
				
				$p->{state}=20;
				$p->{time}=$ts;
				#$p->{piutype}=$piutype;
				$p->{rank}=$rank;
				$p->{cause}=$cause;
				unless ($p->{type}) {
					if ($cause eq 'PiuNoContact' or $cause eq 'PiuFault' or $cause =~ /^Error(Hw)?$/) {
						$p->{type}='auto(PIU)';
					} elsif ($cause eq 'ExtRestartRequest') {
						$p->{type}='manual(PIU)';
					} else {
						$p->{type}='unknown(PIU)';
					}
				}
				$p->{reason}=$cause;
				$p->{scope}=$piutype if $piutype;
				$p->{hwprodno}=$hwprodno if $hwprodno;
				$p->{hwprodrev}=$hwprodrev if $hwprodrev;
				next RECORD;
			}
			
		}
		
		if ($eventType == PGMOUT) {
			
			my $key = "${board}_${swprodno}_${swprodrev}";
			
			# Each board can have a separate restart state per program
			# This is stored in hash %pgmRestart
			
			
			if (exists $pgmRestart{$key}) {
				$srv->logWarn("Program Out event while program restart already ongoing!");
			}
			
			my $rtype;
			if ($cause =~ /Manual|RestartRequest/) {
				$rtype = 'manual';
			} else {
				$rtype = 'auto';
			}
			
			$pgmRestart{$key} = {
				time => $ts,
				#state=>20,
				scope => $piutype,
				cause => $cause,
				type => "$rtype(PIU)",
				reason => $cause,
				infotext => $infoText,
				swprodno => $swprodno,
				swprodrev => $swprodrev
			};
			next RECORD;
			
		}
		
		if ($eventType == OTHER) {
			if ($reason eq 'NodeRestarted') {
				unless ($nodeRestartState) {
					# Must have been a power-off restart
					$nodeRestartState=NR_STARTED | NR_NODERESTARTED;
					$nodeRestartRank="cold";
					$nodeRestartCause="poweroff";
					$nodeRestartCv=$cv;
					$nodeRestartUp=$up;
					# NB. In this case we do not really know when the node was powered off, can only record the
					# time taken to start up
					$nodeRestartDown=$ts;
					$nodeRestartTime=$ts;
					next RECORD;
				}
				$currentCv = $cv;
				$currentUp = $up;
				$nodeRestartState |= NR_NODERESTARTED;
				if ( ($nodeRestartState & NR_COMPLETE) == NR_COMPLETE) {
					# Now have all info needed for restart event
					&$sendNodeRestartEvent();
					undef $nodeRestartDown;
					next RECORD;
				}
				next RECORD;
			}
			if ($reason eq 'PeriodicLogging') {
				$currentCv = $cv;
				
				# Store the fact that sw level has changed (or maybe this is the first one we have seen in this file)
				if (! defined $currentUp or $currentUp ne $up) {
					push @{$self->{downtimes}}, [ $nodeRestartTime, undef, undef, $up, $cv ];
				}
				
				$currentUp = $up;
				next RECORD;
			}
		}
		
		if ($eventType == PGMIN) {
			
			my $key = "${board}_${swprodno}_${swprodrev}";
			
			if (! exists $pgmRestart{$key}) {
				$srv->logWarn("Program In event without Program Restart ongoing!");
			} else  {
				
		        my $p=delete $pgmRestart{$key}; # Return record and delete it from the state hash
				
				# IMPORTANT: Do not report auto restarts. These are already
				# detected in the sys log parser so if we output a record here
				# it would be a duplicate
				
				if ($p->{reason} ne 'ProgramFailure') {
					
					my $rec = new Itk::EventData::EventRec::Restart(
						time => $p->{time},
						nodename => $nodename,
						nodetype => $nodetype,
						restarttype => $p->{type},
						scope => 'Program',
						slot => $board,
						cv => $currentCv,
						sw => $currentUp,
						reason => $p->{reason},
						infotext => $p->{infotext},
						scope => $p->{scope},
						block => "$p->{swprodno}_$p->{swprodrev}",
					);
					
					$srv->logDebug(5, "OutputRecord: ".$rec->sprint) if DEBUG5;
					&$callback($rec);
					
				}
				next RECORD;
	        }  # else
		} # if
		
		
		if ($eventType == NODEIN) {
			
			if ($reason eq 'CelloOperational') {
				
				unless ($nodeRestartState) {
					$self->_error("(rec $logRec) CelloOperational without restart ongoing");
					next RECORD;
				}
				
				if (defined $nodeRestartDown && $nodeRestartDown < $ts) {
					$nodeRestartCdt = $ts - $nodeRestartDown;
				} else {
					$nodeRestartCdt = undef;
				}
				
				$nodeRestartState |= NR_CELLOOPER;
				
				if (($nodeRestartState & NR_COMPLETE) == NR_COMPLETE) {
					# Now we should have enough information to emit an event
					&$sendNodeRestartEvent();
					undef $nodeRestartDown;
					next RECORD;
				}
				
				next RECORD;
			}
			
			if ( ($reason eq 'Operational' and $addInfo =~ /RNC Node Restart Completed|RestartCompleted/ ) # RNC case| LTE case 
				or $addInfo =~ /Cell .+ enabled/) { # RBS Case
			
			unless ($nodeRestartState) {
				next RECORD if $addInfo =~ /Cell/; # remove noise from error reporting
				$self->_error("(rec $logRec) Node Restart Completed without restart ongoing");
				next RECORD;
			}
			
			if (defined $nodeRestartDown && $nodeRestartDown < $ts) {
				$nodeRestartAdt = $ts - $nodeRestartDown;
			} else {
				$nodeRestartAdt=undef;
			}
			
			$nodeRestartState |= NR_APPOPER;
			
			if (($nodeRestartState & NR_COMPLETE) == NR_COMPLETE) {
				# Now we should have enough information to emit an event
				&$sendNodeRestartEvent();
				undef $nodeRestartDown;
				next RECORD;
			}
			
			next RECORD;
			
				}
		}
		
		if ($eventType==PIUIN) {
			
			my $p=$piuRestart{$board};
			$p=$piuRestart{$board}={} unless defined $p;
			if ($reason eq 'CelloOperational') {
				
				#print STDERR ">> celloOp: cause=$p->{cause}\n";
				
				unless ($p->{state}) {
					$self->_error("(rec $logRec) PIUIN & Operational without restart ongoing: $board");
					next RECORD;
				}
				
				# Restarts triggered by sw are reported via the PMD & Syslog
				# so we do not try to report them here
				if ($p->{cause} ne 'Error') {
					
					my $rec = new Itk::EventData::EventRec::Restart(
						time => $p->{time},
						nodename => $nodename,
						nodetype => $nodetype,
						restarttype => $p->{type},
						scope => 'PIU',
						slot => $board,
						cv => $currentCv,
						sw => $currentUp,
						reason => $p->{reason},
						infotext => $p->{infotext},
						scope => $p->{scope},
						productnumber => $p->{hwprodno},
						productrevision => $p->{hwprodrev},
						
						);
					
					$srv->logDebug(5, "OutputRecord: ".$rec->sprint) if DEBUG5;
					&$callback($rec); 
					
				}
				
				delete $piuRestart{$board};
			}
		}
		
		# Fallback Checking. If it looks like we have not seen the end of
		# a restart after time T_MAXNODERESTART secs, then we assume it has
		# finished. This is needed because on occasion some events are missing
		# in the log.
		if ($nodeRestartState && ($ts - $nodeRestartDown > T_MAXNODERESTART)) {
			my $tdiff = $ts - $nodeRestartDown;
			$self->_error("(rec $logRec) Node restart exceeded T_MAXNODERESTART: $tdiff secs");
			&$sendNodeRestartEvent();
			undef $nodeRestartDown;
		}
		
	}
	
	# Save state for next run
	$self->{stateData} = {		
		currentCv => $currentCv,
		currentUp => $currentUp
	};
	
	#	close $dumprecs;
	return OK,$outrecs;
}

##-----------------------------------------------------------------------------
# parseFiles($fileList, $callback, $pmdParser)
# Parses one or more avlog files, returning event records to the callback sub as
# they are found. The last processed record is stored separately for each file
# - $fileList	Ref to array containing local filenames of syslog files
# - $callback 	Ref to callback sub
# - $pmdParser	Ref to instance of PMD Parser from which to fetch restart PMD Info
# - $sysLogParser	Syslog Parser instance, to obtain syslog related info
# RETURNS: $resultcode
# - $resultCode	  Result Code - see constants for definitions
# - $startTime    Starting time for the time window covered by this run (epoch secs)
# - $endTime      Ending time for the time window covered by this run (epoch secs)
#------------------------------------------------------------------------------
sub parseFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fileList, $callback, $pmdParser,$sysLogParser)=@_;

	my (
		$startTime,	# Start of time window. Each run should provide a window of time for which we believe we have captured all events
		$endTime,	# End of time window
		$rcode,		# Result code
	);
	
	# last_record is a hash of file => last record#
	# In case the software is rolled back and then forward we might see cases where more than one of the
	# possible files has changes
	
	my $lastRecTable=$self->{collHist}->get('last_record');
	
	# state_data is a hash of data containing the state after the last file was finished
	$self->{stateData} = $self->{collHist}->get('state_data');
	$self->{stateData} ||= {};
		
	foreach my $file (@{$fileList}) {
		
		my ($baseFileName) = ($file =~ /([^\/]+)$/);
		
		my $lastrecord = $lastRecTable->{$baseFileName};

		my ($parseRcode, $lastRecInFile, $missedRec, $fileStartTime, $fileEndTime) = $self->parse($file, 
			$callback, 
			$pmdParser,
			$sysLogParser,
			$lastrecord
		);
		
		if (defined $lastRecInFile) {
			$lastRecTable->{$baseFileName} = $lastRecInFile;
		}

		$startTime = $fileStartTime unless (defined $startTime and $startTime < $fileStartTime);
		$endTime = $fileEndTime unless (defined $endTime and $endTime > $fileEndTime);
		
		if ($parseRcode != OK) {
			$self->_warn("Failed parsing file $file, rcode=$rcode");
			$rcode=PARSE_FAILED;
			next;
		}
		
		$rcode = OK_MISSEDDATA if (! defined $rcode && $missedRec);
		
	}
	
	$self->{collHist}->put('last_record', $lastRecTable);
	
	$srv->logDebug(6, "Parser State to save: ".Dumper($self->{stateData})) if DEBUG6;
	$self->{collHist}->put('state_data', $self->{stateData});

	# Sort the downtimes for later searching
	@{$self->{downtimes}} = sort { $a->[0] <=> $b->[0] } @{$self->{downtimes}};
	
	$rcode = OK unless defined $rcode;

	$srv->logDebug(4, "Result of file parsing: rcode=$rcode, starttime=$startTime, endtime=$endTime") if DEBUG4;
	
	return $rcode, $startTime, $endTime;
}
##-----------------------------------------------------------------------------
# calcDownTime($startTime, $endTime)
# Calculate the downtime and number of node restarts occurring between the given
# times. As a side effect, also returns the sw level at the startTime
# - $startTime	Starting time for calculation
# - $endTime	Ending time for calculation
# RETURNS: $numRest, $celloDt, $appDt, $up
# - $numRest	Number of Restarts
# - $celloDt	Cello Downtime
# - $appDt	Application Downtime
# - $up		Package running at the startTime
#------------------------------------------------------------------------------
sub calcDownTime {
	$srv->logEnter(\@_) if ENTER;
	my ($self,$startTime, $endTime)=@_;
	my $i = $self->_bsearch($startTime);
	my $dtrecs = $self->{downtimes};
	
	my $numrecs = $#{$dtrecs};
	my ($celloDt, $appDt, $numRest, $up);
	if (defined $i && $i >=0) {
		$up = $dtrecs->[$i][3];

		# cumulate the celloDt, appDt and numRest of FOLLOWING entries until endtime
		$i++;
		while ($i <= $numrecs) {
			if ($dtrecs->[$i][0] > $endTime) {
				last;
			}

			if (defined $dtrecs->[$i][1]) {
				$celloDt += $dtrecs->[$i][1];
				$appDt += $dtrecs->[$i][2];
				$numRest++ ;
			}
			$i++;
		}
	}
	return $numRest, $celloDt, $appDt, $up;
}
##-----------------------------------------------------------------------------
# getRunningUp($startTime)
# Returns the sw level at the startTime
# - $startTime	Starting time for calculation
# RETURNS: $up or ($up, $cv) in list context
# - $up		Package running at the startTime
# - $cv		CV loaded at the startTime
#------------------------------------------------------------------------------
sub getRunningUp {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $startTime) = @_;
	my $i = $self->_bsearch($startTime);
	if (wantarray) {
		if ($i >=0 ) {
			return @{$self->{downtimes}[$i]}[3,4];
		} else {
			return ();
		}
	} else {
		if ($i >=0 ) {
			return $self->{downtimes}[$i][3] ;
		} else {
			return undef;
		}
	}
}

##-----------------------------------------------------------------------------
# getStateData
# Returns the sw level.
# RETURNS: $up or ($up, $cv) in list context
# - $up		Package running at the startTime
# - $cv		CV loaded at the startTime
#------------------------------------------------------------------------------

sub getStateData () {
	my ($self) = @_;
	$srv->logEnter(\@_) if ENTER;
	return $self->{collHist}->get('state_data');
}


##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	my $self=shift;
	# Do destruction!!
}
#------------------------------------------------------------------------------
1; # Modules always return 1!

