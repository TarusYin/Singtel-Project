#!/usr/bin/perl
###----------------------------------------------------------------------------
# Parser/Analyser for syslog files
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
#
# Author: EHARMIC
# $Id: SyslogParser.pm 7663 2012-10-04 11:06:33Z lmcmarl $

package Itk::EventData::SyslogParser;
use strict;
use Itk::EventData::EventRec;
use Digest::MD5 qw(md5_base64);
use Time::Local qw(timegm_nocheck);

BEGIN {
	if (DEBUG6) {
		require Data::Dumper;
		import Data::Dumper;
	}
}
#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	# Result Codes
	OK => 1,			# Operation succeeded
	OK_MISSEDDATA => 2,		# Operation succeeded but there was some missing data
	PARSE_FAILED => -1,		# Parsing failed
	MIN_PMD_MATCH_TIME => 30,	# Minimum time window for finding a PMD (secs)
	MIN_EPOCH => 79833599,		# Min epoch time (corresponds to the end of 1970) - used to compensate for corrupted dates in file
};

use constant MONTHS => { 
			'Jan' => 0,'Feb' => 1,'Mar' => 2,'Apr' => 3,'May' => 4,
			'Jun' => 5,'Jul' => 6, 'Aug' => 7, 'Sep' => 8, 'Oct' => 9, 
			'Nov' => 10, 'Dec' => 11 
		};
		
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'collHist',		# Ref to collection history object
	'nodename',
	'nodetype',
	'pmdflag',		# Flag used to detect duplicated pmd info
	'recordqueue',		# Queue of records waiting for output
	'startdate',		# Timestamp (unix epoch) of start time filter
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;
*srv=\$main::srv;
#==============================================================================
# CLASS METHODS
#==============================================================================

#==============================================================================
# PRIVATE METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# _warn($msg)
# Issue warning, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _warn {
	$srv->logWarn("($_[0]->{nodename}) $_[1]");
}

##-----------------------------------------------------------------------------
# _error($msg)
# Issue Error, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _error {
	$srv->logError("($_[0]->{nodename}) $_[1]");
}

##-----------------------------------------------------------------------------
# _ptime($t)
# Parse date/time stamps from syslog format into epoch seconds
# - $t Time format as supplied in syslog, eg: Nov  4 19:54:55 2007
# RETURNS: $ts,$strtime
# - $ts Time in epoch seconds
# - $strtime Time in format yyyy-mm-dd hh:ss:ss
#------------------------------------------------------------------------------
sub _ptime {
	my ($t)=@_;
	my ($month, $md, $hh,$mm,$ss,$yy)=($t=~ /^(\w{3})\s+(\d+)\s+(\d+):(\d+):(\d+)\s+(\d+)$/) or return undef;
	return undef unless exists MONTHS->{$month};
	my $mo = MONTHS -> {$month};
	my $strtime = sprintf("%04d-%02d-%02d %02d:%02d:%02d",$yy,$mo+1,$md,$hh,$mm,$ss);
	my $ts;
	eval {
		$ts = timegm_nocheck($ss,$mm,$hh,$md,$mo,$yy);
	};
	if ($@) {
		$srv->logWarn("Time out of range: $strtime");
	}
	return $ts,$strtime;
}
#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new($collHist, %opts)
# Constructor
# - $collHist	Ref to instance of collection history
# % nodename	Name of node
# % nodetype	Type of node
# % startdate 	Starting date as a unix epoch timestamp; events earlier than this will be discarded
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $collHist, %opts)=@_;
	$self=fields::new($self) unless ref $self;
	$self->{collHist}=$collHist;
	$self->{nodename}=$opts{nodename};
	$self->{nodetype}=$opts{nodetype};
	$self->{startdate}=$opts{startdate};
	$self->{pmdflag}={};
	$self->{recordqueue}=[];
	return $self;
}

##-----------------------------------------------------------------------------
# parse($file, $callback, $pmdParser, $lastrecord)
# Parses the indicated syslog file
# - $file	Filename of syslog file to parse
# - $callback	Call back sub to recieve results
# - $pmdParser	PMD Parser Instance, to obtain PMD Info for auto restarts
# - $lastrecord	Record ID of the last processed record. If this is not undef,
#		then we skip forward through the file until we find a matching
#		record; otherwise we just process all records in the file
# RETURNS: $resultcode, $lastrecord, $missingRecs, $startTime, $endTime
# - $resultCode	  Result Code - see constants for definitions
# - $lastrecord   Record ID of the last processed record
# - $missingRecs  Flag: if true, we could not find lastrecord so some records have been missed.
# - $startTime    Starting time for the time window covered by this parse (epoch secs)
# - $endTime      Ending time for the time window covered by this parse (epoch secs)
#------------------------------------------------------------------------------
sub parse {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $file, $callback, $pmdParser, $lastrecord)=@_;
	
	my $nodename=$self->{nodename};
	my $nodetype=$self->{nodetype};
	my $startDate=$self->{startdate};
	
	my $fh;
	open $fh, $file or do {
		$self->_error("Failed to open '$file': $!");
		return PARSE_FAILED;
	};
	
	# Persistent state variables
	my (
		$numrecs,	# No of records output 
		
	);
	
	my $process_record = sub {
		my ($recno,$ts, $sev, $record)=@_;
		
		my $outRec;
		
		$_=$record->[0];
		if (/^Creating new crash information envelope/) {
			my ($board, $pmdid);
		
			$board = "${1}${2}${3}" if $record->[1]=~/^\s*initiated by:\s+smn=(\d{2}), apn=(\d{2}), (?:device|suua)=(\d{2})/;
			
			my $piutype;
			if ($3 ne '00') {
				$piutype = 'SPM';
			}
			
			my ($yy,$mo,$dd,$hh,$mm,$ss, $ts1, $rday);
			if (($pmdid,$rday,$hh,$mm,$ss) = ($record->[3]=~/^\s*generated:\s*pmdId=(0x[0-9a-fA-F]+).pmd,\s*date=(\d{2}[-\/]\d{2}[-\/]\d{2}), time=(\d{2}):(\d{2}):(\d{2})/)) {
				
				# dates in above string seem to appear in 2 formats:
				# yy-mm-dd and mm/dd/yy
				($yy,$mo,$dd) = ($rday =~ /(\d{2})-(\d{2})-(\d{2})/) or ($mo,$dd,$yy) = ($rday =~ /(\d{2})\/(\d{2})\/(\d{2})/);

				# Invalid PMD date - occurs if node clock was reset at restart
				# In that case we substitute the date/time of when the pmd was written
				if ($mo==0) {
					$ts1=$ts;
				} else {
					# WARNING: input data is not y21k safe
					# Timestamps in 1970 (0 epoch time) are the earliest we can see
					$yy += ($yy < 70) ? 2000 : 1900;
					
					eval {
						$ts1=timegm_nocheck($ss,$mm,$hh,$dd,$mo-1,$yy);
					};
					if ($@) {
						$self->_error("($file:$.) Invalid Timestamp in $record->[3] - record is discarded");
						return;
					}

				}
			} else {
				$self->_error("Parse failure processing record # $record->[3] ($file:$.)");
				return;
			}
			
			# Due to a bug, sometimes the records are duplicated in syslog
			# Check if we have seen this one before
			my $eventKey = "${board}_${pmdid}_${ts1}";
			if (exists $self->{pmdflag}{$eventKey}) {
				$srv->logDebug(4,"Duplicated event record found for $eventKey in syslog record $recno, discarding\n") if DEBUG1;
				return;
			} else {
				$self->{pmdflag}{$eventKey}=1;
			}
			
			# Other types of restart are created from avlog
			# Only program restarts are reported directly from syslog
			# Note: because we do not know the sw version for each one,
			# instead of outputting the restart we save the details for later
			
			$record->[2]=~/restartType=(\w+)/;
			my $capture = $1;
			$srv->logDebug(1,"RestartType Captured : $capture ") if DEBUG1;
			my $scope = ($capture eq 'PROGRAM') ? 'Program' : 
				    ($capture eq 'PROCESSOR') ? 'Processor' :
				    'Unknown';
				    
			$srv->logDebug(1,"Determined scope : $scope") if DEBUG1;
				    
			my $rec = new Itk::EventData::EventRec::Restart(
				time => $ts1,
				nodename => $nodename,
				nodetype => $nodetype,
				restarttype => "auto(PIU)",
				scope => $scope,
				pmdid => $pmdid,
				slot => $board,
				piutype=>$piutype,
			);
			
			if ($pmdid eq '0x00000000' || ! $pmdid) {
				# PMD ID of 0x00000000.pmd means there probably is no pmd
				my ($ecode) = ($record->[2]=~/ecode=([^,]+),/);
				$rec->set('errorno', $ecode);
			} else {
				unless ($pmdParser->addDetailsToRecord($rec)) {
					$rec->set('pmdid', "PMD_MISSING_$pmdid");
					$rec->set('errorno', 'PMD_MISSING_UNKNOWN');
					$rec->set('extra', 'PMD_MISSING_UNKNOWN');
					$rec->set('process', 'PMD_MISSING_UNKNOWN');
					#$self->_warn("Could not find PMD for pmdid=$pmdid, board=$board, ts=$ts1 (".fmtTime($ts1).")");
					$self->_warn("Could not find PMD for pmdid=$pmdid, board=$board, ts=$ts1");
				}
			}
			
			$srv->logDebug(5, "OutputRecord: ".$rec->sprint) if DEBUG5;
			
			push @{$self->{recordqueue}}, $rec;
			$numrecs++;
			
		}
	};
	
	my (
		$stime,		# Time of current record, string format
		$ts,		# Time of current record, epoch secs
		$recno,		# Current record No.
		$recid,		# Current record ID (md5 of header)
		$sev,		# Severity field
		$skipping,	# Flag: are we skipping forward to find the last processed record?
		$startTime,	# Start time for this parse (either time of last processed, or first record found if none)
		$endTime	# Time of last record found in the file
	);
	
	my $missingrecs=0;	# Flag: any missing records?
	
	$skipping=1 if defined $lastrecord;
	
RECORD:	
	while (<$fh>) {
		chomp;
		# Start of a record
		if (($recno,$sev,$stime) = (/^recid=(\d+),format=STRING,event_type=0,facility=SYSLOG,uid=\d+,gid=\d+,pid=\d+,pgrp=\d+,severity=(\w+),time=(.+)$/)) {
			
			
			# The record ID resets to 0 every time there is a restart. So instead of using the plain recordid
			# we make an md5 hash of the record header so we can check it again next time
			$recid = md5_base64($_);
			#$recid="$recid:$ts";
			
			# Convert ts to epoch time. As a side effect we get an ISO8601 string as well in $stime
			($ts,$stime) = _ptime($stime);
			if (! defined $ts) {
				$self->_error("($file:$.) Invalid Timestamp $stime - record is discarded");
				next RECORD;
			}
			
			$srv->logDebug(6,"Processing record $recid \@ $stime (skipping=$skipping)") if DEBUG6;
			
			# Skip this record if before the start date filter
			if (defined $startDate and $startDate > $ts) {
				$srv->logDebug(6, "Discard record id=$recid ts=$ts because it is earlier than startdate") if DEBUG6;
				next RECORD;
			}
			
			# If we are skipping and the record number of this record is != the last 
			# processed record then we skip this record
			if ($skipping) {
				if ($recid ne $lastrecord) {
					next RECORD;
				} else {
					# If this record is the same as the lastprocessed then
					# we have found the point where we left off last time
					$srv->logDebug(5,"Found last processed record with id=$recid") if DEBUG5;
					$skipping=0;
					$startTime=$ts if $ts>MIN_EPOCH;
					next RECORD;
				}
			} else {
				$startTime=$ts if ($ts > MIN_EPOCH and (! defined $startTime or $ts < $startTime));
			}
			
			my @record;
			while (<$fh>) {
				my $save=$_;
				chomp;
				s/\r//; # remove any carriage returns
				if (/^recid=/ || eof($fh)) {
					# Now we have a whole record.
					$srv->logDebug(6, "Record: recno=$recno, time=$ts ($stime), sev=$sev\n\t".join("\n\t",@record)) if DEBUG6;
					&$process_record($recno,$ts, $sev, \@record);
					$_=$save;
					redo RECORD unless eof($fh);
				} else {
					push @record, $_;
				}
			}
		}
	}
	
	# Got to the end of the file. If $skipping is true that means we did not
	# find the last processed record; so we must have missed some data.
	if ($skipping) {
		$self->_warn("Could not find current record. This means we have missed some data.");
		$skipping=0;
		$missingrecs=1;
		seek($fh, 0,0); # Rewind file back to beginning
		goto RECORD; # start again, this time reading all records
	}
	
	$endTime=$ts;
	
	close $fh;

	$srv->logDebug(4, "Result of parsing $file: lastrec=$recid, missingdata=$missingrecs, starttime=$startTime, endtime=$endTime") if DEBUG4;
	return OK,$recid, $missingrecs, $startTime, $endTime;
}

##-----------------------------------------------------------------------------
# parseFiles($fileList, $callback, $pmdParser)
# Parses a batch of syslog files, returning event records to the callback sub as
# they are found. When auto restarts are found, the PMD Information is added in to the record.
# - $fileList	Ref to array containing local filenames of syslog files
# - $callback 	Ref to callback sub
# - $pmdParser	Ref to instance of PMD Parser from which to fetch restart PMD Info
# RETURNS: $resultcode, $startTime, $endTime
# - $resultCode	  Result Code - see constants for definitions
# - $startTime    Starting time for the time window covered by this run (epoch secs)
# - $endTime      Ending time for the time window covered by this run (epoch secs)
#------------------------------------------------------------------------------
sub parseFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $fileList, $callback, $pmdParser)=@_;

	my (
		$startTime,	# Start of time window. Each run should provide a window of time for which we believe we have captured all events
		$endTime,	# End of time window
		$rcode,		# Result code
		@sortedfilelist,# Files to process in cron order
		%filenames,	# Corresponding file names
		$newLastRecord, # Tracks the max record number processed through all files
	);
	
	# Persistent history. Last file is the last file we read from on this node
	# lastrecord is the last record in that file. When we start up we expect to find the last read
	# record in this file, otherwise we have missed data
	
	my $lastrecord=$self->{collHist}->get('last_record');
	my $lastfilenum = $self->{collHist}->get('last_filenum');

	# We need to process the files in order of their syslog file number (the number that precedes 'syslog')
	# regardless of what directory it was found in
	foreach my $file (@{$fileList}) {
		my ($fnum)=($file=~/(\d+)syslog$/) or do {
			$self->_error("File $file does not match expected name for syslog files. Discarding.");
			next;
		};
		if (defined $lastfilenum && $fnum < $lastfilenum) {
			$self->_error("File $file was modified but we are already on file number $lastfilenum. Discarding.");
			next;
		}
		push @sortedfilelist, $fnum;
		$filenames{$fnum}=$file;
	}
		
	# Now we have a prepared list in the correct order
	foreach my $fnum (@sortedfilelist) {
		
		my $file=$filenames{$fnum};
		
		$srv->logDebug(4, "Processing file: $file") if DEBUG4;
		
		my ($parseRcode, $lastRecInFile, $missedRec, $fileStartTime, $fileEndTime) = $self->parse($file, 
			$callback, 
			$pmdParser,
			# If this file is the one we last parsed, then we pass the 
			# last record number to the parser
			($fnum == $lastfilenum) ? $lastrecord : undef 
		);
		
		if (defined $lastRecInFile) {
			$newLastRecord=$lastRecInFile;
		}
		
		$startTime = $fileStartTime unless (defined $startTime and $startTime < $fileStartTime);
		$endTime = $fileEndTime unless (defined $endTime and $endTime > $fileEndTime);
		
		if ($parseRcode != OK) {
			$self->_warn("Failed parsing file $file, rcode=$rcode");
			$rcode = PARSE_FAILED;
			next;
		}
		
		$rcode = OK_MISSEDDATA if (! defined $rcode and $missedRec);
		
	}
	
	# At the end of this, $lastRec contains the id of last record read
	# and the last element of @sortedfilelist contains the file number of the
	# last file read from
	# Only save these values if we did process some files!
	if (@sortedfilelist && defined $newLastRecord) {
		$self->{collHist}->put('last_record', $newLastRecord);
		$self->{collHist}->put('last_filenum', pop @sortedfilelist);
	}
	
	# Sort the queued records by time
	@{$self->{recordqueue}} = sort { $a->{time} <=> $b->{time} } @{$self->{recordqueue}};
	
	$rcode = OK unless defined $rcode;
	
	return $rcode, $startTime, $endTime;
}
##-----------------------------------------------------------------------------
# getNextRecord()
# Gets the next record which is in the queue. The idea is that the avlog parser
# calls this in order to find all the records that are waiting to have their
# sw and up fields filled in and be sent. They are returned in time sorted order.
# RETURNS: $rec
# - $rec 	Next record in the queu
#------------------------------------------------------------------------------
sub getNextRecord {
	my ($self)=@_;
	return shift @{$self->{recordqueue}};
}

##-----------------------------------------------------------------------------
# flushEvents($callback, $avlog)
# Normally restart events are taken from the queue when the corresponding
# AV Log info is found. However, if there has not been anything written to
# the AV Log in between runs this does not happen. In that case there might
# be records remaining in the record queue - these have to be flushed.
# - $callback		Event delivery callback
# - $avlog			Availability Log Object. Used only to access the AVLog state
#					from the last run.
# RETURNS: void
#------------------------------------------------------------------------------
sub flushEvents {
	my ($self, $callback, $avlog) = @_;
	
	return unless @{$self->{recordqueue}};
	
	# Find last known CV & UP from the avlog state data
	my $avlogState = $avlog->getStateData ();
	my $cv = $avlogState->{currentCv} || 'Unknown';
	my $up = $avlogState->{currentUp} || 'Unknown';
	
	foreach my $rec (@{$self->{recordqueue}}) {
		$rec->set(
			cv => $cv,
			sw => $up,
		);
		&$callback($rec);
	}
	$self->{recordqueue} = [];
}

##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	my $self=shift;
	# Do destruction!!
}
#------------------------------------------------------------------------------
1; # Modules always return 1!

