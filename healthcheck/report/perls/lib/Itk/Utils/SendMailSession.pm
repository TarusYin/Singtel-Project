#!/usr/bin/perl 

package Itk::Utils::SendMailSession;
#use strict;
use warnings;
use MIME::Lite;
use Exporter;
use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
@ISA = qw(Exporter);  
@EXPORT = qw(sendCUDBEmail);  

use constant FROM_MAIL_LIST => 'cudb@web-service.sg.ao.ericsson.se';
my $mail_list  = {
	'eqsvimp' => 'eric.z.yin@ericsson.com',
	'esahwar' => 'sahana.warad@ericsson.com',
	'ealina' => 'alina.zhao@ericsson.com',

	};

##-----------------------------------------------------------------------------
# sendLog($logPath)
# Send the log of automation reload.
# -
#------------------------------------------------------------------------------
sub sendCUDBEmail{
	my ($subject,$data,$userid,$logPath,$scriptPath) = @_;
	my $msg = MIME::Lite->new(
		From     => FROM_MAIL_LIST,
		To       => $mail_list->{$userid},
		Cc       => 'eric.z.yin@ericsson.com',
		Subject  => "CUDB Automation Babysitting",
		Data     => "[".localtime()."]".'<br>'.$data.'<br/>'."Please check the attachment file in terms of the PrintLog",
		Type     => 'text/html',
	);
#open FH, "./Detect_miss.pm";
#my @arr = <FH>;
#close FH;
#$msg->attach(
#	Type     => 'TEXT',
#	Data     => \@arr,
#	Filename => 'Detect_miss.pm',
#	Disposition => 'attachment',
#);
	$msg->attach(
	        Type => 'TEXT',
	        Path => $logPath,
	);

         $msg->attach(
                Type => 'TEXT',
                Path => $scriptPath,
        );
	$msg->send();
}
1;
#sendLog('stojp', "RNC", 'elinyuu','/home/itk/users/elinyuu/Fetch_files.pm');
