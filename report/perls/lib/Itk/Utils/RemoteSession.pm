###----------------------------------------------------------------------------
# Modules handling remote command sessions (eg telnet, ssh), including
# towards CPP nodes.
#
# � Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form without the written
# permission of the copyright owner. The contents are subject to revision without
# notice due to continued progress in methodology, design and manufacturing.
# Ericsson shall have no liability for any error or damage of any kind resulting
# from the use of these documents.
#
# Ericsson is the trademark or registered trademark of Telefonaktiebolaget LM
# Ericsson. All other trademarks mentioned herein are the property of their
# respective owners.
#
#-------------------------------------------------------------------------------
#
# Author: David Markham
# $Id: RemoteSession.pm 7748 2012-10-24 03:52:54Z eanzmark $
#

use strict;

package Itk::Utils::RemoteSession;
use FileHandle;
use Exporter 'import';
our @EXPORT_OK=qw(startRemoteSession);

use constant {
	DEFAULT_LOGIN_TIMEOUT => 5,  # Default timeout for login phase (seconds)
	DEFAULT_COMMAND_TIMEOUT => 10,  # Default timeout for normal command handling phase (seconds)
};

use fields (
	'host',  # Remote hostname or IP address
	'port',  # Remote port
	'timeout',  # Current active timeout (seconds), or "0" for no timeout
	'loggedIn',  # Internal flag to reflect connection status
	'lastCommand',  # Last command issued
	'lastPrompt',  # Last prompt line seen
	'lastError',  # Last error received
	'printErrors',  # If set, errors will be printed
	'cliPrompt',  # Regex for CLI prompt matching
	'echoCommands',  # If set, issued commands will be echoed in output
	'outfh',  # Filehandle for writing session log to 
);

#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;  #reference to global server object
*srv=\$main::srv;

# Returns the default CLI prompt for this class
# Redefine in derived classes as necessary
sub _defaultCliPrompt { return undef; }

#==============================================================================
# CLASS METHODS
#==============================================================================

##------------------------------------------------------------------------------
# startRemoteSession(%opts)
# Factory method to return an appropriate Itk::Utils::RemoteSession derivative
# - %options
# % protocol  String indicating protocol: ssh|telnet
# % username  Username
# % password  Password
# % cpp  Flag to indicate the session is towards a CPP node
# % hostdata  Host data structure as returned by Itk::Config::HostTable::getHostData()  (Other options ignored if present)
# % node  Ref to MeContext MO  (Other options ignored if present)
#   See Itk::Utils::RemoteSession::new() for other options
# RETURNS: RemoteSession object
#------------------------------------------------------------------------------
sub startRemoteSession {
	$srv->logEnter(\@_) if ENTER;
	my (%opts) = @_;
	my ($session, $protocol, $username, $password, $cpp);

	if (my $node = $opts{node}) {
		# Handling for provision of an MeContext MO
		die "Invalid node - not an MeContext MO: $node" unless (ref $node eq 'Itk::NodeData::MO' and $node->getClass() eq 'MeContext');

		require Itk::NodeData::MO;
		$opts{host} = $node->getAttribute('itkAttrIpAddr');
		$username = $node->getAttribute('itkAttrUser') || 'itk';  #provide default username if none in DB
		$password = $node->getAttribute('itkAttrPassword');
		$protocol = $node->getAttribute('itkAttrSecurityActivated')? 'ssh' : 'telnet';
		my $nodeType = $node->getAttribute('itkAttrNodeType');
		$cpp = 1 if $nodeType =~ /^(?:RBS|RNC|MGW|RXI|CPP|ENB)$/i;
		if ($nodeType eq 'SIU') {
			$protocol = 'ssh';
			# FIXME - should be fixed as product matures
			$username = $node->getAttribute('itkAttrUser') || 'admin';
			$password = 'hidden' unless defined $password;
			$opts{prompt} = 'OSmon> $' unless defined $opts{prompt};
		}
		my $secureport = $node->getAttribute('itkAttrSecurePort');
		$opts{port} = $secureport if $secureport;  #pass a custom port number through if specified
		my $timeout = $node->getAttribute('itkAttrConnectionTimeout');
		$opts{timeout} = $timeout if $timeout; 

	} elsif (my $hd = $opts{hostdata}) {
		# Handling for provision of hostdata entry

		if ($hd->{hostaddress} eq 'localhost') {
			$protocol = 'local';
		} elsif ($hd->{nosecure}) {
			$protocol = 'telnet';
		} else {
			$protocol = 'ssh';
		}
		$username = $hd->{username};
		$password = $hd->{password};
		$opts{host} = $hd->{hostaddress} if defined $hd->{hostaddress};
		$opts{port} = $hd->{port} if defined $hd->{port};
		$opts{idfile} = $hd->{identity_file} if defined $hd->{identity_file};
		$opts{prompt} = $hd->{cli_prompt} if defined $hd->{cli_prompt};
		$opts{timeout} = $hd->{timeout} if defined $hd->{timeout};
	
	} else {
		# Default handling for use of normal options
		$protocol = delete $opts{protocol};
		$username = delete $opts{username};
		$password = delete $opts{password};
		$cpp = delete $opts{cpp};
	}

	die "No host specified" unless length($opts{host});
	$srv->logDebug(1, "Open $protocol session (CPP:$cpp) ($username):".join(', ', map { "$_=$opts{$_}" } keys(%opts))) if DEBUG1;
	if ($protocol eq 'ssh') {
		if ($cpp) {
			$session = Itk::Utils::CelloSsh->new(%opts);
		} else {
			$session = Itk::Utils::Ssh->new(%opts);
		}
	} elsif ($protocol eq 'telnet') {
		if ($opts{cpp}) {
			$session = Itk::Utils::CelloTelnet->new(%opts);
		} else {
			$session = Itk::Utils::Telnet->new(%opts);
		}
	} elsif ($protocol eq 'local') {
		$session = Itk::Utils::Local->new(%opts);
	} else {
		die "Unknown protocol '$protocol'";
	}
	unless ($session) {
		die "Can't open $protocol session to '$opts{host}'";
	}
	$session->login($username, $password) or do{
		$session->close();
		die "Can't login to $opts{host} as user '$username'";
	};
	
	return $session;
}

#==============================================================================
# PRIVATE METHODS
#==============================================================================

sub _handleError {
	my ($self, $errorMsg) = @_;
	$self->{lastError} = $errorMsg;  #remember this for lastError method
	return unless $self->{printErrors};
	$srv->logError(__PACKAGE__.": $errorMsg");
}


##------------------------------------------------------------------------------
# _convertOutputToList($outputRef)
# Converts multiline text to a list of single lines
# - $outputRef  Ref to scalar containing text
# RETURNS: @output
# @output  Output data, with each list entry storing a single line
#------------------------------------------------------------------------------
sub _convertOutputToList {
	my ($self, $outputRef) = @_;
	my @outputList = split(/[\012\015]/, $$outputRef);
	return @outputList;
}

	
#==============================================================================
# PUBLIC METHODS
#==============================================================================

##------------------------------------------------------------------------------
# new(%options)
# Constructs a new RemoteSession object (abstract class!)
# - %options
# % host  Remote hostname or IP address
# % port  Remote port
# % timeout  Login/command timeout to use initially ('0' for none)
# % printErrors  If set, errors will not be printed
# % prompt  CLI Prompt expression to use for remote node session
# % logfile  Path to logfile to store session log (file will be created)
# RETURNS: RemoteSession object
#------------------------------------------------------------------------------
sub new {
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->{host} = $options{host} or die __PACKAGE__.": No host provided\n";
	$self->{port} = $options{port};
	$self->{timeout} = defined $options{timeout} ? $options{timeout} : DEFAULT_LOGIN_TIMEOUT;

	$self->{loggedIn} = 0;  #false
	$self->{lastCommand} = '';
	$self->{lastPrompt} = undef;
	$self->{lastError} = 0;
	$self->{printErrors} = $options{printErrors};
	$self->{cliPrompt} = defined $options{prompt}? $options{prompt} : $self->_defaultCliPrompt();
	$self->{echoCommands} = $options{echoCommands};

	if ($options{logfile}) {
		$self->{outfh} = new FileHandle("> $options{logfile}") or
			die __PACKAGE__.": Unable to write to $options{logfile}\n";
	}
	
	return $self;
}


##------------------------------------------------------------------------------
# requestPassword([$prompt])
# Requests a password to be entered by the user on STDIN (typed characters not echoed)
# - $prompt  Optional string to prompt user with
# RETURNS: $password
# - $password Password string supplied by user
#------------------------------------------------------------------------------
sub requestPassword {
  my ($self, $prompt) = @_;
  $prompt = 'Password:' unless $prompt;  #default prompt if nothing supplied
  system "stty -echo";
  print $prompt;
  chomp(my $pw = <STDIN>);
  print "\n";
  system "stty echo";
  return $pw;  #NB - trailing chars has been removed
}


##------------------------------------------------------------------------------
# login($user, $password)
# Attempts to login to the defined host.
# - $user  User name
# - $password  Password
# RETURNS: Boolean indicating success
#------------------------------------------------------------------------------
sub login {
	my ($self, $user, $password) = @_;
	return undef;
}


##------------------------------------------------------------------------------
# sendCommand($cmd, %opts)
# Sends a command to this session.
# - $cmd  Commands to send
# - %options
# % list  Returns the output as a list, one line per list entry
# RETURNS: $output / @output
# - $output  Scalar output from command
# - @output  Array of output from command (one line per index) if "list" option is set
#------------------------------------------------------------------------------
sub sendCommand {
	my ($self, $cmd, %opts) = @_;
	return undef;
}


##------------------------------------------------------------------------------
# sendCommands(@commands)     
# Sends a list of commands to this session.  (DEPRECATED)
# - @commands  List of commands to send consecutively
# RETURNS: @output
# - @output  All output text, one array index per output line
#------------------------------------------------------------------------------
sub sendCommands {
	my ($self, @commands) = @_;
	my @output;
	foreach(@commands) {
		my @newOutput = $self->sendCommand($_, list=>1);
		return undef unless defined @newOutput;  #abort now if problems sending any of the commands
		push(@output, @newOutput);
	}
	return @output;
}


##------------------------------------------------------------------------------
# setTimeout($timeout)
# Sets a new timeout value to use
# - $timeout  Timeout value to use (seconds). "0" denotes no timeout.
# RETURNS: Boolean indicating success
#------------------------------------------------------------------------------
sub setTimeout {
	my ($self, $timeout) = @_;
	$self->{timeout} = $timeout;
	return 1;  #true
}


##------------------------------------------------------------------------------
# lastError()
# Supplies the last error seen in this session
# RETURNS: $error
# - $error  Last error message seen, "0" if none
#------------------------------------------------------------------------------
sub lastError {
	my ($self) = @_;
	return undef unless defined $self;
	return $self->{lastError};
}


##------------------------------------------------------------------------------
# setErrorDisplayState($newPrintState)
# Sets whether errors will be shown to user or not
# - $newPrintState  1= show errors, 0=hide errors
# RETURNS: nothing
#------------------------------------------------------------------------------
sub setErrorDisplayState {
	my ($self, $newPrintState) = @_;
	return undef unless defined $self;
	$self->{printErrors} = $newPrintState;
}


##------------------------------------------------------------------------------
# cliPromptExpression([$expr])
# Gets/Sets an expression to be used to match the command line prompt
# - $expr  Regex string matching prompt
# RETURNS: nothing
#------------------------------------------------------------------------------
sub cliPromptExpression {
	my ($self, $expr) = @_;
	if (defined $expr) {
		$self->{cliPrompt} = $expr;
	}
	return $self->{cliPrompt};
}


##------------------------------------------------------------------------------
# loggedIn($newHideState)
# Indicate whether session is opened and connected successfully
# RETURNS: $state
# - $state  1=session active, 0=session inactive
#------------------------------------------------------------------------------
sub loggedIn {
	my ($self) = @_;
	return if (!defined($self));
	return $self->{loggedIn};
}
	

##------------------------------------------------------------------------------
# writeToLogfile($text)
# Adds text to the logfile
# - $text  Text to add
# RETURNS: nothing
#------------------------------------------------------------------------------
sub writeToLogfile {
	my ($self, $text) = @_;
	my $outFH = $self->{outfh} or die "No logfile open";
	print $outFH $text;
}


##------------------------------------------------------------------------------
# close()
# Close the session
# RETURNS: Boolean indicating success
#------------------------------------------------------------------------------
sub close {
	my ($self) = @_;
	$self->{outfh}->close() if $self->{outfh};
	return 1;
}


#############################################################################

package Itk::Utils::Telnet;
#############################################################################
# Implements telnet 

use base 'Itk::Utils::RemoteSession';
use Net::Telnet;

use fields (
	'prompt',  # Prompt expression to use for remote node session
	'session',  # Holds Net::Telnet session object
	'http',  
);

# Default CLI prompt regex
sub _defaultCliPrompt { return '(\$|\S+>) $'; }


##------------------------------------------------------------------------------
# _errorSub(%options)
# Internal error handling sub for passing to Net::Telnet to get access to error text
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub _errorSub {
	my $self = shift;
	return unless defined($self);
	my $msg;
	if (defined $self->{session}) {
		$msg = $self->{session}->errmsg();
	} else {
		$msg = 'Session not open';
	}
	$msg = "(undefined)" unless defined $msg;
	return if $msg =~ /pattern match read eof/;  #ignore this error
	
	if ($msg =~ /eof read waiting for command prompt: Login failed/) {
		$msg = 'Login failure: incorrect password?';  #map this message to give a better explanation
	}
	$msg = "($self->{host}): $msg";  #add in IP address
	if ($msg =~ /command timed-out/) {
		$msg .= " - \"$self->{lastCommand}\"";    #add command which timed out
	}
	$self->_handleError($msg);
	return;
}


##------------------------------------------------------------------------------
# new(%options)
# Constructs a new Telnet object.
# See Itk::Utils::RemoteSession::new() for base class details.
# - %options
# % dumplog  Sets "Dump_Log" option for Net::Telnet to specify log file for debugging comms
# % http  Flag indicating that a HTTP connection is required
# RETURNS: Telnet object
#------------------------------------------------------------------------------
sub new {
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->SUPER::new(%options);

	#Set up parameters which will be passed to Net::Telnet
	my $timeout = defined $self->{timeout}? $self->{timeout} : Itk::Utils::RemoteSession::DEFAULT_LOGIN_TIMEOUT;  #start with login timeout if not specified as user
	my %netTelnetHash = (
		Host => $self->{host},
		Timeout => $timeout,
		Prompt => '/'.$self->{cliPrompt}.'/',
		Dump_Log => $options{dumplog},
		Errmode => [\&_errorSub, $self],  #point to internal error method
	);
	if ($options{http}) {
		$self->{http} = 1;
		$netTelnetHash{Port} = 80;  #change to port 80
		$netTelnetHash{Prompt} = $self->{cliPrompt} = '/<\/HTML>$/';  #reassign this to be the end of the HTML page
		undef($options{user});  #make sure that we don't try to log in for HTTP
	}
	$srv->logDebug(2, "Launch telnet to $self->{host}".($netTelnetHash{Port}? ':'.$netTelnetHash{Port} : '')) if DEBUG2;
	$self->{session} = new Net::Telnet(%netTelnetHash) || return undef;

	#Initialise other internal variables
	if (defined $options{user} and defined $options{password}) {  #if login details are provided, try to login now
		$self->login($options{user}, $options{password}) or return undef;
	}
	return $self;
}


##------------------------------------------------------------------------------
# login($user, $password)
# See Itk::Utils::RemoteSession::login() for base class details.
#------------------------------------------------------------------------------
sub login {
	my ($self, $user, $pw) = @_;

	$self->{session}->login($user, $pw) or return undef;
	$self->{loggedIn} = 1;

	#Now that logged in to node, change to default command timeout if none specified in constructor
	my $timeout = defined $self->{timeout}? $self->{timeout} : Itk::Utils::RemoteSession::DEFAULT_COMMAND_TIMEOUT;  #set default command timeout if not specified as user
	$self->setTimeout($timeout);

	return 1;  #true = successful
}  


##------------------------------------------------------------------------------
# sendCommand($cmd, %opts)
# See Itk::Utils::RemoteSession::sendCommand() for base class details.
#------------------------------------------------------------------------------
sub sendCommand {
	my ($self, $command, %opts) = @_;
	return undef if not defined($self) or not $self->{loggedIn};
	my @output;
	my $s = *{$self->{session}}->{net_telnet};
	return undef if (!$s->{opened});
	if ($self->{echoCommands}) {
		push(@output, $self->{session}->last_prompt.$command);  #fake command line if 'echoCommands' active
	}
	$self->{lastCommand} = $command;  #remember this in case of timeout
	my $outFH = $self->{outfh};
	foreach ($self->{session}->cmd($command)) {  #issue command, collect output
		chomp;
		print $outFH "$_\n" if $outFH;
		push(@output, $_);
	}
	if ($self->{session}->timed_out) {
		shift(@output);  #remove faked command since this will be in input buffer already
		$self->sendInterrupt();  #interrupt printouts which may be continuing after timeout
		my ($buffer, $prompt) = $self->{session}->waitfor($self->{session}->prompt);  #read input buffer manually
		return undef if (!defined($buffer));
		$self->{session}->last_prompt($prompt);  #assign interrupted prompt, save last one
		foreach (split(/\n/, $buffer)) {
			chomp;
			push(@output, $_);
		}
		$output[0] = $prompt.$output[0];  #fix first entry so that it contains prompt
	}
	return @output if $opts{list};
	# FIXME - more efficient to do this without the temp array when there is time to check the timeout handling
	my $output;
	$output .= "$_\n" foreach @output;
	return $output;
}


##------------------------------------------------------------------------------
# sendInterrupt()
# Sends an interrupt to the session.
# RETURNS: Nothing
#------------------------------------------------------------------------------
sub sendInterrupt {
	my ($self) = @_;
	return if not defined($self) or not $self->{loggedIn};
	my $s = *{$self->{session}}->{net_telnet};
	return unless $s->{opened};
	$self->{session}->put("\003");  #send control-C to interrupt printout
}


##------------------------------------------------------------------------------
# httpGet($page)
# Constructs a new RemoteSession object.
# - $page  Path of page to retrieve (relevant for web server, without address information)
# RETURNS: $output
# - $output  All output text
#------------------------------------------------------------------------------
sub httpGet {
	my ($self, $page) = @_;
	return undef unless defined $self;
	my $s = *{$self->{session}}->{net_telnet};
	return undef unless $s->{opened};
	my @output;
	my $command = "GET $page";
	$self->{lastCommand} = $command;  #remember this in case of timeout
	foreach ($self->{session}->cmd($command)) {  #issue command, collect output
		chomp;
		push(@output, $_);
	}
	if (!$self->{session}->timed_out) {
		push(@output,'</HTML>');  #prompt has been removed - add this artifically to complete HTML
	}
	# FIXME - more efficient to do this without the temp array when there is time to check the timeout handling
	my $output;
	$output .= "$_\n" foreach @output;
	return $output;
}


##------------------------------------------------------------------------------
# setTimeout($timeout)
# See Itk::Utils::RemoteSession::setTimeout() for base class details.
#------------------------------------------------------------------------------
sub setTimeout {
	my ($self, $timeout) = @_;
	return unless $self->{session};
	$self->{timeout} = $timeout;
	undef $timeout if $timeout==0;  #Net::Transport uses 'undef' to indicate no timeout
	$srv->logDebug(2, "Setting timeout to '$timeout'") if DEBUG2;
	return $self->{session}->timeout($timeout);
}


##------------------------------------------------------------------------------
# close()
# See Itk::Utils::RemoteSession::close() for base class details.
#------------------------------------------------------------------------------
sub close {
	my ($self) = @_;
	$srv->logDebug(1, "Closing session") if DEBUG1;
	$self->sendCommand("exit") if $self->{loggedIn};
	$self->{session}->close() if $self->{session};
	$self->{session} = undef;
  $self->{loggedIn} = 0;
  $self->{host} = 'undefined';
	$self->SUPER::close();
	return 1;
}


#############################################################################

package Itk::Utils::Ssh;
#############################################################################
# Implements ssh using Net::SSH2
# FIXME - not in a working state yet, will always fall through to SshExpect now

use base 'Itk::Utils::RemoteSession';
our $LIB_SSH2_Available;
#BEGIN {
#	eval {
#		require Net::SSH2;
#		import Net::SSH2;
#		$LIB_SSH2_Available=1;
#	};
#}

use fields (
	'ssh',  # 
	'password',  # 
	'localwd',  #
	'remotewd',
	'cmdchan',
);

#use constant {
#};


sub _establishCmdChannel {
	my ($self, $cmd) = @_;
	my $chan = $self->{ssh}->channel() or die "Can't establish channel to $self->{host}";
	$chan->shell() or die "Can't start shell on $self->{host}";
	$self->{cmdchan} = $chan;
}


##------------------------------------------------------------------------------
# new(%options)
# Constructs a new object.
# See Itk::Utils::RemoteSession::new() for base class details.
# - %options
# % host  Remote hostname or IP address
# % port  Remote port
# % timeout  Login/command timeout to use initially
# % echoCommands  If set, issued commands will be echoed in output
# RETURNS: Ssh object
#------------------------------------------------------------------------------
sub new {
	# If lib ssh2 was not able to be loaded then we try to use the expect version instead
	my __PACKAGE__ $self = shift;
	return Itk::Utils::SshExpect->new(@_) unless $LIB_SSH2_Available;
	
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->SUPER::new(%options);
	$self->{ssh} = Net::SSH2->new();
	$self->{ssh}->debug(1) if DEBUG3;  #FIXME - change to DEBUG4?
	$self->{port} = 22 unless $self->{port};  #default port
	
	require IO::Socket::INET;
	my $sock = IO::Socket::INET->new(PeerHost=>$self->{host}, PeerPort=>$self->{port})
		or die "Failed to connect to $self->{host}:$self->{port}";
#	$sock->sockopt(SO_LINGER, pack('SS', 0, 0));

	$self->{timeout} = 5 unless defined $self->{timeout};  #FIXME - temp timeout until support is extended

	#set timeout if defined
	if ($self->{timeout}) {
		my @poll = ({ handle => $sock, events => 'in' });
		$srv->logDebug(3, "Poll:in") if DEBUG3;
		return undef unless $self->{ssh}->poll($self->{timeout} * 1000, \@poll) and $poll[0]->{revents}->{in};
		$srv->logDebug(3, "Poll:in:ok") if DEBUG3;
	}

	#test for startup error conditions on the connection (strange states sometimes seen on forwarded ssh ports)
	my @pollErr = ({ handle => $sock, events => ['err','hup','nval'] });
	my $pollErrVal = $self->{ssh}->poll(100, \@pollErr);
	if (not defined $pollErrVal or $pollErrVal > 0) {  #if an error in polling, or any of these events were found
		$srv->logDebug(1, "pollErr returned err:$pollErr[0]->{revents}->{err},hup:$pollErr[0]->{revents}->{hup},nval:$pollErr[0]->{revents}->{nval}") if DEBUG1;
		return undef;
	}
	$srv->logDebug(3, "Poll:err:ok") if DEBUG3;
	
	eval { $self->{ssh}->connect($sock); };
	$srv->logDebug(3, "connect:ok") if DEBUG3;
	#Net::SSH2 croaks if connect fails - need to report this back as failure to caller
	if ($@) {
		$srv->logDebug(1, $@) if DEBUG1;
		return undef;
	}
	
	return $self;
}


##------------------------------------------------------------------------------
# lastError()
# See Itk::Utils::RemoteSession::lastError() for base class details.
#------------------------------------------------------------------------------
sub lastError {
	my ($self) = @_;
	return undef unless $self->{ssh};
	return 0 unless $self->{ssh}->error;  #return false if no error
	
	my @errList = $self->{ssh}->error;
	return $errList[2];  #error string
}


##------------------------------------------------------------------------------
# login($user, $password)
# See Itk::Utils::RemoteSession::login() for base class details.
#------------------------------------------------------------------------------
sub login {
	my ($self, $user, $password) = @_;
	return undef unless $self->{ssh};

	#Warning - auth_list is causing glibc crash in libssh2 0.18
	#my @authList = $self->{ssh}->auth_list($user);  #get available auth methods from remote server

	$srv->logDebug(3, "attempting authentication...") if DEBUG3;
	my $authMethod = $self->{ssh}->auth(rank=>['password', 'keyboard-auto'], username=>$user, password=>$password) or do{
		$srv->logDebug(1, "Unable to authenticate: ".$self->lastError()) if DEBUG1;
		return undef;
	};
	$srv->logDebug(3, "Successful login with auth method '$authMethod'") if DEBUG3;

	return 1;
}


##------------------------------------------------------------------------------
# sendCommand($cmd, %opts)
# See Itk::Utils::RemoteSession::sendCommand() for base class details.
#------------------------------------------------------------------------------
sub sendCommand {
	my ($self, $cmd, %opts) = @_;
	$self->_establishCmdChannel() unless $self->{cmdchan};
	
	return undef;
}


##------------------------------------------------------------------------------
# close()
# See Itk::Utils::RemoteSession::close() for base class details.
#------------------------------------------------------------------------------
sub close {
	my ($self) = @_;
	$self->{ssh}->disconnect();
	$self->SUPER::close();
	return 1;
}

#####################################################################################

package Itk::Utils::SshExpect;
#############################################################################
# Implements ssh using perl Expect

use base 'Itk::Utils::RemoteSession';
our $Expect_Available;
BEGIN {
	eval {
		require Expect;
		import Expect;
		$Expect_Available=1;
	};
}

$Expect::Debug = 3 if DEBUG5;

use fields (
	'expectSess',  # Expect ssh session
	'passwordPrompt',  # Expression to match the login password prompt
	'loginKeyPrompt',  # Expression to match the confirmation at login of a new SSH key for a host
	'identityFilePath',  # Path to private keyfile to be used for connection
);

use constant {
	SSH_BIN => 'ssh',  # ssh executable (require in path to avoid specifying location here)
};

# x1B is leading character for ANSI escape code
sub _defaultCliPrompt { return '[$>] (?:\x1B.{0,5})?$'; }

##------------------------------------------------------------------------------
# new(%options)
# Constructs a new SshExpect object.
# % idfile  Path to private keyfile to be used for connection (must not have a stored passphrase)
# See Itk::Utils::RemoteSession::new() for base class details.
# RETURNS: SshExpect object
#------------------------------------------------------------------------------
sub new {
	die "Neither Net::SSH2 nor Expect are available" unless $Expect_Available;
	
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->{identityFilePath} = delete $options{idfile};
	$self->SUPER::new(%options);

	$self->{passwordPrompt} = 'assword';
	$self->{loginKeyPrompt} = 'yes\/no';

	$self->{expectSess} = undef;
	return $self;
}


##------------------------------------------------------------------------------
# login($user, $password)
# See Itk::Utils::RemoteSession::login() for base class details.
#------------------------------------------------------------------------------
sub login {
	my ($self, $user, $password) = @_;
	return 1 if $self->{expectSess};  #return true if already started
	my $sshcmd = SSH_BIN;
	$sshcmd .= " -l $user";
	$sshcmd .= " -i \"$self->{identityFilePath}\"" if defined $self->{identityFilePath};
	$sshcmd .= " -p \"$self->{port}\"" if defined $self->{port};
	$sshcmd .= " $self->{host}";
	$srv->logDebug(1, "Launching '$sshcmd'") if DEBUG1;
	
	$self->{expectSess} = Expect->new() || die "Unable to start expect"; 
$self->{expectSess}->raw_pty(1);
#$self->{expectSess}->slave->stty(qw(raw -echo));
	$self->{expectSess}->spawn($sshcmd) || die "Unable to launch ssh session '$sshcmd'"; 

#$self->{expectSess} = Expect->new($sshcmd) || die "Unable to start expect";
	$self->{expectSess}->log_stdout(DEBUG5? 1 : 0);  #set debugging level in expect to get output here
#	$self->{expectSess}->slave->stty(qw(raw -echo)) unless $self->{echoCommands};
#$self->{expectSess}->log_file("output.log");

	$self->{expectSess}->expect($self->{timeout}, '-re', $self->{passwordPrompt}.'|'.$self->{loginKeyPrompt}) ||
		die "Unable to connect to $self->{host}"; 
	my $rematch = $self->{expectSess}->exp_match();  #string that matched expect
	my $keyprompt = $self->{loginKeyPrompt};
	if ($rematch =~ /$keyprompt/) {  #initial ssh connection, need to accept key
		$self->{expectSess}->send("yes\n");
		$self->{expectSess}->expect($self->{timeout}, '-re', $self->{passwordPrompt}) ||
			die "Unable to handshake with $self->{host}"; 
	}
	$self->{expectSess}->send("$password\n");
	$self->{expectSess}->expect($self->{timeout}, '-re', $self->{cliPrompt}) ||
		die "Unable to login to $self->{host}"; 
	# Store text on last unterminated line (cli prompt)
	$self->{lastPrompt} = '';
	$self->{lastPrompt} .= $1 if $self->{expectSess}->before() =~ /([^\015\012]+)$/;
	$self->{lastPrompt} .= $self->{expectSess}->match();
	return 1;
}


##------------------------------------------------------------------------------
# sendCommand($cmd, %opts)
# See Itk::Utils::RemoteSession::sendCommand() for base class details.
#------------------------------------------------------------------------------
sub sendCommand {
	my ($self, $cmd, %opts) = @_;
	return undef unless $self->{expectSess};
	
	$self->{lastCommand} = $cmd;
	$srv->logDebug(5, "Sending '$cmd'") if DEBUG5;
	$self->{expectSess}->send("$cmd\n");
	$self->{expectSess}->expect($self->{timeout}, '-re', $self->{cliPrompt}) or do{
		$self->_handleError("Unable to execute '$cmd'");
		return undef;
	};
	my $output;
	if ($self->{echoCommands}) {
		if ($opts{delCmd}) {
			$output = $self->{lastPrompt};
		} else {
			$output = $self->{lastPrompt}.$cmd."\n";
		}
		$output .= $self->{expectSess}->before();
	} else {
		$output = $self->{expectSess}->before();
		if ($opts{delCmd}) {
			$output =~ s/^$cmd[\012\015]+//;
		}
	}
	# Remove and store text on last unterminated line (cli prompt)
	$self->{lastPrompt} = '';
	$self->{lastPrompt} .= $1 if $output =~ s/([^\015\012]+)$//;
	$self->{lastPrompt} .= $self->{expectSess}->match();

	$srv->logDebug(6, "Received:\n$output") if DEBUG6;
	if (my $outFH = $self->{outfh}) {
		print $outFH $output;
	}
	return $self->_convertOutputToList(\$output) if $opts{list};
	return $output;
}


##------------------------------------------------------------------------------
# close()
# See Itk::Utils::RemoteSession::close() for base class details.
#------------------------------------------------------------------------------
sub close {
	my ($self) = @_;
	return unless $self->{expectSess};
	$self->{expectSess}->send("exit\n");
	$self->{expectSess}->soft_close();
	$self->{expectSess} = undef;
	$self->SUPER::close();
	return 1;
}


#############################################################################

package Itk::Utils::CelloTelnet;
#############################################################################
# Implements telnet towards CPP nodes using Net::Telnet

use base 'Itk::Utils::Telnet';

##------------------------------------------------------------------------------
# new(%options)
# Constructs a new CelloTelnet object.
# See Itk::Utils::Telnet::new() for base class details.
# RETURNS: CelloTelnet object
#------------------------------------------------------------------------------
sub new {
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->SUPER::new(%options);
	return $self;
}


##------------------------------------------------------------------------------
# login($user, $password)
# See Itk::Utils::RemoteSession::login() for base class details.
#------------------------------------------------------------------------------
sub login {
	my ($self, $user, $pw) = @_;
	if (length($user)==0) {  #can't log on with no username to Cello
		$self->_handleError(__PACKAGE__.": No username provided for login to $self->{host}");
		return undef;
	}
	return $self->SUPER::login($user, $pw);
}  


##------------------------------------------------------------------------------
# lhsh($linkhandler)
# Attempts to open lhsh towards a link handler
# - $linkhandler  Linkhandler string (eg '001400')
# RETURNS: Boolean indicating if session is now connected to the requested linkhandler
#------------------------------------------------------------------------------
sub lhsh {
	my ($self, $linkhandler) = @_;
	return if not defined($self) or not $self->{loggedIn};

	my $output = $self->sendCommand("lhsh $linkhandler");
	return 0 if (not defined $output or length($output)==0);
	return 0 unless $self->{session}->last_prompt =~ /$linkhandler/;
	return 0 if $output =~ /failed to connect to/;
	return 1;
}


#############################################################################

package Itk::Utils::CelloSsh;
#############################################################################
# Implements ssh towards CPP nodes using Net::SSH2
# FIXME - not implemented properly yet

use base 'Itk::Utils::Ssh';

##------------------------------------------------------------------------------
# new(%options)
# Constructs a new CelloSsh object.
# See Itk::Utils::Ssh::new() for base class details.
# RETURNS: CelloSsh object
#------------------------------------------------------------------------------
sub new {
	# If lib ssh2 was not able to be loaded then we try to use the expect version instead
	my __PACKAGE__ $self = shift;
	return Itk::Utils::CelloSshExpect->new(@_) unless $Itk::Utils::Ssh::LIB_SSH2_Available;
	
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->SUPER::new(%options);
	return $self;
}


#####################################################################################

package Itk::Utils::CelloSshExpect;
#############################################################################
# Implements ssh towards CPP nodes using Expect

use base 'Itk::Utils::SshExpect';

# CPP command line prompt
sub _defaultCliPrompt { return '\$ $'; }


##------------------------------------------------------------------------------
# new(%options)
# Constructs a new CelloSshExpect object.
# See Itk::Utils::SshExpect::new() for base class details.
# RETURNS: CelloSshExpect object
#------------------------------------------------------------------------------
sub new {
	die "Neither Net::SSH2 nor Expect are available" unless $Itk::Utils::SshExpect::Expect_Available;
	
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->SUPER::new(%options);

	return $self;
}

##------------------------------------------------------------------------------
# sendCommand($cmd, %opts)
# See Itk::Utils::RemoteSession::sendCommand() for base class details.
#------------------------------------------------------------------------------
sub sendCommand {
	my ($self, $cmd, %opts) = @_;
	# CPP ssh daemon echoes commands in output - need to remove this
	my $output = $self->SUPER::sendCommand($cmd, delCmd=>1);
	return $self->_convertOutputToList(\$output) if $opts{list};
	return $output;
}  


#############################################################################

package Itk::Utils::Local;
#############################################################################
# Implements local session 

use base 'Itk::Utils::RemoteSession';

use fields (
	'prompt',  # Prompt expression to use for remote node session
);

# Default CLI prompt regex
sub _defaultCliPrompt { return '(\$|\S+>) $'; }


##------------------------------------------------------------------------------
# new(%options)
# Constructs a new Local object.
# See Itk::Utils::RemoteSession::new() for base class details.
# - %options
# RETURNS: Local object
#------------------------------------------------------------------------------
sub new {
	my __PACKAGE__ $self = shift;
	my (%options) = @_;
	$self = fields::new($self) unless (ref $self);
	$self->SUPER::new(%options);

	return $self;
}


##------------------------------------------------------------------------------
# login($user, $password)
# See Itk::Utils::RemoteSession::login() for base class details.
#------------------------------------------------------------------------------
sub login {
	my ($self, $user, $pw) = @_;

	$self->{loggedIn} = 1;

	return 1;  #true = successful
}


##------------------------------------------------------------------------------
# sendCommand($cmd, %opts)
# See Itk::Utils::RemoteSession::sendCommand() for base class details.
#------------------------------------------------------------------------------
sub sendCommand {
	my ($self, $command, %opts) = @_;
	return undef if not defined($self) or not $self->{loggedIn};

	my $output;
	
	if ($self->{echoCommands}) {
		$output = $command . "\n";
		print "echo command running for scalar\n"
	}
	$output .= `$command 2>&1 3>&1`;
	if (my $outFH = $self->{outfh}) {
		print $outFH $output;
	}
	$srv->logDebug(1, "Result of '$command' is: $?, output: $output\n") if DEBUG1;	

	return $self->_convertOutputToList(\$output) if $opts{list};
	return $output;
}


##------------------------------------------------------------------------------
# close()
# See Itk::Utils::RemoteSession::close() for base class details.
#------------------------------------------------------------------------------
sub close {
	my ($self) = @_;
	$srv->logDebug(1, "Closing session") if DEBUG1;
	$self->{loggedIn} = 0;
	$self->SUPER::close();
	return 1;
}


#####################################################################################

1; # Modules must return 1
