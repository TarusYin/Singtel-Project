#!/usr/bin/perl
###----------------------------------------------------------------------------
# Parser/Analyser for PMD files
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
#
# Author: EHARMIC
# $Id: PmdParser.pm 6676 2012-04-05 03:20:14Z eharmic $

package Itk::EventData::PmdParser;
use strict;
use Time::Local qw(timegm_nocheck);
use Hash::Util qw(lock_keys);


BEGIN {
	if (DEBUG5) {
		require Data::Dumper;
		import Data::Dumper;
	}
}

#==============================================================================
# CONSTANTS
#==============================================================================
use constant {
	# Result Codes
	OK => 1,			# Operation succeeded
	PARSE_FAILED => -1,		# Parsing failed
	MIN_PMD_MATCH_TIME => 30,	# Minimum time window for finding a PMD (secs)
	PMD_TIME_MISMATCH_THRESH => 10,	# Threshold for mismatch warning between PMD time and syslog time 
};
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
#	'pmdtable',		# Table of PMD information
	'btindex',		# Index of PMDs by board and time
	'nodename',		# Node name (used for warnings and errors)
	'zpm',			# Path to ZPM
	'dumpdir',		# Path to move dumps to after parsing
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;
*srv=\$main::srv;
#==============================================================================
# CLASS METHODS
#==============================================================================

#==============================================================================
# PRIVATE METHODS
#==============================================================================
##-----------------------------------------------------------------------------
# _warn($msg)
# Issue warning, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _warn {
	$srv->logWarn("($_[0]->{nodename}) $_[1]");
}

##-----------------------------------------------------------------------------
# _error($msg)
# Issue Error, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _error {
	$srv->logError("($_[0]->{nodename}) $_[1]");
}

##-----------------------------------------------------------------------------
# _weekToDate($yy, $ww)
# Convert year and week number to date of the monday of the given week, according
# to ISO8601 rules.
# - $yy 	Year
# - $ww		Week Nr
# RETURNS: $date
# - $date	Date as a yyyy-mm-dd formatted string
#------------------------------------------------------------------------------
sub _weekToDate {
	my ($yy,$ww)=@_;
	
	my $j1_ts;
	eval {
		$j1_ts = timegm_nocheck(0,0,0,1,0,$yy);
	};
	if ($@) {
		return undef;
	}
	
	my $j1_dow = (gmtime($j1_ts))[6];
	my $w1_ts; # epoch ts of the first day of week 1
	if ($j1_dow >=1 and $j1_dow <= 3) { # Mon, Tues, Wed
		$w1_ts=$j1_ts - ($j1_dow-1)*86400;
	} else { # Thu, Fri, Sat, Sun
		$w1_ts=$j1_ts + (8-$j1_dow)*86400;
	}
	my $ts = $w1_ts + ($ww-1)*604800;
	
	return Itk::EventData::CollectionController::fmtTime($ts,1);
}

#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new(%opts)
# Constructor
# % nodename	Name of currently processed node (used for warning and error prints)
# % zpm		Path to ZPM executable
# % dumpdir     Directory to move dumps to after parsing
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self, %opts)=@_;
	lock_keys(%opts, qw(nodename zpm dumpdir));
	$self=fields::new($self) unless ref $self;
	$self->{btindex}={};
	$self->{nodename}=$opts{nodename};
	$self->{zpm}=$opts{zpm};
	$self->{dumpdir}=$opts{dumpdir};
	return $self;
}

##-----------------------------------------------------------------------------
# parse($file)
# Parses the indicated PMD file
# - $file	Filename of PMD to parse
# RETURNS: $resultcode, $pmdInfo
# - $resultCode	  Result Code - see constants for definitions
# - $pmdInfo 	Hash of PMD data as follows:
# 		 * board => Board that the restart occurred on
#		 * id => PMD Id
#		 * time => Time that the restart occurred (epoch secs)
#		 * errorno => error number
#		 * extradata => extra data
#		 * process => process name
#		 * block => Name of the block
#		 * productname => eg. GPB, ET-MFG, etc
#		 * productnumber => ROJ Number
#		 * productrevision => hw revision
#		 * productdate => manufacture dateEg. 07W09
#		 * serialnumber => hw serial no
#		 * fingerprint => The restart fingerprint
#		 * capsule  => the last capsule state
#------------------------------------------------------------------------------
sub parse {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $file)=@_;
	
	$srv->logDebug(4, "Parse File: '$file'") if DEBUG4;
	my $fh;
	open $fh, "$self->{zpm} $file|" or do {
		$self->_error("Failed to run zpm on $file: $!");
		return PARSE_FAILED,undef;
	};
	
	my %pmdInfo;
	
	# A note about the style of this parser. I have used sequences of
	# 'while' blocks for efficiency since I know the order in which items
	# will appear
	
	while (<$fh>) {
		if (/Dump identity\s*:\s*(0x[0-9a-f]+)/) {
			$pmdInfo{id}=$1;
			last;
		}
	}

BLOCK:
	while (<$fh>) {
		
		next unless (/^\[/);
		chomp;
						
		if (/^\[ERROR CODE\]/) {
			$srv->logDebug(6, "Found Error Code") if DEBUG6;
			while (<$fh>) {

				if (/^\s*PMD-id: 0x[0-9a-fA-F]+\s+Board location:\s+(\d{4})(\d+)(.sp)?/) {
					if ($1) {
						$pmdInfo{board}="${1}00";
						$pmdInfo{scope}="SPM" if $3;
					}
				}

				if (/^\s*Time: (\d{2})-(\d{2})-(\d{2})\s+(\d{2}):(\d{2}):(\d{2})/) {
					$srv->logDebug(6, "Found Time: $1-$2-$3 $4:$5:$6") if DEBUG6;
					# NB. Above has only 2 digits for year! Not y3k safe..
					eval {
						$pmdInfo{'time'}=timegm_nocheck($6,$5,$4,$3,$2-1,$1);
					};
					if ($@) {
						$self->_warn("Invalid Time in $file:$. '$1-$2-$3 $4:$5:$6' - PMD will be discarded");
						goto ABORT;
					}
				}
				

				if (/^\s*ERROR NUMBER (0x[0-9a-fA-F]+) WITH EXTRA DATA (0x[0-9a-fA-F]+)/) {
					$pmdInfo{errorno}=$1;
					$pmdInfo{extradata}=$2;
					# Next line should be process
					$_=<$fh>; chomp;
					if (/^\s*PROCESS (.+)/) {
						$pmdInfo{process}=$1;
					} else {
						$self->_warn("Process Name not found!");
					}
				}

				if (/^\s*BLOCK\s+(\S+)/) {
					$pmdInfo{block}=$1;
				}
				
				last if (/^\[/);

			}
			
			redo BLOCK;
			
		} elsif (/^\[FLASH PARAMETERS\]/) {
			$srv->logDebug(6, "Found Flash Params") if DEBUG6;
			while (<$fh>) {
				chomp;
				# Note: these seem to occur in differing orders
				if (/^\s*\*?productname=(.+)$/) {
					$pmdInfo{productname}=$1;
					next;
				}
				if (/^\s*\*?productnumber=(.+)$/) {
					$pmdInfo{productnumber}=$1;
					next;
				}
				if (/^\s*\*?productrevision=(.+)$/) {
					$pmdInfo{productrevision}=$1;
					next;
				}
				if (/^\s*\*?productdate=(.+)$/) {
					my $pdate=$1;
					if ($pdate =~ /^(\d{2}W\d{2})$/) {
						my $y;
						if ($1>90) { # Cello nodes not invented before 1990.. 
							$y=1900+$1;
						} else {
							$y=2000+$1;
						}
						$pmdInfo{productdate}=_weekToDate($y,$2);
					} elsif ($pdate =~ /^(\d{4})(\d{2})(\d{2})$/) {
						$pmdInfo{productdate}="$1-$2-$3";
					} elsif ($pdate =~ /^\d{4}-\d{2}-\d{2}$/) {
						$pmdInfo{productdate}=$pdate;
					} else {
						$self->_warn("$file: Invalid production date '$pdate'");
					}
					next;
				}
				if (/^\s*\*?serialnumber=(.+)$/) {
					$pmdInfo{serialnumber}=$1;
					next;
				}
				last if (/^\s*$/); # blank line
			}
			
			next BLOCK;
			
		} elsif (/^\[LOADED PROGRAMS\]$/) {
			my @loaded_progs;
			$srv->logDebug(6, "Found Loaded Programs") if DEBUG6;
			while (<$fh>) {
				if (/^\s*Load module id: ([A-Z]{3}\d{7}_R\S+)\s*$/) {
					push @loaded_progs,$1;
					next;
				}
				last if (/^\[/);
			}
			$pmdInfo{loadedprograms}=join(':',@loaded_progs);
			
			redo BLOCK;
		} elsif (/^\[ERROR LOG\]/) {
			$srv->logDebug(6, "Found Error Log") if DEBUG6;
			# We keep going until we find the last error information
			# which corresponds to the current restart
			my $errorInfo;
			while (<$fh>) {
				chomp;
				if (/Error Information:/) {
					$errorInfo='';
					while (<$fh>) {
						chomp;
						last if /^\s*$/; # End at blank line
						s/^\s+//; # trim leading and tailing whitespace
						s/\s+$//;
						s/[;[:cntrl:]]//g; # Strip any semicolons or control characters
						$errorInfo.=$_;
					}
				} elsif (/Error Description\s+:\s+\[([^\]]+)\]/) {
					$errorInfo=$1;
					while (<$fh>) {
						last if /^----/ || /^\s*$/;
					}
				}
				last if (/^\[/);
			}
			$pmdInfo{errordesc}=$errorInfo;
			redo BLOCK;
				
		} elsif (/^\[CALLTRACE FOR PROCESS 0x[0-9a-fA-F]+ (.+)\]$/) {
			$srv->logDebug(6, "Found Calltrace for process $1") if DEBUG6;
			next BLOCK if $1 eq 'wdog_helper';
			# Look for elements of stack backtrace
			my @stacktrace;
			while (<$fh>) {
				chomp;
				if (/^\s*[0-9a-fA-F]{8}  \[([0-9a-fA-F]{8})\],(\S+)\s*$/) {
					push @stacktrace, "$1,$2";
					next;
				}
				last if (/^\[/);
			}
			$pmdInfo{stacktrace}=join(':',@stacktrace) if @stacktrace;
			
			redo BLOCK;
		} elsif (/^\[CX.*\]$/) {
			$srv->logDebug(6, "Protential capsule info " ) if DEBUG6;
			my $message=undef;
			my $state=undef;
			my $capsule=undef;
			while (<$fh>) {
				chomp;
				$srv->logDebug(6, " capsule block : $_ " ) if DEBUG6;			
				# verify the last capsule state
				if (/last active capsule/) {
					next;
				} elsif	(/capsule\s=\s(\w+)/) {
						$capsule = $1;
						$srv->logDebug(6, "capsule : $capsule" ) if DEBUG6;
						next;
				} elsif (/message\s=\s(.*)/) {
						$message = $1;
						$srv->logDebug(6, "message : $message" ) if DEBUG6;
						next;
				} elsif	(/state\s=\s(.+)$/) {
						$state= $1;
						$srv->logDebug(6, "state : $state" ) if DEBUG6;
						last;
				} else {
					last if (/^\[/);
				}
			} # while
			if (defined ($capsule)) {
				$pmdInfo{capsule} = $capsule."@".$state."<".$message;
			}
			next BLOCK;
		} # if
	} # main while	
	close $fh;
	#lock_keys(%pmdInfo);
	return OK,\%pmdInfo;
	
ABORT:
	close $fh;
	return PARSE_FAILED, undef;

}

##-----------------------------------------------------------------------------
# parseFiles($pmdFileList, $callback, %opts)
# Parses a batch of PMD files, storing the fingerprints in a local lookup table
# so they can be later matched to syslog entries
# - $pmdFileList	Ref to array containing PMD Info (as returned from FileFetcher)
#			Each element in the array is a hash with the following keys:
#			 * localfile => the local path to the fetched file.
#			 * file => original filename on node
#			 * id => pmd id
#			 * board => board that generated the PMD
# - $callback 	Ref to callback sub
# % pmddelivery	Specifies method for PMD delivery. If false or not present then
#           legacy delivery of PMDs applies. If true then they are moved into the
# 			dumpdir and the callback is called with the name as the first argument.
# % tmpDir  Temporary directory used for constructing archives (if legacy delivery)
# RETURNS: $resultcode
# - $resultCode	  Result Code - see constants for definitions
#------------------------------------------------------------------------------
sub parseFiles {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $pmdFileList, $callback, %opts)=@_;
	lock_keys(%opts, qw(pmddelivery tmpDir));

	foreach my $pmdFile (@{$pmdFileList}) {
		my $pmdFileName = $pmdFile->{localfile};
		my ($rcode, $pmdInfo) = $self->parse($pmdFile->{localfile});
		
		if ($rcode != OK) {
			$self->_warn("Failed parsing file $pmdFileName, rcode=$rcode");
			next;
		}
		
		# Validation
		if ($pmdFile->{id} != $pmdInfo->{id}) {
			$self->_warn("File $pmdFileName: id ($pmdInfo->{id}) does not match dumplist id ($pmdFile->{id})");
		}
		if (defined $pmdInfo->{board} && $pmdFile->{board} != $pmdInfo->{board}) {
			$self->_warn("File $pmdFileName: board ($pmdInfo->{board}) does not match dumplist board ($pmdFile->{board})");
		} elsif (! defined $pmdInfo->{board}) {
			# Sometimes PMD does not contain board position
			$pmdInfo->{board} = $pmdFile->{board};
		}
		
		# If we are going to keep the dumps, then
		# now is the time to move it to the output dir
		if ($self->{dumpdir}) {
			my ($pmdBaseName) = ($pmdFileName =~ /([^\/]+)$/);
			unless ($pmdBaseName) {
				$srv->logError("Could not derive PMD base file name for $pmdFileName");
				next;
			}
			
			#pmd file name
			my $destFile;
			my $newPmdName = "$self->{nodename}_$pmdBaseName";
			
			if ($opts{pmddelivery}) {
				$destFile="$self->{dumpdir}/$newPmdName";
			} else {
				# pmd file name with the path
				$destFile="$opts{tmpDir}/$self->{nodename}/pmd/$newPmdName";
			}
			
			$srv->logDebug(5, "Rename PMD $pmdFileName to $destFile") if DEBUG5;
			unless (rename $pmdFileName, $destFile) {
				$srv->logWarn("Problem renaming '$pmdFileName' to '$destFile': $!");
				next;
			}
			
			&$callback($destFile) if $opts{pmddelivery};
			
			# Filename we save is the one that the PMD file will be known as
			# on the server
			$pmdInfo->{file}="$newPmdName";
			
		} else {
			# Save the filename as on the node
			$pmdInfo->{file}=$pmdFile->{file};
		}
		
		my $board=$pmdInfo->{board};
		$srv->logDebug(4, "Storing PMD Info for file $pmdFile->{localfile} board=$board time=$pmdInfo->{time}") if DEBUG4;
		$srv->logDebug(5, "PmdInfo = ".Dumper($pmdInfo)) if DEBUG5;
		
		# An index is kept into restarts occurring on each board.
		if (exists $self->{btindex}{$board}) {
			push @{$self->{btindex}{$board}}, [ $pmdInfo->{time}, $pmdInfo];
		} else {
			$self->{btindex}{$board} = [ [ $pmdInfo->{time}, $pmdInfo] ] ;
		}

	}
	
	if ($self->{dumpdir} && ! $opts{pmddelivery}) {
		# zip all the files for transfer.
		# use the command line to avoid dependencies on perl libraries
		#prepare a pmd file suffix.
		my ($ss,$mm,$hh,$dd,$mo,$yy)=localtime(time);
		my $zipfileName = sprintf("%s/%s_%04d%02d%02d_%02d%02d.pmd.zip",$self->{dumpdir},$self->{nodename},$yy+1900,$mo+1,$dd,$hh,$mm,$ss);
		
		$srv->logDebug(1, "zip file name for the pmd : $zipfileName ");
		my $dirFile = $opts{tmpDir} . "/" . $self->{nodename} . "/pmd/*";
		my $cmd = "zip -j -q $zipfileName $dirFile";
		$srv->logDebug(1, " about to execute $cmd ");
		my $ret = system ($cmd);
				
		if ($ret == -1) {
			$srv->logError("Could not zip pmd file for ".$self->{nodename}." with ".$zipfileName . " in dir ". $dirFile);
		} else {
			# remove the directory
			system ( "rm -rf $dirFile" );
		}
	}
		
	return OK;
}

##-----------------------------------------------------------------------------
# addDetailsToRecord($rec)
# Adds details to the restart record supplied, assuming it matches a PMD
# - $rec	An instance of Itk::EventData::EventRec::Restart
# RETURNS: $result
# - $result	1 if the PMD was found and the record updated, undef otherwise
#------------------------------------------------------------------------------
sub addDetailsToRecord {
	$srv->logEnter(\@_) if ENTER;
	my ($self, $rec)=@_;
	my ($board,$pmdid,$etime)=$rec->get(qw(slot pmdid time));
	
	my $pmd=$self->findPmdByTimeAndAddress($board,$etime,$pmdid);
	
	
	
	unless ($pmd) {
		$srv->logDebug(4,"Not able to find matching pmd for board=$board, id=$pmdid, time=$etime") if DEBUG4;
		return undef;
	}
	
	$srv->logDebug(5,"Found PMD with id $pmd->{id} time=$pmd->{time} board=$pmd->{board}") if DEBUG5;
	
	if (abs($etime-$pmd->{'time'}) > PMD_TIME_MISMATCH_THRESH) {
		$srv->logDebug(4,"Mismatch in times for board=$board, id=$pmdid: pmd time='$pmd->{time}', envelope time='$etime'");
	}
	
	$rec->set(
		pmdid => $pmd->{file},
		errorno => $pmd->{errorno},
		extra => $pmd->{extradata},
		process => $pmd->{process},
		block => $pmd->{block},
		stacktrace => $pmd->{stacktrace},
		productnumber =>   $pmd->{productnumber},
		productrevision => $pmd->{productrevision},
		productdate =>     $pmd->{productdate},
		serialnumber =>    $pmd->{serialnumber},
		piutype => $pmd->{productname},
		infotext => $pmd->{errordesc},
		capsule => $pmd->{capsule},
	);
	
	$rec->set(scope => 'SPM') if $pmd->{scope} eq 'SPM'; 
	
	return 1;
}
##-----------------------------------------------------------------------------
# findPmdByTimeAndAddress($board,$time, $pmdid)
# Find the PMD Id associated with a restart on a particular board
# - $board	Linkhandler of board, eg. 001000
# - $time	Epoch time at which the restart occurred
# - $pmdid  pmdid
# RETURNS: $pmdInfo
# - $pmdInfo	Ref to a hash. See parse() for a description of the fields.
#		Undef if none found.
#------------------------------------------------------------------------------
sub findPmdByTimeAndAddress {
	my ($self, $board, $time, $pmdid)=@_;
	
	$srv->logDebug(6, "Search for restart on board $board \@ time $time with pmdid $pmdid ") if DEBUG6;
	unless (exists $self->{btindex}{$board} && ref $self->{btindex}{$board} eq 'ARRAY') {
		$srv->logDebug(5,"No PMDs found for board $board") if DEBUG5;
		return undef;
	}
	
	# {btindex}{$board} contains an array of entries [ $ts, $pmdInfo ] where $ts is the time that the restart occurred
	# Note that the array is not in sorted order
	my ($minDiff, $selPmd, $selInd);
	my $ind=-1;
	foreach my $restart (@{$self->{btindex}{$board}}) {
		$ind++;
		#next if $restart->[0] > $time;
		my $tDiff = abs($time-$restart->[0]);
		$srv->logDebug(6,"Checking restart \@ time $restart->[0] (pmdid=$restart->[1]->{id}) with search time $time: Diff = $tDiff") if DEBUG6; 
		next if (defined $minDiff and $minDiff<$tDiff);
		next if ($pmdid ne $restart->[1]->{id});
		$minDiff=$tDiff;
		$selPmd=$restart->[1];
		$selInd=$ind;
	}
	
	if ((defined $minDiff and $minDiff < MIN_PMD_MATCH_TIME)) {
		$srv->logDebug(6, "Found restart \@ time $selPmd->{time}  $pmdid and " .$selPmd->{id}) if DEBUG6;
		# This record is now deleted from available PMDs index so it cannot be accidentally referenced a second time
		# also speeds up subsequent searches
		splice @{$self->{btindex}{$board}}, $selInd, 1;
		return $selPmd;
	} else {
		$srv->logDebug(5,"Could not find PMD ID corresponding to $board \@ $time") if DEBUG5;
		return undef;
	}
}

##-----------------------------------------------------------------------------
# reportUnusedPmds()
# Reports all PMDs that were parsed but not actually used. This is an indication
# of problems since it means the corresponding PMD was not found in the syslog
#------------------------------------------------------------------------------
sub reportUnusedPmds {
	$srv->logEnter(\@_) if ENTER;
	my ($self)=@_;
	foreach my $board (keys %{$self->{btindex}}) {
		foreach my $pmdEntry (@{$self->{btindex}{$board}}) {
			my $pmd=$pmdEntry->[1];
			$self->_warn("PMD: id=$pmd->{id} board=$pmd->{board} time=$pmd->{time} was found but never used!");
		}
	}
}

##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	# Do destruction!!
}
#------------------------------------------------------------------------------
1; # Modules always return 1!

