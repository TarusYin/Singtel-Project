#!/usr/bin/perl
###----------------------------------------------------------------------------
# Persistant storage for event collection status information
#
# (c) Ericsson AB 2007 - All Rights Reserved
#
# No part of this material may be reproduced in any form
# without the written permission of the copyright owner.
# The contents are subject to revision without notice due 
# to continued progress in methodology, design and manufacturing. 
# Ericsson shall have no liability for any error or damage of any
# kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of
# Telefonaktiebolaget LM Ericsson. All other trademarks mentioned
# herein are the property of their respective owners. 
# 
#------------------------------------------------------------------------------
#
# Author: eharmic
# $Id: CollectionHistory.pm 1850 2009-01-06 22:02:11Z eanzmark $

package Itk::CollectionHistory;
use strict;
use Hash::Util qw(lock_keys);
use Fcntl ':flock';
use DB_File;
use Storable qw(thaw freeze);
# NB: Do not add dependancy to Itk::Server ... even though perl -c fails
# without it ... until the old NITS pack is dead and buried
BEGIN {
	if (DEBUG6) {
		require Data::Dumper;
		import Data::Dumper;
	}
}

#==============================================================================
# CONSTANTS
#==============================================================================
#use constant (
#	DBNAME => 'event.db',	# BDB file used to store history data
#);
#==============================================================================
# INSTANCE VARIABLES
#==============================================================================
use fields (
	'node',		# Node that this instance is for
	'dbfile',	# Database file
	'data',		# Ref to in memory data store
	'dirty',	# Flag: is write needed?
);
#==============================================================================
# CLASS VARIABLES
#==============================================================================
our $srv;  #reference to global server object
*srv=\$main::srv;
my $numInstances=0;
#==============================================================================
# CLASS METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# getDbNodes()
# Get the node names present in the DB
# % storedir	Local Storage Directory
# % dbfilename		DB Filename - either absolute or relative to storedir
# RETURNS: $listRef
# - $listRef  Ref to list of nodes in database  
#------------------------------------------------------------------------------
sub getDbNodes {
	$srv->logEnter(\@_) if ENTER;
	my (%opts)=@_;

	my $dbfile;
	if (! $opts{storedir}) {
		$dbfile=$opts{dbfilename};
	} else {
		$dbfile=$opts{storedir}."/".$opts{dbfilename};
	}
	die "DB '$dbfile' not accessible\n" unless -f $dbfile;

	# Get a shared lock on the lockfile
	my $lockfile="$dbfile.lock";
	# First ensure that it actually exists
	unless (-e $lockfile) {
		open LOCK, ">$lockfile" or die "Cannot create $lockfile : $!";
		close LOCK;
	}
	
	$srv->logDebug(5,"Waiting for shared lock on $lockfile") if DEBUG5;
	open LOCK, "<$lockfile" or die "Cannot open $lockfile : $!";
	unless (flock LOCK, LOCK_SH) {
		die "Timed out trying to get a lock on $lockfile\n";
	}
	$srv->logDebug(5,"Obtained shared lock on $lockfile") if DEBUG5;
	
	my %db;
	tie %db, "DB_File", $dbfile;
	my @nodes = keys %db;
	untie %db;
	
	flock LOCK, LOCK_UN;
	close LOCK;
	$srv->logDebug(5,"Released shared lock on $lockfile") if DEBUG5;

	return \@nodes;	
}

#==============================================================================
# PRIVATE METHODS
#==============================================================================
##-----------------------------------------------------------------------------
# _warn($msg)
# Issue warning, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _warn {
	$srv->logWarn("($_[0]->{node}) $_[1]");
}

##-----------------------------------------------------------------------------
# _error($msg)
# Issue Error, prepending the current node name
# - $msg Warning Message
#------------------------------------------------------------------------------
sub _error {
	$srv->logError("($_[0]->{node}) $_[1]");
}

##-----------------------------------------------------------------------------
# _readDbData()
# Read the data packet from the database corresponding to the current node
#------------------------------------------------------------------------------
sub _readDbData {
	$srv->logEnter(\@_) if ENTER;
	my ($self)=@_;
	
	# Get a shared lock on the lockfile
	my $lockfile="$self->{dbfile}.lock";
	# First ensure that it actually exists
	unless (-e $lockfile) {
		open LOCK, ">$lockfile" or die "Cannot create $lockfile : $!";
		close LOCK;
	}
	
	$srv->logDebug(5,"Waiting for shared lock on $lockfile") if DEBUG5;
	open LOCK, "<$lockfile" or die "Cannot open $lockfile : $!";
	unless (flock LOCK, LOCK_SH) {
		die "Timed out trying to get a lock on $lockfile\n";
	}
	$srv->logDebug(5,"Obtained shared lock on $lockfile") if DEBUG5;
	
	my %db;
	tie %db, "DB_File", $self->{dbfile};
	my $dataRec = $db{$self->{node}};
	$self->{data}=thaw($dataRec);
	untie %db;
	
	flock LOCK, LOCK_UN;
	close LOCK;
	$srv->logDebug(5,"Released shared lock on $lockfile") if DEBUG5;
	
	$self->{data}={} unless (defined $self->{data});
	$self->{dirty}=0;
}


#==============================================================================
# PUBLIC METHODS
#==============================================================================

##-----------------------------------------------------------------------------
# new(%opts)
# Constructor
# % node 	Node name that this instance is storing the history of
# % storedir	Local Storage Directory
# % dbfilename		DB Filename - either absolute or relative to storedir
#------------------------------------------------------------------------------
sub new {
	$srv->logEnter(\@_) if ENTER;
	my ($self, %opts)=@_;
	lock_keys(%opts,qw(node storedir dbfilename));
	unless (ref $self) {
		$self=fields::new($self);
		if (DEBUG9) {
			$numInstances++;
			my @caller = caller(1);
			my $caller="$caller[3] ($caller[1]:$caller[2])";
			$srv->logDebug(9,"NEW $self created by $caller : n=$numInstances");
		}
	}

	$self->{node}=$opts{node} or die "No node supplied";
	if (! $opts{storedir}) {
		$self->{dbfile}=$opts{dbfilename};
	} else {
		$self->{dbfile}=$opts{storedir}."/".$opts{dbfilename};
	}
	$srv->logDebug(4,"Created instance for node '$self->{node}', db='$self->{dbfile}'") if DEBUG4;
	$self->_readDbData();
	return $self;
}
##-----------------------------------------------------------------------------
# put($name, $value, [$classname])
# Stores a value in the collection history. Note that values are stored in a
# namespace that is private to each calling module.
# - $name	Name of the data being stored
# - $value	The actual value (scalar or ref to data structure)
# - $classname	Name of the class that stores the data. If not supplied, assumes
#		the calling class.
#------------------------------------------------------------------------------
sub put {
	$srv->logEnter(\@_) if ENTER;
	my ($self,$name,$value, $package)=@_;
	$package ||=(caller())[0];
	$srv->logDebug(4, "Put value '$value' into '$name' for package '$package'") if DEBUG4;
	$srv->logDebug(6, "Value is: ".Dumper(\$value)) if DEBUG6;
	$self->{data}{$package}{$name}=$value;
	$self->{dirty}=1;
}

##-----------------------------------------------------------------------------
# get($name, [$classname])
# Retrieves a value in the collection history. Note that values are stored in a
# namespace that is private to each calling module.
# - $name	Name of the data being stored
# - $classname	Name of the class that stored the data. If not supplied, assumes
#		the calling class.
# RETURNS: $value
# - $value	The actual value (scalar or ref to data structure)
#------------------------------------------------------------------------------
sub get {
	$srv->logEnter(\@_) if ENTER;
	my ($self,$name, $package)=@_;
	$package ||=(caller())[0];
	my $value;
	if (exists $self->{data}{$package} && exists $self->{data}{$package}{$name}) {
		$value = $self->{data}{$package}{$name};
	} else {
		$value = undef;
	}
	$srv->logDebug(4, "Got value '$value' from '$name' for package '$package'") if DEBUG4;
	$srv->logDebug(6, "Value is: ".Dumper($value)) if DEBUG6;
	return $value;
}
##-----------------------------------------------------------------------------
# getAllKeys($classname)
# Returns a list of all the keys in this object for the given class
# - $classname	Name of the class owning the keys to be returned
# RETURNS: @list
# - @list	List of the keys
#------------------------------------------------------------------------------
sub getAllKeys {
	$srv->logEnter(\@_) if ENTER;
	my ($self,$classname)=@_;
	return keys %{$self->{data}{$classname}};
}

##-----------------------------------------------------------------------------
# getAllClasses()
# Returns a list of all the object classses that have stored data in this object 
# RETURNS: @list
# - @list	List of the classes
#------------------------------------------------------------------------------
sub getAllClasses {
	$srv->logEnter(\@_) if ENTER;
	my ($self,$classname)=@_;
	return keys %{$self->{data}};
}
##-----------------------------------------------------------------------------
# rm($name, [$classname])
# Deletes a key
# - $name	Name of the data being stored
# - $classname	Name of the class that stored the data. If not supplied, assumes
#		the calling class.
#------------------------------------------------------------------------------
sub rm {
	$srv->logEnter(\@_) if ENTER;
	my ($self,$name, $package)=@_;
	$package ||=(caller())[0];
	delete $self->{data}{$package}{$name};
	$self->{dirty}=1;
}
##-----------------------------------------------------------------------------
# rmclass($classname)
# Deletes all keys associated with a class
# - $classname	Name of the class that stored the data. If not supplied, assumes
#		the calling class.
#------------------------------------------------------------------------------
sub rmclass {
	$srv->logEnter(\@_) if ENTER;
	my ($self,$package)=@_;
	$package ||=(caller())[0];
	delete $self->{data}{$package};
	$self->{dirty}=1;
}

##-----------------------------------------------------------------------------
# commit()
# Writes all changed data to the database
#------------------------------------------------------------------------------
sub commit {
	$srv->logEnter(\@_) if ENTER;
	my ($self)=@_;
	
	# Do not do anything unless there has been a change
	return unless $self->{dirty};
	
	# Get an exclusive lock on the lockfile
	my $lockfile="$self->{dbfile}.lock";
	
	$srv->logDebug(5,"Waiting for exclusive lock on $lockfile") if DEBUG5;
	open LOCK, ">$lockfile" or die "Cannot open $lockfile : $!";
	unless (flock LOCK, LOCK_EX) {
		die "Timed out trying to get a lock on $lockfile\n";
	}
	$srv->logDebug(5,"Obtained exclusive lock on $lockfile") if DEBUG5;
	
	my %db;
	tie %db, "DB_File", $self->{dbfile};
	my $dataRec = freeze($self->{data});
	$db{$self->{node}} = $dataRec;
	untie %db;
	
	$srv->logDebug(5,"Released exclusive lock on $lockfile") if DEBUG5;
	flock LOCK, LOCK_UN;
	close LOCK;
	
	$self->{dirty}=0;
	$srv->logDebug(4,"Committed data for node '$self->{node}', db='$self->{dbfile}'") if DEBUG4;
}

##-----------------------------------------------------------------------------
# DESTROY()
# Destructor
#------------------------------------------------------------------------------
sub DESTROY {
	$srv->logEnter(\@_) if ENTER;
	my $self=shift;
	if ($self->{dirty}) {
		$self->_warn("DESTROY called with uncommitted data");
	}
	if (DEBUG9) {
		$numInstances--;
		$srv->logDebug(9, "DESTROY $self : n=$numInstances");
	}	
}
#------------------------------------------------------------------------------
1; # Modules always return 1!


