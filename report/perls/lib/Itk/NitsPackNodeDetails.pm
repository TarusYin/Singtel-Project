#!/usr/bin/perl 
###-----------------------------------------------------------------------------------------------
# This module implements the NITS_PACK NodeDetails interface.  It provides subroutines to generate
# details for logging into CPP based nodes and also transforming configuration file nodenames 
# into ITK nodenames.
# 
# (c) Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form without the written permission of the 
# copyright owner. The contents are subject to revision without notice due to continued progress
# in methodology, design and manufacturing. Ericsson shall have no liability for any error or 
# damage of any kind resulting from the use of these documents.
#
# Ericsson is the trademark or registered trademark of Telefonaktiebolaget LM  Ericsson. All other
# trademarks mentioned herein are the property of their respective owners. 
#--------------------------------------------------------------------------------------------------
#
# Author: Paul Stewart, userid: epauste, email: paul.s.stewart@ericsson.com
# $Id: NitsPackNodeDetails.pm 1850 2009-01-06 22:02:11Z eanzmark $

package Itk::NitsPackNodeDetails;
use strict;
use Hash::Util qw(lock_keys);
use Itk::NitsPackServer;

#---------------------------------------------
# crete a reference to a global server object
#----------------------------------------------
our $srv;
*srv = \$main::srv;

##--------------------------------------------------------------------------------------------------
# getNodeDetails()
# This subroutine is for use on an NITS_PACK system only. It will read the ipdatabase, sitefilelists 
# and any nodename transformation subroutines from the NITS_PACK config file.
# When called, it will return a record consisting of the following node details 
# 	ipdatabasename_nodename => 
#			  	   username     => node username
#				   passwd       => node password
#                                  ip_address   => node ip address
#                                  itk_nodename => ipdatabase_nodename_transformed
# % ip_database		Filename of ipdatabase
# % sitefile		Either a single Site File or a ref to an array of site files
# % nodename_transform	Node Name Transform sub ref
# RETURNS: 		\%nodeNameToFdnHashRef,		
#-----------------------------------------------------------------------------------------------------
sub getNodeDetails {
	$srv->logEnter(\@_) if DEBUG;
	my %params = @_;
	lock_keys(%params, qw/ip_database sitefile nodename_transform/);

	#---------------------------------------------------------------------
	# Check there is a valid entry in the config file for the IP database
	#---------------------------------------------------------------------
	my $ip_database = $params{ip_database};
	die "getNodeDetails >> IP Database filename not supplied, Aborting" 
		unless defined $ip_database;		
	#------------------------------------------------------------------------
	# Check there is a valid entry in the config file for the sitefile Lists
	#------------------------------------------------------------------------
	my $sitefiles  = $params{sitefile};

	#--------------------------------------------------------------------------------------------------------------------------
	# Some networks may require that the nodename in the ipdatabase is transformed before any logs are passed back to the EITS.
	# if this is the case, a subroutine will be present in the config file informing how the translation will occur
	#--------------------------------------------------------------------------------------------------------------------------	
	my $nodenameTransformer =$params{nodename_transform};

	$srv->logDebug(1,"getNodeDetails >> received the following input files \n\t ip_database : '$ip_database'\n\t sitefile(s) : '$sitefiles'") if DEBUG1;
	#-----------------------------------------------------------------------------------------
	# Open the ipdatabase file on the nits_pack server
	#-----------------------------------------------------------------------------------------
	
	my $ipDatabaseHRef 	 = _readIpDatabaseFile( ip_database => $ip_database );
	my $detailedNodeListHRef = _readSiteListsAndConstuctNodeDetails (ip_database => $ipDatabaseHRef, site_files => $sitefiles, name_transform_subroutine => $nodenameTransformer );
	return $detailedNodeListHRef;
}

##-------------------------------------------------------------------------------------------------------
# _readIpDatabaseFile(%params)
# This private subroutine is for use on an NITS_PACK system only. It will read the ipdatabase and create 
# a record structure consisting of the following data
# When called, it will return a record consisting of the following node details 
# 	ipdatabasename_nodename => [ip_address,  passwd ]
#
# - %params		Hash containg the following name => values pairs
# 	* ip_database	The address of the file received from the NITS_PACK config file.
#
# RETURNS: 		\%ipDatabaseH,		
# - \%ipDatabaseH 	Reference to hash of arrays, with ip_database nodename as the key pointing to an 
# array consisting of ipaddress and password
#---------------------------------------------------------------------------------------------------------
sub _readIpDatabaseFile {
	my %params = @_;
	lock_keys(%params, qw/ip_database/);
	my %ipDatabaseH;
	
	#---------------------------------------------
	# Open the ipdatabase file or die immediately
	#---------------------------------------------
	unless (open(IPDBFH, $params{ip_database})) {
		die "getNodeDetails >> Could not open IP DB File '$params{ip_database}' on standalone NITS Pack, \n\t Error Message:  $! \n\n";
	}
	#----------------------------------------------------------------------------------------------------------------
	# Loop through the ipdatabase file storing the username, password and ip address of each node into a hash record
	#----------------------------------------------------------------------------------------------------------------
	while (<IPDBFH>) {
		chomp;
		next if (/^#/ || /^\s*$/);		# Ignore lines begining with hashes (comments) or empty line
		my ($node,$ipOrHost,$pw) = split /\s+/;
		if (!defined $node || !defined $ipOrHost || !defined $pw ) {
			$srv->logError("getNodeDetails >> Incorrect definition in ip_database file '$params{ip_database}' at line $., skipping this defintion");
			next;
		}
		# FIX ME: By spliting on \s+, if there is a space after $ipOrHost and $pw is not set in the file, then the variable $pw will be defined but an empty string!!!
		push @{$ipDatabaseH{$node}},($ipOrHost,$pw);
	}
	close IPDBFH or $srv->logWarn("getNodeDetails >> Error attempting to close received ip_database file '$params{ip_database}, \n\t ERROR: $!");
	#$srv->logDebug(3,"_readIpDatabaseFile >> Constructed the following IP Database file\n" . Dumper(\%ipDatabaseH)) if DEBUG3;
	return 	\%ipDatabaseH;
}	

##-------------------------------------------------------------------------------------------------------
# _readSiteListsAndConstuctNodeDetails(%params)
# This private subroutine is for use on an NITS_PACK system only. 
# It will read the sitefiles specified in the NITS_PACK config file and create 
# a record structure consisting of the following data
#	ipdatabasename_nodename => 
#			  	   username     => node username
#				   passwd       => node password
#                                  ip_address   => node ip address
#                                  itk_nodename => ipdatabase_nodename_transformed
#
# - %params			Hash containg the following name => values pairs
# 	* ip_database		The location of the file received from the NITS_PACK config file.
#	* site_files    	The list of sitefiles (locations) received from the NITS_PACK config file.
#	* name_transform_subroutine
# 				Optional paramater. A subroutine reference obtained from the NITS_PACK config,
# 				to be used for transforming ipdatabase nodenames to ITK expected nodenames.
# RETURNS: 		\%nodeListDetails,		
#---------------------------------------------------------------------------------------------------------
sub _readSiteListsAndConstuctNodeDetails {	
	my %params = @_;
	lock_keys(%params, qw/ip_database site_files name_transform_subroutine/);
	my $ipdatabase = $params{ip_database};
	
	my @sitefileList;
	if (ref $params{site_files} eq 'ARRAY') {
		@sitefileList = @{$params{site_files}};
	} elsif (defined $params{site_files}) {
		@sitefileList = ( $params{site_files} );
	}
	
	#-------------------------------------------------------------------------------------
	# Open each site file name and build a unique list of nodes that have been requested
	#-------------------------------------------------------------------------------------
	my (%selectedSites, %nodeListDetails);
	foreach my $sitefile (@sitefileList) {
		open(SITEFH, $sitefile) or 
			die "getNodeDetails >> Could not open SiteFile '$params{site_files}', on standalone NITS Pack, \n\t Error Message: $!\n";
		
		while (<SITEFH>) {
			chomp;
			next if (/^#/);		# Ignore lines begginning with hashes (comments)
			if (/^\s*(\S+)\s*$/) {  # Ignore any preceeding or trailing spaces in sitefile
				$selectedSites{$1}=1;
			}
		}

		close SITEFH or $srv->logWarn("getNodeDetails >> Error attempting to close received sitefile file '$params{site_files}, \n\t ERROR: $!");	
	}

	
	#-------------------------------------------------------------------------------------
	# Create HoH of details for all selected nodes. If no sitefile then all sites in ipdatabase
	# are included.
	# i.e: username,password,ip_address,nodename_transforms,prefixs
	#-------------------------------------------------------------------------------------
	foreach my $siteNode (@sitefileList ? keys %selectedSites : keys %{$ipdatabase}) {

		#-----------------------------------------------------------------------------------------------------------
		# If the node is defined in the ip_database and not yet defined in the nidelist details hash, then store it
		#-----------------------------------------------------------------------------------------------------------
		if (defined $params{ip_database}->{$siteNode}) {
			if (! exists $nodeListDetails{$siteNode}) {
				$nodeListDetails{$siteNode}{username}     = 'itk';
				$nodeListDetails{$siteNode}{ip_address}   = $ipdatabase->{$siteNode}[0];
				$nodeListDetails{$siteNode}{passwd}       = $ipdatabase->{$siteNode}[1];
				#----------------------------------------------------------------------------------------------
				# If neccessary for this network, then transform the ipdatabase name to an ITK understood name
				#----------------------------------------------------------------------------------------------
				if (defined $params{name_transform_subroutine}) {
					$nodeListDetails{$siteNode}{itk_nodename} = &{$params{name_transform_subroutine}}($siteNode);
				} else {
					$nodeListDetails{$siteNode}{itk_nodename} = $siteNode;
				}
			}
			next;
		} else {
			$srv->logWarn("main >> Node '$siteNode' does not exist in the IP Database file, continuing with next node in sitefile");
			next;
		}
	}

	#$srv->logDebug(3,"_readSiteListsAndConstuctNodeDetails >> Constructed the following Unique Site List Node Details\n" . Dumper(\%nodeListDetails)) if DEBUG3;

	return \%nodeListDetails;
}
1
