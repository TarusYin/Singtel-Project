#!/usr/bin/perl
#------------------------------------------------------------------------------
#
# xmlcollect : fetches xml files from cello nodes and stores in a spool directory
#
# © Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form without the written
# permission of the copyright owner. The contents are subject to revision without
# notice due to continued progress in methodology, design and manufacturing.
# Ericsson shall have no liability for any error or damage of any kind resulting
# from the use of these documents.
#
# Ericsson is the trademark or registered trademark of Telefonaktiebolaget LM
# Ericsson. All other trademarks mentioned herein are the property of their
# respective owners.
#------------------------------------------------------------------------------
# EPA/SR/G/R/U M. Harris - GSDC Australia
# $Id: xmlcollect 4902 2011-06-14 05:06:56Z eharmic $

use strict;
use FindBin qw($Bin);
use lib "$Bin/../lib/";
use Net::FTP;
use Net::Ping;
use Getopt::Long;
use File::Path;
use File::Temp qw(tempdir);
use Storable;
use Time::Local;
use Itk::NitsPackServer;
use Itk::FileUtils;
use Parallel::ForkManager;

our $srv;
$srv = new Itk::NitsPackServer(config => "$Bin/../etc/itk.conf");

#------------------------------------------------------------------------------
# Configuration
#------------------------------------------------------------------------------

# Maximum number of files to allow in temp directory (per node).
# Once this limit is reached the oldest ones will be deleted
# Default is 7 days x 96 files
my $maxfiles = 672;

# Spool Directory
my $spooldir = "$ENV{HOME}/spool/pmxml";

# Default Paths to try on node for PM files
my @pmpaths=('/p001200/pm', '/p001300/pm', '/c/public_html/cello/XML_files', '/p001200/pm_data', '/p001300/pm_data', '/c/pm_data');

# Default file match
my $filematch='.xml.gz$';

our ($debug,$debug2);
my ($sitefile, 
	$ipdatabase, 
	$help, 
	$dontfetch, 
	$handler, 
	$maxProc,	# Maximum # of parallel processes
	$jobName,	# Name of lockfile
	$lockfile,	# Locking file used to prevent multiple executions
    $diskSpaceFree,  # disk space free on disk.	
);

GetOptions(
        'h|help' 		=> \$help,
	'i=s'			=> \$ipdatabase,
	's=s'    		=> \$sitefile,
	'd=s'			=> \$spooldir,
	'm=s'			=> \@pmpaths,
	'f=s'			=> \$filematch,
	'g'			=> \$debug,
	'gg'			=> \$debug2,
	'x'			=> \$dontfetch,
	'maxfiles=s' 		=> \$maxfiles,
	'handler=s' 		=> \$handler,
	'maxproc=s'		=> \$maxProc,
	'j|job=s'		=> \$jobName,
	'diskspace=s' => \$diskSpaceFree,
) or usage();



$debug=1 if $debug2;

@pmpaths = split(/,/,join(',',@pmpaths));
print "Trying pmpaths: @pmpaths\n" if $debug;

$maxProc ||= 50;
$maxProc ||= 50;

$diskSpaceFree ||= 20;

usage() if $help;


$ipdatabase ||= find_ipdatabase() or die "No ipdatabase specified and could not find one\n";
print "Using ipdatabase: $ipdatabase\n" if $debug;

print "Loading handler file: $handler\n" if $debug;
if ($handler) {
	unless (my $return = do $handler) {
		    die "Couldn't compile $handler: $@\n" if $@;
		    die "Couldn't read $handler: $!\n" unless defined $return;
		    warn "Initialisation of '$handler' was not successful\nReminder: Modules need to return true!\n" unless $return;
	}
}

# obtain a lock to ensure that no other copy is running under this job name
if ($jobName) {
  $lockfile="/tmp/itklock-eventcollect-$jobName";
  open LOCK, ">$lockfile" or die "Cannot open $lockfile : $!";
  unless (flock LOCK, 6) {
  	print "Another instance of xmlcollect is already running. This instance will abort";
  	die "Unable to get lock: program aborted\n";
  }
}

#------------------------------------------------------------------------------
# Globals
#------------------------------------------------------------------------------

# Where errors will be logged
my $errorlog="$spooldir/errorlog.txt";

# A pinger so we can check if nodes are alive
my $pinger = new Net::Ping;

# List of RNCs from which the files will be fetched
my %nodelist;

# Months - used when comparing file timestamps
my %months=(Jan=>0, Feb=>1, Mar=>2, Apr=>3, May=>4, Jun=>5, Jul=>6, Aug=>7, Sep=>8, Oct=>9, Nov=>10, Dec=>11);

my $current_yr = (gmtime(time))[5]+1900;

# Compile file match expression
$filematch=qr/$filematch/ if $filematch;

#------------------------------------------------------------------------------
# usage
#------------------------------------------------------------------------------
sub usage {
	$"=',';
	print STDERR <<_EOF_;

xmlcollect [-h] [-g] [-i <ipdatabase>] [-s <sitefile>] [-d <spooldir>] 
           [-m <pmpath>] [-maxfiles <num>] [-f <regex>] [-x] [-handler <file>]
           [-maxproc <name>] [-job <jobname>]

Fetch XML files from cello nodes and store in a spool directory, where
they can be picked up by a remote host.

Each time the tool is run, it will check all available files on each node
and fetch those that have not been fetched previously.

-h       : Show help
-g       : Debug mode - shows parallel collection attempts
-gg      : Debug mode - more verbose
-i       : Specify IP database (if not specified, then it will be
           autodetected from moshell in the path)
-s       : Specify sitefile. If not specified then it is assumed that XML
           files are to be fetched from *all* nodes in the ipdatabase
-d       : Spool directory (default: $spooldir)
-m       : PM dirs to search on nodes, comma separated list
           (default: @pmpaths)
-f       : If set, any PM files on the node will only be fetched if they match
           this regular expression.
           (default: $filematch)
-maxfiles: Specify the max number of files that will be kept in the spool dir
           per node. After this limit is reached, the oldest files will be
	   deleted. (default: $maxfiles)
-x       : Don't fetch any files. Mark all files on the node as already fetched
           This can be used the first time you run xmlcollect, so that only
	   files fetched from this point in time on will be fetched.
-handler : Load the specified file, which is interpreted as perl code. Within
           this file you may define subroutines that can get called at various
	   points in the process:
	   
	   preFetch() : If it exists, this will be called before any nodes are
	   processed. You can use this to set up anything which needs to be
	   set up prior to fetching any files.
	   
	   postFileFetch(\$node, \$file) : If it exists, this will be called 
	   after each file has been fetched. \$node is the Node name, \$file is
	   the path to the file. This sub must return a true value if the file
	   was successfully handled: if not, the file will be re-attempted at the
	   next run.

           postNode(\$node, \$dir) : If it exists, this will be called after all
           the files for a node are fetched. \$node is the Node name, \$dir is
	   the path to the directory in which the files for this node are stored
	   
	   postFetch(\$dir) : If it exists, this will be called after alll nodes
	   have been processed. \$dir is the path to the directory in which all
	   files have been stored.
	   
-maxproc : Maximum number of parallel processes
  	   Default: '$maxProc'
  	   
-j|job   : Job name. If specified then prevents another instance of xmlcollect
	   with the same job name from starting.
	   
-diskspace: 20% is default. Specify the percentage of diskspace available for collection. 
            for example, if the value is 10, the collection will be executed only if you have at least 10%
            available on your disk.
	   
	   
_EOF_
	exit;
}
#------------------------------------------------------------------------------
# find ipdatabase file
# Will check as follows:
# 1. If there is an environment variable called IPDATABASE, then return that
# 2. If there is an environment variable called MOSHELL_HOME, then return
#	MOSHELL_HOME/sitefiles/ipdatabase
# 3. If there is an moshell in the path, then find its dir and return
#	DIR/sitefiles/ipdatabase
#------------------------------------------------------------------------------
sub find_ipdatabase {
	return $ENV{IPDATABASE} if $ENV{IPDATABASE};
	my $moshelldir=$ENV{MOSHELL_HOME} || `dirname \`which moshell\``;
	chomp $moshelldir;
	return undef unless -d $moshelldir;
	my $ipdatabase="$moshelldir/sitefiles/ipdatabase";
	return undef unless -e $ipdatabase;
	return $ipdatabase;
}
#------------------------------------------------------------------------------
# logerror
#------------------------------------------------------------------------------
sub log_error {
        my $msg = shift;
        open LOGFILE, ">>$errorlog" or print STDERR "$msg\nCould not log to logfile: $!\n" and return 0;
        my ($ss,$mm,$hh,$dd,$mo,$yy) = localtime(time());
        printf  LOGFILE "[%04d-%02d-%02d %02d:%02d:%02d] $msg\n", ($yy+1900, $mo+1, $dd, $hh, $mm, $ss);
        printf STDERR "ERROR: $msg\n" if $debug;
        close LOGFILE;
        return 1;
}
#------------------------------------------------------------------------------
# MAIN
#------------------------------------------------------------------------------

# If there is a sitefile, then read it
if ($sitefile) {
	open (SITEFILE, "<$sitefile") or die "Could not open $sitefile: $!\n";
	while (<SITEFILE>) {
		chomp;
		next if (/^\s*(#.+)?$/); # Skip comments & blank lines
		my ($node)=split;
		$nodelist{$node} = undef; # This just creates the key in the hash
	}
	close SITEFILE;
}

open (IPDATABASE, "<$ipdatabase") or die "Could not open $ipdatabase: $!\n";
while (<IPDATABASE>) {
	next if (/^\s*(#.+)?$/); # Skip comments & blank lines
	
	my ($node, $ip, $pwd) = split;
	
	# If we have a sitefile, and this node was not in it, then skip this node
	next if ($sitefile and ! exists $nodelist{$node});
	
	$nodelist{$node} = [$ip, $pwd];	
}

close IPDATABASE;

# Make sure spool directory exists. NB. mkpath dies if it fails
# and will also attempt to create all intermediate dirs
mkpath $spooldir unless -d $spooldir;


my $nftp;
my $filelist;
my $filelist_file;
my $start_time;


my $pm = new Parallel::ForkManager($maxProc); 
my @nodelist = sort keys %nodelist;
my ($nodesStarted,$nodesComplete);
my $totalNodes = scalar @nodelist;

$pm->run_on_start(
	sub { 
		my ($pid,$nodeId)=@_;
		$nodesStarted+=1;
		my $nodesRemaining = $totalNodes-$nodesComplete;
		$start_time = time();
		#print ">>>>> Processing started for '$nodeId', Process ID '$pid' : total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining)\n" if $debug;
		
	}
);

$pm->run_on_finish(
	sub { 
		my ($pid, $exit_code, $nodeId) = @_;
		$nodesStarted-=1;
		$nodesComplete+=1;
		my $nodesRemaining = $totalNodes-$nodesComplete;
		my $runTime = time() - $start_time;
		print ">>>>> Processing ended for '$nodeId', Process ID '$pid' : total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining) : runtime $runTime s\n" if $debug;
	}
);




my $pid;
NODE:
foreach my $node (sort keys %nodelist) {


	# Fork a new process. Parent loops (pid=undef) and child continues
	$pid = $pm->start($node) and next;

    #check the diskSpace 
    my %disk = Itk::FileUtils::getDiskSpace ($spooldir); 
    

    if ( (100 - $disk{precentUsed}) < $diskSpaceFree) {
    	
	    log_error "The File collection is interrepted. The diskspace is already at ". $disk{precentUsed} ;
	    log_error "The free disk space threshold is set at $diskSpaceFree ";
	    log_error "To continue set the free disk space threshold to a lower value or free up some disk space ";

  	    $pm->finish;
    	next;
    }


	if (exists $main::{preFetch}) {
		&{$main::{preFetch}}();
	}

	# Create a temp dir
	my $tmpdir=tempdir(DIR=>$spooldir, CLEANUP => 1);


        my $ip=$nodelist{$node}[0];
        my $passwd=$nodelist{$node}[1];
	
	print "Trying node $node with ip/dns $ip and password $passwd\n" if $debug2;

        # Make sure node's spool directory exists. NB. mkpath dies if it fails
	# and will also attempt to create all intermediate dirs
        mkpath "$spooldir/$node" unless -d "$spooldir/$node";

	# Algorithm:
	# 1) Load previous list of fetched files
	# 2) get list of files on node, filtered according to parameters
	# 3) Remove any entries in list of fetched files that do not exist on the node
	# 4) For each file on node:
	# 	a) Fetch each file into temp dir
	# 	b) Move file into spool dir
	#    	c) Add file to list of files fetched
	#    	d) Save list of files fetched in this session
	
	# List of fetched files is stored in a hash for quick lookup
	# On disk we store it using Storable, also for efficiency
	$filelist_file="$spooldir/filelist_${node}.dat";
	if (-e $filelist_file) {
		$filelist = retrieve($filelist_file);
	} else {
		$filelist={};
	}
	
	# Check if this node is even reachable
	unless ($pinger->ping($ip,2)) {
		log_error("Could not ping $node");
		next NODE;
	}
	
	# Try to fetch the files
	eval {
		unless ($nftp=Net::FTP->new($ip, Timeout => 10 )) {
                log_error "Failed to connect to $node. Try again next time!";
			next NODE;
		}

		unless ($nftp->login('xmlfetch', $passwd)) {
			log_error "Failed to log in to $node. Try again next time!";
			next NODE;
		}
	
		unless ($nftp->binary()) {
			log_error "Could not set binmode on $node!";
			next NODE;
		}
	};
	if ($@) {
		die unless $@ =~ /timeout/i;
		log_error("$node:$@");
		next NODE;
	}		
	
	# Our algorithm for getting the active PM directory is:
	# 1) Get an 'ls' of the parent dir of all the candidates
	# 2) find the entry for the target dir (if not existing then skip)
	# 3) The current dir is the one with the most recent modification time
	# NB. Cello does not support the MDTM command, which would be much easier
	# drwxrwxrwx   0 0          0                512  Nov  4 23:48 pm
	my $latest=0;
	my $pmpath='';
	foreach my $candidate (@pmpaths) {
		print "$node: Trying path: $candidate\n" if $debug2;
		my ($parentdir, $subdir) = ($candidate =~ /^([\w\/]+)\/(\w+)\/?$/);
		my @subdirs;
		eval {
			@subdirs=$nftp->dir($parentdir);
		};
		if ($@) {
			die unless $@ =~ /timeout/i;
			log_error("$node:$@");
		}
		next unless @subdirs; # If parent does not even exist
		my $line=(grep substr($_,61) eq $subdir, @subdirs)[0];
		print "Dir Line: $line\n" if $debug2;
		my $mtime = substr($line, 48, 12);
		print "mtime for $candidate: $mtime\n" if $debug2;
		# Timestamps will match the pattern below if it is recent
		
		my $ts;
		if ($mtime =~ /(\w\w\w)\s+(\d+)\s+(\d+):(\d+)/) {  #format "Oct 22 16:04"
			#print "mtime: $1 $2 $3:$4\n" if $debug2;
			my $dir_year = $current_yr;
			#Handle the specific case of Feb 29 - we don't want to evaluate this for the current
			#year if the file was really from last year (otherwise gmtime will die) (issue #1248)
			if ($1 eq 'Feb' and $2==29) {
				my ($current_day, $current_mon) = (gmtime(time))[3..4];
				#If current day is before Feb 29, dir is from the previous year
				if ($current_mon==0 or ($current_mon==1 and $current_day<29)) {
					$dir_year = $current_yr-1;
				}
			}
			eval { $ts = timegm(0,$4,$3,$2,$months{$1},$dir_year); };
			if ($@) {
				log_error "Could not determine dir timestamp from '$mtime' (using year=$dir_year)";
				next;
			}
			#If this timestamp is > the current time then the date must
			# have been in the previous year... doh!
			# We include a grace period of 12 hours (43200 secs) to allow for servers which
			# are on a different timezone to their nodes
			if ($ts > (time+43200)) {
				eval {
					$ts = timegm(0,$4,$3,$2,$months{$1},$current_yr-1);
				};
				if ($@) {
					log_error "Could not determine dir timestamp from '$mtime' (using year before $current_yr)";
					next;
				}
			}
		} elsif ($mtime =~ /(\w\w\w)\s+(\d+)\s+(\d{4})/) {  #format "May 29 2008"
			#print "mtime: $1 $2 $3\n" if $debug2;
			$ts = timegm(0,0,0,$2,$months{$1},$3);  #use time of "00:00:00" on this date
		} else {
			next;
		}
		if ($ts>$latest) {
			$pmpath=$candidate;
			$latest=$ts;
		}				
	}
	
	unless ($pmpath) {
		log_error "Could not find the pm dir for $node";
		next NODE;
	}
	
	print "$node: Using path: $pmpath\n" if $debug2;
	
	my @nodefiles;
	eval {
		unless ($nftp->cwd("$pmpath")) {
			log_error "Failed to cd to $pmpath on $node";
			next NODE;
		}
	
		# Get list of files in dir
		@nodefiles=$nftp->ls();
	};
	if ($@) {
		die unless $@ =~ /timeout/i;
		log_error("$node:$@");
	}
	
	# No files?
	next NODE unless @nodefiles;
	
	# Clear any files from the fetched file list that are no longer
	# on the node. Otherwise the list will grow forever
	
	foreach my $file (keys %{$filelist}) {
		delete $filelist->{$file} unless grep($_ eq $file, @nodefiles);
	}
	
	FILE:	foreach my $file (@nodefiles) {

		next FILE if ($filematch && $file !~ $filematch);
		
		print "$node: Check if file $file is to be fetched: " if $debug2;
		
		if ($dontfetch) {
			$filelist->{$file}=1;
			print "\033[33m NO \033[0m : it will be flagged as fetched\n" if $debug2;
			next FILE;
		}
		
		# Have we already fetched this one?
		if (exists $filelist->{$file}) {
			print "\033[33m NO \033[0m\n" if $debug2;
			next FILE;
		}
		
		print "\033[33m YES \033[0m\n" if $debug2;

		print "$node: Try to fetch $file from $node:  " if $debug2;
		
		# File is initially fetched into a temporary directory,
		# then moved into the actual destination. This is done so that
		# a file reciever will not get a partially completed file
		# if they happen to run at the same time as this is still
		# running
		
		my $ftpresult;
		eval {
			$ftpresult = ($nftp->get($file, "$tmpdir/$file") && 	
					rename("$tmpdir/$file", "$spooldir/$node/$file"));
		};
		if ($@) {
			die unless $@ =~ /timeout/i;
			log_error("$node:$@");
			last FILE;  #don't bother continuing with other files on the node
		}		

		# Check if file was retrieved. We test return from ftp operation
		unless ($ftpresult) {
			# In case file was partially recieved: delete remnants
			unlink "$tmpdir/$file" if (-e "$spooldir/$node/$file");
			print "\033[31mFAILED\033[0m\n" if $debug2;
		} else {
			print "\033[32m OK \033[0m\n" if $debug2;
			if (exists $main::{postFileFetch}) {
				next unless (&{$main::{postFileFetch}}($node, "$spooldir/$node/$file"));
			}
			# Flag the file as fetched by putting it in the hash
			$filelist->{$file}=1;
			store($filelist, $filelist_file);
		}
	}

	# We do this first, so that the postNode script has a chance to process
	# files before they get scrubbed if there are too many files
	if (exists $main::{postNode}) {
		&{$main::{postNode}}($node, "$spooldir/$node");
	}
	
	# Make sure we have not exceeded the maximum number of files
	my @filelist=sort glob "$spooldir/$node/*.xml.gz";
	while (scalar @filelist > $maxfiles) {
		
		# Since these are sorted, we are getting the oldest file
		# with shift
		my $f = shift @filelist;
		unlink $f;
		
	}
	
	print "$node: ",scalar @filelist," files waiting in the spool dir\n" if $debug2;;

# The block below will be executed even for the cases where next is used to exit
# the loop early
} continue {
	if($pid==0) {
		store($filelist, $filelist_file);
		$nftp->quit() if $nftp;
		$nftp=undef;
		$pm->finish; # perform exit in the child process	
	}
}

$pm->wait_all_children;	# Do not allow the program to terminate until all child processes are finished

if (exists $main::{postFetch}) {
	&{$main::{postFetch}}($spooldir);
}

my $totalRunTime = time() - $^T;
print "\n Processed $totalNodes nodes in $totalRunTime seconds.\n";




# Done. Release Lock
if ($jobName) {
  flock LOCK, 8;
  close LOCK;
  unlink $lockfile;
}

$pinger->close();


