#!/usr/bin/perl 
#------------------------------------------------------------------------------
#
# logcollect : Utility to enable the collection of log files from remote servers
# 	       via sftp. It can be initialised via a crontab so that only new files 
# 	       fetched back at each operation.
# 		 required for ITK Systems
#
# � Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form without the written
# permission of the copyright owner. The contents are subject to revision without
# notice due to continued progress in methodology, design and manufacturing.
# Ericsson shall have no liability for any error or damage of any kind resulting
# from the use of these documents.
#
# Ericsson is the trademark or registered trademark of Telefonaktiebolaget LM
# Ericsson. All other trademarks mentioned herein are the property of their
# respective owners.
#------------------------------------------------------------------------------
# EPA/SR/G/R/U Paul Stewart - GSDC Australia
# $Id: logcollect 4443 2011-02-08 04:09:16Z eanzmark $

use strict;
use FindBin qw($Bin);
use lib "$Bin/../lib";
use Itk::NitsPackServer;
use Getopt::Long;
use File::Path;
use File::Copy;
use Hash::Util qw(lock_keys);
use IO::File;
use Itk::FileTransfer;
use Time::Local;
use Data::Dumper;
#------------------------------------------------------------------------------
# Global Variables
#------------------------------------------------------------------------------
our $srv;
my (
	# Command line options
	$help,		 # Help was requested
	$localDirStore,	 # Directory where the output files will be stored
	$remoteDirStore, # specifies the remote directory log files are stored
	$logmatch,	 # Regex to enable filtering of which logfiles to collect
	$subdirs,	 # List of subdirs to be searched looking for matching logfile (as well as the base directory)
	$transfermode,	 # indicates the form of file transfer to be used (sftp or ftp)
	$startdate,	 # used at inital run to bring back files older than this date
	$remoteServer,	 # remote ip or dns address where the logs are stored
	$username,	 # username to log into the remote server with
	$password,	 # password on the remote server
	$port,		 # remote port to use (defaults to 22)
	$test,		 # flag that will show what files will be downloaded without actually downloading anything
	$jobid,		 # A jobid to uniquely identify the collection
	$keepdir,	 # if set, then files fetched will be kept in subdirs corresponding to dir sturcture from base remote directory
	
);

#------------------------------------------------------------------------------
# MAIN
# Command Line Options
#------------------------------------------------------------------------------

GetOptions(
        'h|help' 		=> \$help,
	'ld|spooldir=s'		=> \$localDirStore,
	'rd|remotedir=s'	=> \$remoteDirStore,
	'logmatch=s'		=> \$logmatch,
	'sub|subdirs=s'		=> \$subdirs,
	'sd|startdate=s'	=> \$startdate,
	'rs|remoteserver=s'	=> \$remoteServer,
	'un|username=s'		=> \$username,
	'pw|password=s'	 	=> \$password,
	'port=s'		=> \$port,
	'test'			=> \$test,
	'jobid=s'		=> \$jobid,
	'keepdir'		=> \$keepdir,
	
) or usage();

usage() if $help;
$srv=new Itk::NitsPackServer();
$srv->logFatal("logcollect >> local directory for storing log files must be specified on the coli (ld|spooldir), aborting") unless $localDirStore;
if ( (defined $startdate and !defined $remoteDirStore) or (!defined $startdate and defined $remoteDirStore) ) {
	$srv->logFatal("logcollect >> remote directory (rd|remotedir) must only be specified at initial run (along with sd|startdate command), aborting");
}
$srv->logFatal("logcollect >> remote server for collecting log files must be specified on the coli (rs|remoteserver), aborting") unless defined $remoteServer;
$srv->logFatal("logcollect >> remote username for collecting log files must be specified on the coli (un|username), aborting") unless defined $username;
$srv->logFatal("logcollect >> remote password for collecting log files must be specified on the coli (pw|password), aborting") unless defined $password;
$srv->logFatal("logcollect >> No Log Match Regular expression supplied on the coli (logmatch), aborting") unless defined $logmatch;
$srv->logFatal("logcollect >> No jobid specified on the coli (jobid), aborting") unless defined $jobid;
$srv->logFatal("logcollect >> Subdirs can only be specified on initial run with startdate, as this ensure history file correctly intitated, aborting") if (defined $subdirs and !defined $startdate);

#-------------------------------------------------------------------------
#
# MAIN
#
#-------------------------------------------------------------------------
$srv->logInfo("logcollect >> Starting operations");

#----------------------------------------------------------------------------------------------------
# Create the local directory to store the logfiles and the history file if it does not already exist
#----------------------------------------------------------------------------------------------------
unless (-d $localDirStore) {
	mkpath($localDirStore)  or die "Could not create local directory '$localDirStore' to store fetched logs, error msg '$!'\n";
}

my (%historyH, %storefile);
my %tmphistoryH = %historyH;

my $collectionHistoryFile = "$localDirStore/.$jobid-history.txt";

#-------------------------------------------------------------------------------------
# If no startdate (i,e: initial run) has been received on the coli and the collection 
# file exists, then this must be a subsequent run, so we load the history file
#-------------------------------------------------------------------------------------
if (-e $collectionHistoryFile and !defined $startdate) {
	unless (open(HISTORY, $collectionHistoryFile)) {
		die "Failed attempting to open collection History file '$collectionHistoryFile', Error Code: $!\n";	
	}
	while (<HISTORY>) {
		chomp;
		my ($remotedirectory, $localdirectory, $last_fetched_epoch_time) = split /;/;
		$historyH{$remotedirectory}  = $last_fetched_epoch_time;
		$storefile{$remotedirectory} = $localdirectory;
	}
	close(HISTORY);
}

$srv->logInfo("logcollect >> History File before new collection attempt\nt" . Dumper(\%historyH));

#--------------------------------------------------------------------------------------
# If it is an initial run and subdirs is specified on the coli, log the subdirectories
#--------------------------------------------------------------------------------------
my @subdirectories = split /\s*,\s*/, $subdirs;
if (defined @subdirectories) {
	$srv->logInfo("logcollect >> initial run. The following subdirectories have been specified on the coli\n\t@subdirectories");
	if (defined $keepdir) {
		#------------------------------------------------------------------------
		# This means that we want to create the dir structure at the remote end
		#------------------------------------------------------------------------
		foreach my $subdirectory (@subdirectories) {
			if (!-d "$localDirStore/$subdirectory") {
				mkpath("$localDirStore/$subdirectory")  or die "Could not create local directory '$localDirStore/$subdirectory' to store fetched logs, error msg '$!'\n";
			}
		}
	}
}

#------------------------------------------
# Attempt to establish a new SFTP instance
#------------------------------------------
my $sftp_dir = $remoteDirStore;
$srv->logInfo("logcollect >> Initiating SFTP session towards server '$remoteServer'");
my $sftpObject = new Itk::SFTP(host => $remoteServer,  port => $port, timeout => '120');
$sftpObject->login("$username","$password");


#----------------------
# Start fetching files
#----------------------
if (defined $startdate) {
	#--------------------------------------------------------------------------
	# initial run, so we just just the base directory supplied and any subdirs
	#--------------------------------------------------------------------------
	$srv->logInfo("logcollect >> startdate received ($startdate), indicating this is an initial run with jobid '$jobid'");
	my $filelist = $sftpObject->dirStat("$sftp_dir");
	fetch_files($filelist,$sftp_dir);
	
} else {
	#-----------------------------------------------------------------------------------------------
	# Subsequent run, so we use the previous history hash to get a list of the directories to query
	#-----------------------------------------------------------------------------------------------
	foreach my $dirs (keys %historyH) {
		$srv->logInfo("logcollect >> Non initial run : get dirstat of directory '$dirs', last file mod time : '$historyH{$dirs}'");
		my $filelist = $sftpObject->dirStat("$dirs");
		fetch_files($filelist,$dirs);
	}	
}

#---------------------------------------------------------------------
# All files should be fetched by now, so just close the sftp instance
#----------------------------------------------------------------------
$sftpObject->quit();


#-----------------------------------------------------------------------------
# Create a Temporary collection file to write to. If successfuly written, we 
# will overwrite the master and keep the temp for redundancey purposes
#-----------------------------------------------------------------------------
unless ($test) {
	my $tmp_collectionHistoryFile = "$collectionHistoryFile.tmp";
	unless (open(NEWHISTORY,"> $tmp_collectionHistoryFile")) {
		$srv->logInfo("logcollect >> Creating new temporary collection history file '$tmp_collectionHistoryFile'");
		die "Failed attempting to open a temporary collection History file '$tmp_collectionHistoryFile', Error Code: $!\n";	
	}
	
	if (defined $startdate) {
		#-----------------------
		# Initial Run
		#-----------------------
		$srv->logInfo("logcollect >> writing initial history file '$tmp_collectionHistoryFile' with contents\n\t" . Dumper(\%historyH));	
		if (defined @subdirectories) {
			foreach my $dir (@subdirectories) {
				my $rd = "$remoteDirStore/$dir";
				my $ld = defined $keepdir ? "$localDirStore/$dir" : $localDirStore;
				$storefile{$rd}=$ld;
			}
		} else {
			$storefile{$remoteDirStore}=$localDirStore;
		}
		foreach my $path (keys %historyH) {
			
			print NEWHISTORY "$path;$storefile{$path};$historyH{$path}\n";
		}
	} else {
		#------------------------
		# Subsequent run
		#------------------------
		
		foreach my $path (keys %tmphistoryH) {
			#--------------------------------------------------------------------
			# update original history hash with updates for specific directories
			#--------------------------------------------------------------------
			$historyH{$path} = $tmphistoryH{$path};
		}
		$srv->logInfo("logcollect >> Generating new temp history file '$tmp_collectionHistoryFile' with contents\n\t" . Dumper(\%historyH));
		foreach my $path (keys %historyH) {
		print NEWHISTORY "$path;$storefile{$path};$historyH{$path}\n";	
		}
	}
	$srv->logInfo("logcollect >> copying '$tmp_collectionHistoryFile' to '$collectionHistoryFile'");
	my $cmd = "cp $tmp_collectionHistoryFile $collectionHistoryFile";
	system("$cmd");

}
$srv->logInfo("logcollect >> Finished operation");

#----------------------------------------------------------------------------------------------------
# Subroutines
#----------------------------------------------------------------------------------------------------
sub fetch_files {
	my ($filelist,$sftp_dir) = @_;
	foreach my $file (@$filelist) {
		#---------------------------------------------------------------------------------
		# Only pick up file names that match the logmatch supplied on the coli, or if it 
		# is a requested subdir, then cd to it and perform the logmatch check
		#----------------------------------------------------------------------------------
		if ($file->{isdir} == '16384' and grep (/^$file->{name}$/, @subdirectories) ) {
			#------------------------------------------------------------------------
			# change to the subdir and get a listing and attempt to match on logfile
			#------------------------------------------------------------------------
			my $local_relative_dir = $file->{name} if defined $keepdir;
			my $subdir_filelist = $sftpObject->dirStat("$sftp_dir/$file->{name}");
			foreach my $subdir_file (@$subdir_filelist) {
				my $matchResult;
				eval { ($matchResult) = ($subdir_file->{name} =~  /$logmatch/)};
				next unless $matchResult;
				if (defined $startdate) {
					initial_startdate($subdir_file,"$sftp_dir/$file->{name}");	
				}
				
			}
		} else {
			#------------------------------------------------------------
			# Perform the match in the base directory that was specified
			#------------------------------------------------------------
			my $matchResult;
			eval { ($matchResult) = ($file->{name} =~  /$logmatch/)};
			next unless $matchResult;
			
			
			#-----------------------------------------------------------------------------
			# if the startdate was specified then we only pull files back that are older
			#-----------------------------------------------------------------------------
			if (defined $startdate) {
				initial_startdate($file,$sftp_dir);		
					
	
			} elsif ( defined $historyH{$sftp_dir} ) {
				next unless $file->{mtime} gt $historyH{$sftp_dir}; 
				#------------------------------------------------------------------------------------------------------------------
				# else if we are using the local history file, then we use the last stored epoc time from there for specified dir
				#------------------------------------------------------------------------------------------------------------------			
				
				if ($test) {
					$srv->logInfo("Fetching of remote file '$sftp_dir/$file->{name}' to local file '$storefile{$sftp_dir}/$file->{name}' testmode only");
					next;
				} else {
					my $fetchResult = $sftpObject->get("$sftp_dir/$file->{name}","$storefile{$sftp_dir}/$file->{name}");
					$srv->logInfo("Fetching of remote file '$sftp_dir/$file->{name}' to local file '$storefile{$sftp_dir}/$file->{name}' returned '$fetchResult'");
				}
				
				#-------------------------------------------------------------
				# update the history hash to reflect the latest file mod time
				#-------------------------------------------------------------
				if ( $file->{mtime} gt $tmphistoryH{$sftp_dir} ) { 
					$tmphistoryH{$sftp_dir} = $file->{mtime};
				}
				
			} else {
				die "Neither a startdate or an existing history file waas found, aborting as this could potentially bring abck a shed load of files";
			}
		}
	}
}

sub initial_startdate {
	my ($file,$sftp_dir) = @_;
	die "startdate must be in the format yyyy-mm-dd or yyyymmdd" unless $startdate =~ /^\d\d\d\d-?\d\d-?\d\d$/;
	my ($yyyy,$mm,$dd) = ($startdate =~ /^(\d\d\d\d)-?(\d\d)-?(\d\d)$/);
	$mm = $mm -1;
	my $startingepoch = timelocal(0,0,0,$dd,$mm,$yyyy);
	
	if ($file->{mtime} > $startingepoch) {
		$historyH{$sftp_dir} = 0 unless defined $historyH{$sftp_dir};
		
		my $localdir;
		if (defined $keepdir) {
			($localdir) = ( $sftp_dir =~ /^.*\/(\S+)$/);
			print "DERIVED local directory to store  $localdir\n";
		}
		
		my $localdir = defined $localdir ?  "$localDirStore/$localdir/$file->{name}" : "$localDirStore/$file->{name}";
		
		if ($test) {
			$srv->logInfo("Fetching of remote file '$sftp_dir/$file->{name}' to local file '$localdir' testmode only");
			return;
		} else {
			my $fetchResult = $sftpObject->get("$sftp_dir/$file->{name}","$localdir");
			$srv->logInfo("Fetching of remote file '$sftp_dir/$file->{name}' to local file '$localdir' returned '$fetchResult'");
		}
		
		#-------------------------------------------------------------
		# update the history hash to reflect the latest file mod time
		#-------------------------------------------------------------
		if ( $file->{mtime} gt $historyH{$sftp_dir} ) { 
			$historyH{$sftp_dir} = $file->{mtime};
		}
	}
}

sub usage {
	print STDERR <<_EOF_;

	logcollect [-h] -ld|spooldir [-rd|remotedir] -logmatch <regex> [-sub|subdirs] [-sd|startdate] -rs|remoteserver
		   -un|username -pw|password [-port <prefix>] -jobid <lockname> [-keepdir] [-test]

	ITK utility to enable the collection of files from a remote server.
	It can automatically determine which files have not previously been fetched, hence only collecting new file
	each times it runs.
	If specified, specific subdirectories of the given base directory will also be searched.
	Files that match the given regular expression will be fetched back to the local server. 
	The same directory structure as found on the remote server can be kept if specified on the command line, 
	otherwise all matching files will be fetched to the given directory on the local server.
		
-h|help			Show Help Menu (This menu)
-ld|spooldir		The Base directory were the fetched files will be stored on the local server
-rd|remotedir		The Base directory were searching for matching logfiles will beging from on the
			remote server
-logmatch		A regular expression. All log files matching the regular expression will be fetched.
-sub|subdirs		A comma seperated list of subdirs to be searched looking for matching logfiles 
			(The base directory will also be searched)
-sd|startdate 		If specified, then only log files that are older than this date will be fetched. This 
			COLI option should be used the first time the logcollection utility is initiated.
-rs|remoteserver	The remote ip or dns address where the logs are stored
-un|username		The username to log into the remote server.
-pw|password		The password on the remote server
-port			The port to use for sftp to the remote server. (defaults to 22)
-jobid			A jobid to uniquely identify the collection. This ensures if multiple logcollection 
			instances are running, they will not overwrite each instances collection histories 
			log files
-keepdir		If set, then files fetched will be kept in subdirs corresponding to dir sturcture from 
			the base remote directory
-test			If set, then the utility will not fetch any files, but instead show what files would have
			been fetched.
			Use this on initial setup to determine correct operation.

Examples
=========
	Initial setup to collect remote log files.
	---------------------------------------------------------------------------	
		The first time the collection utilility is run, we need to set up the logcollection histiory file. 
		The local and remote log directories must be specified.
		The remote host/ip, username and password must be specified (and port if port forwarding is taking
		place)
		The startdate must be specified.
		The logmatch regular expression must be specifed.
		The jobid must be specifed.
		subdirs is optional, but as we have specified it here, then if any directories called logstore1,
		logstore2 or logstore3 exist in the base directory /export/home/eric/remotelogdir, then they will 
		be searched for matching files.
		keepdir is optional. If specified, and if subdirs is specified, then a directory structure similiar
		to that of the remote server will be setup where the correspoding fetched files will be stored.
		The optional test flag (not shown) is recomended during configuration. It will allow you to see what
		directories will be searched and what files will be retreived.		
			
		logcollect -ld /home/itk/mylogdir -rd /export/home/eric/remotelogdir -rs utran01 -un itk -pw itkwho 
	                   -port 999 -startdate 2008-10-20 -logmatch '\.log\.gz' -jobid fetchlogs  
			   -subdirs logstore1,logstore2,logstore3 -keepdir 	

		Executing the above will fetch all files matching the regular expression '\.log\.gz' back to the local 
		server.
		On the local server, the following directory structure will be setup: 
			/home/itk/mylogdir
			/home/itk/mylogdir/logstore1
			/home/itk/mylogdir/logstore2
			/home/itk/mylogdir/logstore3
		Matching files on the remote serve will be stored in the corresponding location pon the local server. 
		If the keepdirs had not been specified,	then all files would be stored in the directory /home/itk/mylogdir.
		A hidden history file will also be stored in the base directory specified so that the net time the utility 
		runs, only newer files are collected.
		
	Setting up logcollect to continuously bring files back to your local server.
	---------------------------------------------------------------------------
		After initial setup, the command line parameters must be changed, and it is recomended to put the script into
		the crontab for complete automation.
		The local log directory must be specified, as this contains the history and configuration data file
		The remote host/ip, username and password must be specified (and port if port forwarding is taking
		place)
		The logmatch regular expression must be specifed.
		The jobid must be specifed. This also helps the utility pick up the correct history / config file in the local 
		directory store.
		The optional test flag (not shown) is recomended during configuration. It will allow you to see what
		directories will be searched and what files will be retreived.		
			
			logcollect -ld /home/itk/mylogdir -rs utran01 -un itk -pw itkwho  -port 999 -logmatch '\.log\.gz' 
				   -jobid fetchlogs
		
		Running the above will cause the history/configuration file specified by jobid in the /home/itk/mylogdir directory
		to be read and used for determing what remote directories should be searched for log files meeting the regular expresion defined
		by the logmatch command line option.
		In addition for which directory to search, the last timestamp collected will be stored.

		For continous operation and collection, place this utility into the crontab scheduler.
		
_EOF_
	exit;

}

