#!/usr/bin/perl
#------------------------------------------------------------------------------
#
# itkfilecollect : fetches files from cello nodes and stores in a spool directory
#
# © Ericsson AB 2006 - All Rights Reserved
#
# No part of this material may be reproduced in any form without the written
# permission of the copyright owner. The contents are subject to revision without
# notice due to continued progress in methodology, design and manufacturing.
# Ericsson shall have no liability for any error or damage of any kind resulting
# from the use of these documents.
#
# Ericsson is the trademark or registered trademark of Telefonaktiebolaget LM
# Ericsson. All other trademarks mentioned herein are the property of their
# respective owners.
#------------------------------------------------------------------------------

use strict;

use FindBin qw($Bin);
use lib "../../../lib/";
use lib "$Bin/../lib/";

use Getopt::Long;
use File::Path;
use File::Temp qw(tempdir);
use Storable;
use Time::Local;
use Parallel::ForkManager;
use Data::Dumper;

use Itk::FetchFiles;
use Itk::FileUtils;
use Itk::NitsPackNodeDetails;
use Itk::NitsPackServer;
use Itk::PmData::DataFormatter;

#------------------------------------------------------------------------------
# Global Variables
#------------------------------------------------------------------------------
our $srv;

my (
	$help,
	$storedir,  	# store the historical database
	$dbfilename,	# dbfile name
	$fileType,  	# identify which file to fetch
	$dontfetch, 	# don't fetch the file
	$handler,   	# handler for file
	$ipdatabase,	# ip database
	$sitefile,  	# site file
	$maxProc,	# Maximum # of parallel processes
	$jobName,	# Name of lockfile
	$lockfile, 	# Locking file used to prevent multiple executions
	$pathdirs,  	# default path directory
	$spooldir,  	# spool directory.
	$configFile,	# configuration file
	$maxfiles,  	# max file
	$filematch, 	# filematch
	$protocol,  	# protocol used
	$port,      	# port
	$ossHost,   	# OSS Host Name
	$ossUser,   	# OSS Username
	$ossPass,   	# OSS Password
	$fdnFile,	# File containing nodes and FDNs to fetch ebs from
	$diskSpaceFree,  # disk space free on disk.
	$dbBackups,     # number of db backups to keep
	$zip            # indicate to zip the output in one file.
);


#------------------------------------------------------------------------------
# Configuration
#------------------------------------------------------------------------------

GetOptions(
	'h|help' 	=> \$help,
	'dbfile=s'	=> \$dbfilename,
	'storedir=s'	=> \$storedir,
	'fileType=s' 	=> \$fileType,
	'i=s'           => \$ipdatabase,
	'f=s'           => \$filematch,
	's=s'           => \$sitefile, 
	'd=s'		=> \$spooldir,
	'c|configfile=s' => \$configFile,
	'x'		=> \$dontfetch,
	'm=s'           => \$pathdirs,
	'protocol=s'    => \$protocol,
	'maxfiles=s' 	=> \$maxfiles,
	'handler=s' 	=> \$handler,
	'maxproc=s'	=> \$maxProc,
	'port=s'        => \$port,
	'j|job=s'	=> \$jobName,
	'oss=s'		=> \$ossHost,
	'ossuser=s'	=> \$ossUser,
	'osspass=s'	=> \$ossPass,
	'fdnfile=s'	=> \$fdnFile,
	'diskspace=s' => \$diskSpaceFree,
	'dbbackups=s' => \$dbBackups,
	'zip' => \$zip
	
) or usage();

# MAX_PROC_TIME 		- Max amount of time a single node is allowed to take
# TIMEOUT_POLL_INTERVAL 	- How often parent process checks for timed out children
use constant TIMEOUT => {
	'PMXML'		=>	{MAX_PROC_TIME  => 60,	TIMEOUT_POLL_INTERVAL=>15},
	'GPEH'		=>	{MAX_PROC_TIME  => 1200,TIMEOUT_POLL_INTERVAL=>60},
	'OSS'		=>	{MAX_PROC_TIME  => 2400,TIMEOUT_POLL_INTERVAL=>60},
};


#--------------------------------------------
# Configuration
#--------------------------------------------
$configFile ||= "$Bin/../etc/itk.conf";
$srv=new Itk::NitsPackServer(config => $configFile);

$ipdatabase ||= $srv->getConfig("ipdatabase") || find_ipdatabase();
$sitefile ||= $srv->getConfig("sitefile");
$handler ||= $srv->getConfig("itkfilecollect.handler");
$protocol ||= ($srv->getConfig("transfermethod") || "ftp");
$maxProc ||= $srv->getConfig("itkfilecollect.maxproc") || 50;
$jobName ||= $srv->getConfig("itkfilecollect.jobname");
$dbfilename ||= $srv->getConfig("itkfilecollect.dbfile") || "itkcollectdb.db";
$ossHost ||= $srv->getConfig("itkfilecollect.oss");
$ossUser ||= $srv->getConfig("itkfilecollect.ossuser");
$ossPass ||= $srv->getConfig("itkfilecollect.osspass");
$fdnFile ||= $srv->getConfig("itkfilecollect.fdnfile");
$maxfiles ||= $srv->getConfig("itkfilecollect.maxfiles");

$spooldir ||= $srv->getConfig("spooldir") || "$ENV{HOME}/spool";
$storedir ||= $srv->getConfig("itkfilecollect.logstore") || "$ENV{HOME}/logstore/pm";

$diskSpaceFree ||= $srv->getConfig("itkfilecollect.diskspace");

$diskSpaceFree ||= 20;

my $current_yr = (gmtime(time))[5]+1900;


$maxProc ||= 50;

usage() if $help;

exit if ($srv->jobstopFileExists);

#------------------------------------------------------------------------------
# Globals
#------------------------------------------------------------------------------


#------------------------------------------------------------------------------
# usage
#------------------------------------------------------------------------------
sub usage {
	$"=',';
	print STDERR <<_EOF_;

itkfilecollect -fileType <PMXML | GPEH | OSS> [-protocol <ftp | sftp>] [-dbfile <filename>] 
               [-h] [-i <ipdatabase>] [-s <sitefile>] [-d <spooldir>] [-storedir <storedir>] 
               [-m <pmpath>] [-maxfiles <num>] [-f <regex>] [-x] [-handler <file>]
               [-maxproc <name>] [-job <jobname>] [-p port] [-oss <oss_ip_address>]
	           [-ossuser <user>] [-osspass <pass>] [-fdnfile <file>] 
	           [-diskspace <percent>]

Fetch XML or GEPH files from cello nodes and store in a spool directory, where
they can be picked up by a remote host.

Each time the tool is run, it will check all available files on each node
and fetch those that have not been fetched previously.

-h       : Show help
-fileType: PMXML | GPEH | OSS
-protocol: ftp | sftp 
           (default: ftp)
-dbfile  : dbfilename 
           (default: filecollectdb)
-storedir: directory where the historical data collection is stored. 
           (default: $storedir)   	   
-i       : Specify IP database (if not specified, then it will be
           autodetected from moshell in the path)
-s       : Specify sitefile. If not specified then it is assumed that
           files are to be fetched from *all* nodes in the ipdatabase
-d       : Spool directory:  
           (default: $spooldir)
-m       : PM or GPEH dirs to search on nodes, comma separated list
           (default: /p001200/pm,/p001300/pm,/c/public_html/cello/XML_files,/p001200/pm_data,/p001300/pm_data,/c/pm_data)
-f       : If set, any PM or GPEH files on the node will only be fetched if they match
           this regular expression.
-maxfiles: Specify the max number of files that will be kept in the spool dir
           per node.
-x       : Don't fetch any files. Mark all files on the node as already fetched
           This can be used the first time you run xmlcollect, so that only
	       files fetched from this point in time on will be fetched.
-handler : Load the specified file, which is interpreted as perl code. Within
           this file you may define subroutines that can get called at various
	       points in the process:
	   
	       preFetch() : If it exists, this will be called before any nodes are
	       processed. You can use this to set up anything which needs to be
	       set up prior to fetching any files.
	   
	       postFileFetch(\$node, \$file, \%nodedetails) : If it exists, this will be called 
	       after each file has been fetched. \$node is the Node name, \$file is
	       the path to the file and \%nodedetails is a hashref containing the details 
	       about the node if they are known:  fdn, type, username, passwd, ip_address.
	       This sub must return a true value if the file was successfully handled: 
	       if not, the file will be re-attempted at the next run.

	       postNode(\$node, \$dir, \%nodedetails) : If it exists, this will be called after all
	       the files for a node are fetched. \$node is the Node name, \$dir is
	       the path to the directory in which the files for this node are stored.
	       \%nodedetails is a hashref containing the details about the node if they are known: 
	       fdn, type, username, passwd, ip_address.
	   
	       postFetch(\$dir) : If it exists, this will be called after all nodes
	       have been processed. \$dir is the path to the directory in which all
	       files have been stored.
	   
-maxproc : Maximum number of parallel processes
  	       Default: '$maxProc'
-j|job   : Job name. If specified then prevents another instance of xmlcollect
	       with the same job name from starting.
-oss     : Specify OSS IP address or hostname (needed to collect EBS/OSS files)
-ossuser : Specify OSS username (needed to collect EBS/OSS files)
-osspass : Specify OSS password (needed to collect EBS/OSS files)
-fdnfile : Specify file containing list of node unique names & FDNs (needed for EBS/OSS collection)
	   File is of the format:  nodeuniquename nodefdn nodetype [username password host]
-diskspace: 20% is default. Specify the percentage of diskspace available for collection. 
            for example, if the value is 10, the collection will be executed only if you have at least 10%
            available on your disk.
-dbbackups : The number of hisory db backups to keep and cycle. If set to 0 or 'none', backup will be 
            skipped. The default number is 5.
	   
	   
_EOF_
	exit;
}


#==============================================================================
# MAIN
#==============================================================================

# define an hash for the option
my %collectOptions = ();

#
# check the file of collection
#
my $collectModule = undef;

#
# check if collection valid
#
if ($fileType !~ /^PMXML|GPEH|OSS$/) {
    die "fileType: '$fileType' is invalid only PMXML, GPEH or OSS is accepted\n";
}

if ($fileType=~/^OSS$/) {
	die "OSS address, username and password and fdnfile are required for OSS collection"  unless ($ossHost && $ossUser && $ossPass && $fdnFile);

} else {
	# check the ip database and the sitefile
	if (!defined ($ipdatabase)) {
		$ipdatabase = find_ipdatabase();
		if (!defined ($ipdatabase)) {
			die  "A path for the ipdatabase file is required ";
		}			
	}
	$collectOptions{ipdatabase}=$ipdatabase;
}

#
# check if the protocol is valid]
#
if ($protocol ne "ftp" && $protocol ne "sftp") {
    $srv->logError(" protocol: '$protocol' is invalid only ftp or sftp is accepted ");
	exit;		
} 
$collectOptions{protocol}=$protocol;

#
# check if site name defined.
if (defined ($sitefile)) {
	$collectOptions{sitefile}=$sitefile;
}

# check if the store directory exist 
# if it doesn't, it will be created
mkpath $spooldir unless -d $spooldir;
# check the result	
$collectOptions{spooldir}=$spooldir;

if (defined ($dbfilename)) {
	$collectOptions{dbfilename}=$dbfilename;
} else {
	$collectOptions{dbfilename}="itkcollectdb";
}

# check if the store directory exist 
# if it doesn't, it will be created
mkpath $storedir unless -d $storedir;
# check the result
# fix me
$collectOptions{storedir}=$storedir;

# do a backup of history db here
unless ($dbBackups =~ /^(0|none)$/) {
	Itk::FileUtils::backupHistoryFile($collectOptions{storedir}, $collectOptions{dbfilename}, $dbBackups);
} else {
	$srv->logDebug(1, "Backing up history database skipped due to -dbbackups set to $dbBackups.");
}

# check the filematch
if (defined ($filematch)) {
	# Compile file match expression
	$filematch=qr/$filematch/;
	$collectOptions{filematch}=$filematch;
} 

# check the port
if (defined ($port)) {
   $collectOptions{port}=$port;
}

# check the dontfetch flag
if (defined ($dontfetch)) {
   $collectOptions{dontfetch}=$dontfetch;
}

# check the maxfiles flag
if (defined ($maxfiles)) {
   $collectOptions{maxfiles}=$maxfiles;
}

if (defined ($zip) ) {
    $collectOptions{zip}=1;	
} else {
	$collectOptions{zip}=0;
}

# obtain a lock to ensure that no other copy is running under this job name
if ($jobName) {
  $lockfile="/tmp/itklock-itkfileCollect-$jobName";
  open LOCK, ">$lockfile" or die "Cannot open $lockfile : $!";
  unless (flock LOCK, 6) {
  	 "Another instance of xmlcollect is already running. This instance will abort";
  	die "Unable to get lock: program aborted\n";
  }
}

if ($handler) {
   $srv->logInfo("Loading handler file: $handler\n");
	unless (my $return = do $handler) {
		    die "Couldn't compile $handler: $@\n" if $@;
		    die "Couldn't read $handler: $!\n" unless defined $return;
		    warn "Initialisation of '$handler' was not successful\nReminder: Modules need to return true!\n" unless $return;
	}
}

#
# default path for the file
#
my @pmpaths = undef;
if (defined ($pathdirs)) {
   @pmpaths = split(/,/,join(',',$pathdirs));
   $collectOptions{defaultdir}=\@pmpaths;
}

#-----------------------------------
# variable declation
#-----------------------------------
my $nodelist = getListNodeToCollect ($fileType, $ipdatabase, $sitefile, $fdnFile);
my @keynodelist = sort keys %$nodelist;

my $pm = new Parallel::ForkManager($maxProc); 

my ($nodesStarted,$nodesComplete,%childStartTimes);
my $totalNodes = scalar @keynodelist;

$pm->run_on_start(
	sub { 
		my ($pid,$nodeId)=@_;
		$nodesStarted+=1;
		$childStartTimes{$pid}=time;
		my $nodesRemaining = $totalNodes-$nodesComplete;
		$srv->logInfo("Processing started for '$nodeId', Process ID '$pid' : total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining) ");
	
	}
);

$pm->run_on_finish(
	sub { 
		my ($pid, $exit_code, $nodeId, $exitSig) = @_;
		$nodesStarted-=1;
		$nodesComplete+=1;
		my $nodesRemaining = $totalNodes-$nodesComplete;
		if ($exitSig) {
			$srv->logError("Child process terminated on signal $exitSig");
		}
		my $runTime = time() - $childStartTimes{$pid};
		$srv->logInfo("Processing ended for '$nodeId', Process ID '$pid' : total($totalNodes), running($nodesStarted), complete($nodesComplete), remaining($nodesRemaining) : runtime $runTime ");
		delete $childStartTimes{$pid};
	}
);

# To guard against hanging processes, we keep track of how long each child has
# been running and kill any that take longer than MAX_PROC_TIME
$SIG{ALRM}=sub {
	my $t=time;
	if (DEBUG3) {
		my @lines = map sprintf("\t$_ => %d s",$t - $childStartTimes{$_}), keys %childStartTimes;
		$srv->logDebug(3, "Timeout Check: current process run times:\n".join("\n",@lines));
	}
	foreach my $pid (keys %childStartTimes) {
		if ($t - $childStartTimes{$pid} > TIMEOUT->{fileType}->{MAX_PROC_TIME}) {
			$srv->logError("Process pid=$pid exceeded MAX_PROC_TIME - killing it");
			kill 15, $pid;
		}
	}
	alarm TIMEOUT->{fileType}->{TIMEOUT_POLL_INTERVAL};
};

alarm TIMEOUT->{fileType}->{TIMEOUT_POLL_INTERVAL};

my $pid;
NODE:
foreach my $node (@keynodelist) {

	
	$srv->logDebug(1, "Start collection for $node") if DEBUG1;
	
	# Fork a new process. Parent loops (pid=undef) and child continues
	$pid = $pm->start($node) and next;

	if (exists $main::{preFetch}) {
		&{$main::{preFetch}}();
	}

    #check the diskSpace 
    my %disk = Itk::FileUtils::getDiskSpace ($spooldir); 
    

    if ( (100 - $disk{precentUsed}) < $diskSpaceFree) {
	    $srv->logError("The File collection is interrepted. The diskspace is already at ". $disk{precentUsed} );
	    $srv->logError("The free disk space threshold is set at $diskSpaceFree ");
	    $srv->logError("To continue set the free disk space threshold to a lower value or free up some disk space ");
  	    $pm->finish;
    	next;
    }

	if ($fileType eq "OSS") {
		$collectModule = new Itk::FetchFilesOSS (
			%collectOptions,
			nodename=>$node,
			nodedetails => $nodelist->{$node},
			osshost=>$ossHost,
			ossuser=>$ossUser,
			osspass=>$ossPass
		);
		
	} else {

		$srv->logDebug(1, "$node: ip_address=$nodelist->{$node}->{ip_address} collection type=$fileType") if DEBUG1;
	
		if ($fileType eq "PMXML") {
			$collectModule = new Itk::FetchFilesPMXML (
				%collectOptions,
				nodename=>$node,
				ip => $nodelist->{$node}->{ip_address},
				password => $nodelist->{$node}->{passwd},
				username => $nodelist->{$node}->{username},
				nodedetails => $nodelist->{$node},
			);
		} elsif ($fileType eq "GPEH"){
			$collectModule = new Itk::FetchFilesGPEH (
				%collectOptions,
				nodename=>$node,
				ip => $nodelist->{$node}->{ip_address},
				password => $nodelist->{$node}->{passwd},
				username => $nodelist->{$node}->{username},
				nodedetails => $nodelist->{$node},
			);
		}
	}

 	#
 	# collect from the node
	my $retval = $collectModule->collect ();
	$collectModule->runPostNodeHook() or $srv->logError("PostNode hook failed for node $node.");

	$pm->finish;
}


$pm->wait_all_children;	# Do not allow the program to terminate until all child processes are finished

if (exists $main::{postFetch}) {
	&{$main::{postFetch}}($spooldir);
}

my $totalRunTime = time() - $^T;
$srv->logInfo("Processed $totalNodes nodes in $totalRunTime seconds.\n");

# Done. Release Lock
if ($jobName) {
	flock LOCK, 8;
	close LOCK;
	unlink $lockfile;
}


##------------------------------------------------------------------------------
# getListNodeToCollect
# Gets the list of node to collect data from.
# 
# RETURNS: A hash reference whose keys are node names, and values are either
# ip address, usernames, passwords, etc (non-EBS) or FDNs (EBS)
#------------------------------------------------------------------------------

sub getListNodeToCollect {
	my ($fileType, $ipdatabase, $sitefile, $fdnFile) = @_;
	
	if ($fileType =~/^OSS$/) {
		
		my %nodeList;
		open FDNFILE, $fdnFile or die "Failed to open $fdnFile: $!\n";
		while (<FDNFILE>) {
			next if /^\s*#/; # Skip any comments
			chomp;
			my ($nodeName, $nodeFdn, $nodeType, $nodeUsername, $nodePassword, $nodeAddress) = split or next;
			$nodeList{$nodeName}={	fdn=>$nodeFdn,
						type=>$nodeType,
						username=>$nodeUsername,
						passwd=>$nodePassword,
						ip_address=>$nodeAddress
						};
		}
		close FDNFILE;
		return \%nodeList;
		
	} else {
		# Get list of nodes to work with and filter them if necessary
		return Itk::NitsPackNodeDetails::getNodeDetails(
			ip_database => $ipdatabase,
			sitefile => $sitefile
		);
	}
				   
}

#------------------------------------------------------------------------------
# find ipdatabase file
# Will check as follows:
# 1. If there is an environment variable called IPDATABASE, then return that
# 2. If there is an environment variable called MOSHELL_HOME, then return
#	MOSHELL_HOME/sitefiles/ipdatabase
# 3. If there is an moshell in the path, then find its dir and return
#	DIR/sitefiles/ipdatabase
#------------------------------------------------------------------------------
sub find_ipdatabase {
	return $ENV{IPDATABASE} if $ENV{IPDATABASE};
	my $moshelldir=$ENV{MOSHELL_HOME} || `dirname \`/usr/bin/which moshell 2>/dev/null\``;
	chomp $moshelldir;
	return undef unless -d $moshelldir;
	my $ipdatabase="$moshelldir/sitefiles/ipdatabase";
	return undef unless -e $ipdatabase;
	return $ipdatabase;
}



